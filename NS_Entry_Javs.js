﻿function abc() {

    alert('Calling a java script function from code behind.');
}
function OnKeyPress(nextfield, event) {
    //alert('OnKeyPress');
    if (event.keyCode == 13) {
        LoopTable();
        var _next = document.getElementById(nextfield);
        _next.focus();
    }
}
function AddGoatKeyPress(e) {
    //alert('AddGoatKeyPress');
    var keynum;
    if (window.event) { // IE					
        keynum = e.keyCode;
    } else
        if (e.which) { // Netscape/Firefox/Opera					
            keynum = e.which;
        }
    if (keynum == 13) {
        var _goatId = document.getElementById('txtAddGoat').value.toUpperCase();
        if (_goatId.length == 8) {
            _goatId = 'P' + _goatId;
        }
        ClearAddGoat();
        wsProxy.GetAddGoat(_goatId, wsGotAddGoat, wsFail);
    }
}
function OnMemberPress(event) {

    //alert('OnMemberPress');
    if (event.keyCode == 13) {
        var _id = document.getElementById('txtMemId').value
        if (_id != '') {
            wsProxy.GetShowAs(_id, wsGotMember, wsFail);
            document.getElementById('btnGrpCancel').click();
        } else {
            document.getElementById('txtShowAs').value = 'Member Not Found';
            document.getElementById('txtMemId').value = '';
            document.getElementById('txtMemId').focus();
        }
    }
    return false;
}
function OnCKeyPress(nextfield, event) {
    //alert('OnCKeyPress');
    //return false;
    if (event.keyCode == 13) {
        LoopCTable();
        var _next = document.getElementById(nextfield);
        _next.focus();
    }

}

function FilledDrops() {
            var _ddBreed = document.getElementById('ddbreed');
            var _ddClass = document.getElementById('ddclass');
            document.getElementById('txtClassNum').value = '';
            if ((_ddBreed.value != '') && (_ddClass.value != '')) {
               var _BreedName = _ddBreed.options[_ddBreed.selectedIndex].text;
               var _classNum = GetClassNum(_BreedName,_ddClass.value);
                document.getElementById('txtClassNum').value = _classNum;
                var _next= document.getElementById('textEntry1');
                _next.focus();
            }
        }
         function FillAddDrops() {
            var _ddBreed = document.getElementById('ddaddbreed');
            var _ddClass = document.getElementById('ddaddclass');
            document.getElementById('txtAddClassNum').value = '';
            if ((_ddBreed.value != '') && (_ddClass.value != '')) {
                var _BreedName = _ddBreed.options[_ddBreed.selectedIndex].text;
                var _classNum = GetClassNum(_BreedName,_ddClass.value);
                document.getElementById('txtAddClassNum').value = _classNum;
            }
        }
       function GetClassNum(_breed,_classname) {
//            var _breed  = document.getElementById('ddbreed').value;
//            var _classname = document.getElementById('ddclass').value;
            var _barr;
            switch(_breed) {
                case "Alpine":
                    _barr = new Array(89, 90, 91, 92, 93, 96, 97, 98, 99, 100, 101, 94, 95, 102, 103, 104, 105, 106);
                    break;
                case "LaMancha":
                     _barr = new Array(1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 13, 6, 7, 14, 15, 16, 17, 18);
                    break;
               case "Nigerian Dwarf":
                    _barr = new Array(107, 108, 109, 110, 111, 114, 115, 116, 117, 118, 119, 112, 113, 120, 121, 122, 123, 124);
                    break;
                case "Nubian":
                    _barr = new Array(125, 126, 127, 128, 129, 132, 133, 134, 135, 136, 137, 130, 131, 138, 139, 140, 141, 142);
                    break;
                case "Oberhasli":
                    _barr = new Array(71, 72, 73, 74, 75, 78, 79, 80, 81, 82, 83, 76, 77, 84, 85, 86, 87, 88);
                    break;
                case "Recorded Grade":
                    _barr = new Array(37, 38, 39, 40, 41, 43, 44, 45, 46, 47, 48, 42, 0, 49, 50, 0, 51, 52);
                    break;
                case "Saanen":
                    _barr = new Array(53, 54, 55, 56, 57, 60, 61, 62, 63, 64, 65, 58, 59, 66, 67, 68, 69, 70);
                    break;
                case "Toggenburg":
                    _barr = new Array(19, 20, 21, 22, 23, 26, 27, 28, 29, 30, 31, 24, 25, 32, 33, 34, 35, 36);
                    break;
                case "Sable":            
                    _barr = new Array(143, 144, 145, 146, 147, 150, 151, 152, 153, 154, 155, 148, 149, 156, 157, 158, 159, 160);
                    break;
            }
            var _class = new Array("Junior Kid", "Intermediate Kid", "Senior Kid", "Junior Yearling", "Senior Yearling", "Yearling Milker", "2 Year Old Milker", "3 Year Old Milker", "4 Year Old Milker", "5 And 6 Year Old Milker", "7 Years And Older Milker", "Best 3 Junior Does", "Junior Get Of Sire", "Dairy Herd", "Best 3 Senior Does", "Senior Get Of Sire", "Produce Of Dam", "Dam And Daughter");
            var _ptr = -1;
            for(var i=0; i < _class.length; ++i) {
                if(_class[i] == _classname) {
                    _ptr = i;
                    break;
                }
            }
            if(_ptr != -1) {
                return _barr[_ptr];
            } else {
                return _ptr;
            }
        }

        function ClearAddGoat() {
            document.getElementById('txtAddGoatName').value = '';
            document.getElementById('txtAddBirthdate').value = '';
            document.getElementById('txtAddTattoo').value = '';
            document.getElementById('hdnAddGoatId').value = '';
            document.getElementById('hdnAddSireId').value = '';
            document.getElementById('hdnAddDamId').value = '';
            document.getElementById('hdnBreederId').value = '';
        }  
    function wsGotAddGoat(result) {
        var _retVal = result.getElementsByTagName("string")[0].childNodes[0].nodeValue;
        if(_retVal != 'NONE') {
            var _arr =_retVal.split(';');
            document.getElementById('txtAddGoatName').value = _arr[1];
            document.getElementById('txtAddBirthdate').value = _arr[4];
            document.getElementById('txtAddTattoo').value = _arr[6];
            document.getElementById('hdnAddGoatId').value = _arr[0];
            document.getElementById('hdnAddSireId').value = _arr[2];
            document.getElementById('hdnAddDamId').value = _arr[3];
            document.getElementById('hdnBreederId').value = _arr[5];
        } else {
            document.getElementById('txtAddGoatName').value = 'GOAT NOT FOUND';
        }
    }
    function GetAddMember() {
        var memid = document.getElementById('ddExhibitors').value;
        wsProxy.GetAddMember(memid, wsGotAdd, wsFail);

    }
    function wsGotAdd(result) {
        var _retVal = result.getElementsByTagName("string")[0].childNodes[0].nodeValue;
        var _arr =_retVal.split(';');
        document.getElementById('hdnAddMemberId').value = _arr[0];
        document.getElementById('hdnAddCustomerId').value = _arr[1];
        document.getElementById('hdnAddDisplayAs').value = _arr[2];
        document.getElementById('hdnAddShowAs').value = _arr[3];
    }

    function wsGotMember(result)
    {
        var _showas = result.getElementsByTagName("string")[0].childNodes[0].nodeValue;
        document.getElementById('txtShowAs').value = _showas;
        var _next = document.getElementById('textCEntry1');
            _next.focus();
        }
    function WriteAdd() {
            var _memberId = document.getElementById('hdnAddMemberId').value;
            var _customerId = document.getElementById('hdnAddCustomerId').value;
            var _displayAs = document.getElementById('hdnAddDisplayAs').value.toUpperCase();
            var _showAs = document.getElementById('hdnAddShowAs').value.toUpperCase();
            var _breederId = document.getElementById('hdnBreederId').value;
            var _goatName = document.getElementById('txtAddGoatName').value.toUpperCase().trim();
            var _goatId = document.getElementById('hdnAddGoatId').value;
            var _regId = document.getElementById('txtAddGoat').value.toUpperCase();
            var _sireId = document.getElementById('hdnAddSireId').value;
            var _damId = document.getElementById('hdnAddDamId').value;
            var _breedNum = document.getElementById('ddaddbreed').value;
            var _breedName = document.getElementById('ddaddbreed').options[document.getElementById('ddaddbreed').selectedIndex].text;
            var _className = document.getElementById('ddaddclass').options[document.getElementById('ddaddclass').selectedIndex].text;
            var _classNum = document.getElementById('txtAddClassNum').value;
            
            if((_className.indexOf('Get Of') > -1) || (_className.indexOf('Best 3') > -1) || (_className.indexOf('Dairy Herd') > -1) || (_className.indexOf('Produce Of') > -1) || (_className.indexOf('Dam And') > -1)) {
                var _milkOrDry = 'G';
            } else if(_className.indexOf('Milker') !=-1) {
                    var _milkOrDry = 'M';
            } else {
                    var _milkOrDry = 'D';
            }
 
            if((_memberId == '') || (_customerId == '') || (_displayAs == '') || (_showAs == '') || (_breedName == '') || (_className == '') || (_classNum == '')) {
                alert('Missing Information - Try Again...');
                return false;
            } else if((_milkOrDry != 'G') && ((_breederId == '') || (_goatName == '') || (_goatId == '') || (_sireId == '') || (_damId == ''))) {
                alert('Missing Information - Try Again...');
                return false;
            } else {
                var _strInsert = 'INSERT INTO NS_ANIMAL_ENTRY ( SHOW_YEAR, ENTRY_DATE, ENTRY_NUM, CHECKED_IN, MEMBERSHIP_ID,';
                _strInsert += 'CUSTOMER_ID, DISPLAY_AS, SHOWING_AS, BARCODE, BREEDER_ID, SIRE_ID, DAM_ID, GOAT_ID, REG_NUM,';
                _strInsert += 'ANM_NAME, CLASS_TYPE, CLASS_NAME, CLASS_NUM, BREED_CODE, PLACING, UDDER, AWARDS, ENTRY ) VALUES (';
                _strInsert += '2016, sysdate, ns_entry_id.nextval, \'Y\',\'' + _memberId + '\',\'' + _customerId + '\',\'' + _displayAs.toUpperCase + '\',';
                _strInsert += '\'' + _showAs.toUpperCase + '\',NULL,\'' + _breederId + '\',\'' + _sireId + '\',\'' + _damId + '\',\'' + _goatId + '\',';
                _strInsert += '\'' + _regId.toUpperCase + '\',\'' + _goatName.toUpperCase + '\',\'' + _milkOrDry.toUpperCase + '\',\'' + _breedName.toUpperCase + ' ' + _className.toUpperCase + '\',' + _classNum + ',\'' + _breedNum + '\',NULL,NULL,NULL,\'WEB\')';
                _strInsert = _strInsert.replace(/\'\'/g, 'NULL')
//                alert(_strInsert);
                wsProxy.AddEntry(_strInsert,wsAddEntry,wsFail);
            }
    }
    function wsAddEntry(result) {
        var _retVal = result.getElementsByTagName("string")[0].childNodes[0].nodeValue
        if(_retVal == "OK") {
            alert('Entry Successfully Added');
            ClearAddGoat();
             document.getElementById('txtAddGoat').value = '';
            document.getElementById('txtAddClassNum').value = '';
           document.getElementById('ddaddbreed').selectedIndex = 0;
            document.getElementById('ddaddclass').selectedIndex = 0;

        } else {
            alert('ERROR: Entry Not Added');
        }
    }
         function LoopTable()
        {
            try {
                div=document.getElementById('divResults');
                var elms = div.getElementsByTagName("*");
                for(var i = 0, maxI = elms.length; i < maxI; ++i) {
                    var elm = elms[i];
                   if (elm.tagName=='INPUT') {
                        if (elm.id.substring(0, 9)== 'textEntry') {
                            var _entryNum = document.getElementById(elm.id).value 
                            if (_entryNum != '') {
                                _entryNum=_entryNum.replace( 'NS2017', '' );
                                document.getElementById(elm.id).value = _entryNum;
                                var _boxNum=elm.id.replace("textEntry", "");
                                var _name = document.getElementById('txtANum' + _boxNum).value
                                if (_name == '') {
                                    wsProxy.GetEntry(_entryNum, wsGotEntry, wsFail, _boxNum);
                                    document.getElementById('btnGrpCancel').click();
                                }
                            }
                        }
                    }
                 }
            } catch (e) {
            }
        }

        function wsGotEntry(result,_boxNum)
         {
            var _retarr = result.getElementsByTagName("string")[0].childNodes[0].nodeValue.split("|");
            document.getElementById('txtANum' + _boxNum).value=_retarr[1];
            document.getElementById('txtAName' + _boxNum).value=_retarr[2];
            document.getElementById('txtMName' + _boxNum).value = _retarr[3];
            var _nextBox = parseInt(_boxNum) + 1;
            var elem = document.getElementById('textEntry' + _nextBox)
            elem.focus();
        }

        function wsFail(error) {
             alert("Stack Trace: " + error.get_stackTrace() + '\n' +
                  "Error: " + error.get_message() + '\n' +
                  "Status Code: " + error.get_statusCode() + '\n' +
                  "Exception Type: " + error.get_exceptionType() + '\n' +
                  "Timed Out: " + error.get_timedOut());    
        }
        


        function LoopCTable()
        {
            try {
                div=document.getElementById('divCheckin');
                var elms = div.getElementsByTagName("*");
                for(var i = 0, maxI = elms.length; i < maxI; ++i) {
                    var elm = elms[i];
                   if (elm.tagName=='INPUT') {
                        if (elm.id.substring(0, 10)== 'textCEntry') {
                            var _entryNum = document.getElementById(elm.id).value 
                            if (_entryNum != '') {
                                _entryNum = _entryNum.replace('NS2017', '');
                                document.getElementById(elm.id).value = _entryNum;
                                var _boxNum = elm.id.replace("textCEntry", "");
                                var _name = document.getElementById('txtCANum' + _boxNum).value
                                if (_name == '') {
                                    wsProxy.GetEntry(_entryNum, wsGotCEntry, wsFail, _boxNum);
                                    document.getElementById('btnGrpCancel').click();
                                }
                         }    }
                    }
                 }
            } catch (e) {
            }
            return false;
        }

        function wsGotCEntry(result,_boxNum)
        {
            var _retarr = result.getElementsByTagName("string")[0].childNodes[0].nodeValue.split("|");
            if (_retarr[2].length == 0) {
                document.getElementById('txtCAName' + _boxNum).value = _retarr[4];
            } else {
                document.getElementById('txtCAName' + _boxNum).value = _retarr[2];
            }
            document.getElementById('txtCANum' + _boxNum).value=_retarr[1];
           
            document.getElementById('txtCMName' + _boxNum).value=_retarr[5];
            document.getElementById('textCEntry' + _boxNum).tooltip = _retarr[5];
            var _nextBox = parseInt(_boxNum) + 1;
            var elem = document.getElementById('textCEntry' + _nextBox)
            elem.focus();
        }
    function ClearSub()
    {
        document.getElementById('divGridview').innerHTML='';
        document.getElementById('txtSMemId').value='';
        document.getElementById('txtSShowAs').value='';
        document.getElementById('txtSMemId').focus();
    }
    // REPORT FUNCTIONS
    function LaunchShow() {
//    alert('In Show Entry');
//    
    rdoShowEntry.checked(false);
}
function LaunchCheckin() {
//    alert('In Check In');
    rdoCheckIn.checked(false);
}
function LaunchResults() {
//    alert('In Results Entry');
    rdoResults.checked(false);
}
function LaunchExhibitorReport() {
    var _division = prompt('Division:','');
    if(_division) {
        _division = _division.toUpperCase();
        var _url;
        _url = 'Display2.aspx?r=1&d=' + _division;
        mywindow = window.open(_url, '');
    }
    rdoExhibitor.checked(false);
}
function LaunchCheckinReport() {
        rdoCheckinReport.checked(false);
        var _url;
        _url = 'Display2.aspx?r=2';
        mywindow = window.open(_url, '');
}
function LaunchSingleCheckinReport() {
        rdoSingleCheckinReport.checked(false);
        var _url;
        _url = 'Display2.aspx?r=2';
        mywindow = window.open(_url, '');
}

function LaunchRingReport() {
        rdoRingReport.checked(false);
        var _url;
        _url = 'Display2.aspx?r=3';
        mywindow = window.open(_url, '');
}
function LaunchSingleRingReport() {
    var _memberId = prompt('Member Id:','');
    if(_memberId) {
        var _url;
        _url = 'Display2.aspx?r=5&m=' + _memberId;
        mywindow = window.open(_url, '');
    }
    rdoSingleRingReport.checked(false);
}
function LaunchMilkoutReport() {
    var _division = prompt('Division:','');
    if(_division) {
        _division = _division.toUpperCase();
        var _url;
        _url = 'Display2.aspx?r=8&d=' + _division;
        mywindow = window.open(_url, '');
    }
    rdoMilkout.checked(false);
}
function LaunchSecretaryReport() {
    var _division = prompt('Division:','');
    if(_division) {
        _division = _division.toUpperCase();
        var _url;
        _url = 'Display2.aspx?r=6&d=' + _division;
        mywindow = window.open(_url, '');
    }
    rdoSecretary.checked(false);
}
function LaunchShowbookReport() {
    var _division = prompt('Division:','');
    if(_division) {
        _division = _division.toUpperCase();
        var _url;
        _url = 'Display2.aspx?r=9&d=' + _division;
        mywindow = window.open(_url, '');
    }
    rdoShowbook.checked(false);
}

function LaunchExhibitorBarcodeReport() {
        rdoExhibitorBarcode.checked(false);
        var _url;
        _url = 'Display2.aspx?r=7';
        mywindow = window.open(_url, '');
}
function LaunchResultsReport() {
    var _division = prompt('Division:','');
    if(_division) {
        _division = _division.toUpperCase();
        var _url;
        _url = 'Display2.aspx?r=12&d=' + _division;
        mywindow = window.open(_url, '');
    }
    rdoResults.checked(false);
}
function LaunchSingleCheckinReport() {
    var _memberId = prompt('Member Id:', '');
    if (_memberId) {
        var _url;
        _url = 'Display2.aspx?r=13&m=' + _memberId;
        mywindow = window.open(_url, '');
    }
    rdoSingleRingReport.checked(false);
}