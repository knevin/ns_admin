﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services
Imports System.Xml

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.

<WebService(Namespace:="nmProxy")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class wsProxy
    Inherits System.Web.Services.WebService
    <WebMethod()> _
<ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Xml, XmlSerializeString:=True)> _
    Public Function TestDBConn() As String
        Dim xmldoc As New XmlDocument
        Dim _url As String = "http://web.adga.org/NSEntry2/wsAdmin.asmx/TestConnection"
        'Dim _url As String = "http://web.adga.org/NSEntry2/wsAdmin.asmx/ATest"
        _url = StripUrl(_url)
        xmldoc.Load(_url)
        Dim _retstr As String = ""
        _retstr = Trim(xmldoc.InnerText)
        Return _retstr
    End Function
    <WebMethod()> _
<ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Xml, XmlSerializeString:=True)> _
    Public Function AddEntry(ByVal _stmnt As String) As String
        Dim xmldoc As New XmlDocument
        'Dim _url As String = "http://localhost:63154/NationalShow.asmx/WriteAdd?_stmnt=" & _stmnt
        Dim _url As String = "http://web.adga.org/NSEntry2/wsAdmin.asmx/WriteAdd?_stmnt=" & _stmnt
        '_url = StripUrl(_url)
        xmldoc.Load(_url)
        Dim _retstr As String = ""
        _retstr = Trim(xmldoc.InnerText)
        Return _retstr
    End Function

    <WebMethod()> _
<ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Xml, XmlSerializeString:=True)> _
    Public Function GetDOB(ByVal _sireid As String, ByVal _damid As String) As String
        Dim _retstr As String = ""
        Dim _siredob As Date = "01/01/1900"
        Dim _damdob As Date = "01/01/1900"
        Dim sirexmldoc As New XmlDocument
        'Dim _url As String = "http://localhost:63154/NationalShow.asmx/GetGoatInfo?_GoatID=" & _sireid
        Dim _url As String = "http://web.adga.org/NSEntry2/wsAdmin.asmx/GetGoatInfo?_GoatID=" & _sireid
        _url = StripUrl(_url)
        sirexmldoc.Load(_url)
        If Not sirexmldoc.GetElementsByTagName("DOB")(0) Is Nothing Then
            _siredob = CDate(Trim(sirexmldoc.GetElementsByTagName("DOB")(0).ChildNodes(0).Value))
        End If
        Dim damxmldoc As New XmlDocument
        '_url = "http://localhost:63154/NationalShow.asmx/GetGoatInfo?_GoatID=" & _damid
        _url = "http://web.adga.org/NSEntry2/wsAdmin.asmx/GetGoatInfo?_GoatID=" & _damid
        _url = StripUrl(_url)

        damxmldoc.Load(_url)
        If Not damxmldoc.GetElementsByTagName("DOB")(0) Is Nothing Then
            _damdob = CDate(Trim(damxmldoc.GetElementsByTagName("DOB")(0).ChildNodes(0).Value))
        End If
        _retstr = "SIRE DOB:" & _siredob.ToShortDateString & ";DAM DOB:" & _damdob.ToShortDateString

        Return _retstr
    End Function

    <WebMethod()> _
   <ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Xml, XmlSerializeString:=True)> _
    Public Function CheckTattoo(ByVal _customerid As String, ByVal _tattoo As String) As String
        Dim xmldoc As New XmlDocument
        'Dim _url As String = "http://localhost:63154/NationalShow.asmx/CheckMemberTattoo?_customerID=" & _customerid & "&_tattoo=" & _tattoo
        Dim _url As String = "http://web.adga.org/NSEntry2/wsAdmin.asmx/CheckMemberTattoo?_customerID=" & _customerid & "&_tattoo=" & _tattoo
        _url = StripUrl(_url)
        xmldoc.Load(_url)
        Return xmldoc.InnerText
    End Function
    <WebMethod()> _
<ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Xml, XmlSerializeString:=True)> _
    Public Function GetEntry(ByVal _entryNum As Integer) As String
        Dim xmldoc As New XmlDocument
        'Dim _url As String = "http://localhost:63154/NationalShow.asmx/GetNSEntry?_entryNum=" & _entryNum
        Dim _url As String = "http://web.adga.org/NSEntry2/wsAdmin.asmx/GetNSEntry?_entryNum=" & _entryNum
        Dim _retstr As String = ";;"
        _url = StripUrl(_url)
        xmldoc.Load(_url)
        Return xmldoc.InnerText
    End Function
    <WebMethod()> _
    <ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Xml, XmlSerializeString:=True)> _
    Public Function GetGoatName(ByVal _id As String) As String
        Dim xmldoc As New XmlDocument
        'Dim _url As String = "http://localhost:63154/NationalShow.asmx/GetGoatInfo?_GoatID=" & _id
        Dim _url As String = "http://web.adga.org/NSEntry2/wsAdmin.asmx/GetGoatInfo?_GoatID=" & _id
        Dim _retstr As String = ";;"
        _url = StripUrl(_url)
        xmldoc.Load(_url)
        If Not xmldoc.GetElementsByTagName("DISPLAY_NAME")(0) Is Nothing Then
            _retstr = Trim(xmldoc.GetElementsByTagName("SEX")(0).ChildNodes(0).Value) & ";" & Trim(xmldoc.GetElementsByTagName("DISPLAY_NAME")(0).ChildNodes(0).Value)
        End If
        Return _retstr
    End Function
    <WebMethod()> _
<ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Xml, XmlSerializeString:=True)> _
    Public Function GetOwners(ByVal _sireid As String, ByVal _damid As String, ByVal _dob As String, ByVal _submitter As String) As String
        Dim xmldoc As New XmlDocument
        'Dim _url As String = "http://localhost:63154/NationalShow.asmx/GetBreeder?_sireID=" & _sireid & "&_damID=" & _damid & "&_dob=" & _dob & "&_submitter=" & _submitter
        Dim _url As String = "http://web.adga.org/NSEntry2/wsAdmin.asmx/GetBreeder?_sireID=" & _sireid & "&_damID=" & _damid & "&_dob=" & _dob & "&_submitter=" & _submitter
        _url = StripUrl(_url)
        xmldoc.Load(_url)
        Dim _retstr As String = ""
        _retstr = Trim(xmldoc.InnerText)
        Return _retstr
    End Function
    <WebMethod()> _
<ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Xml, XmlSerializeString:=True)> _
    Public Function GetAddGoat(ByVal _regId As String) As String
        Dim xmldoc As New XmlDocument
        'Dim _url As String = "http://localhost:63154/NationalShow.asmx/GetAddGoatInfo?_regId=" & _regId
        Dim _url As String = "http://web.adga.org/NSEntry2/wsAdmin.asmx/GetAddGoatInfo?_regId=" & _regId
        _url = StripUrl(_url)
        xmldoc.Load(_url)
        Dim _retstr As String = ""
        _retstr = Trim(xmldoc.InnerText)
        Return _retstr
    End Function
    <WebMethod()> _
 <ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Xml, XmlSerializeString:=True)> _
    Public Function GetGoatHist(ByVal _id As String) As String
        Dim xmldoc As New XmlDocument
        'Dim _url As String = "http://localhost:63154/NationalShow.asmx/GetGoatHistory?_GoatID=" & _id
        Dim _url As String = "http://web.adga.org/NSEntry2/wsAdmin.asmx/GetGoatHistory?_GoatID=" & _id
        _url = StripUrl(_url)
        xmldoc.Load(_url)
        Dim _retstr As String = ""
        If Not xmldoc.GetElementsByTagName("EFFDT")(0) Is Nothing Then
            Dim i As Integer = 0
            Dim _sdate As Date
            For i = 0 To xmldoc.GetElementsByTagName("MEMBERSHIP_ID").Count - 1
                _sdate = xmldoc.GetElementsByTagName("EFFDT")(i).ChildNodes(0).Value
                _retstr = _retstr & _sdate.ToShortDateString & ";"
                _retstr = _retstr & Trim(xmldoc.GetElementsByTagName("MEMBERSHIP_ID")(i).ChildNodes(0).Value) & ";"
            Next
        End If
        Return _retstr
    End Function
    <WebMethod()> _
<ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Xml, XmlSerializeString:=True)> _
    Public Function GetShowAs(ByVal _id As String) As String
        Dim xmldoc As New XmlDocument
        'Dim _url As String = "http://localhost:63154/NationalShow.asmx/HasEntry?_memberId=" & _id
        Dim _url As String = "http://web.adga.org/NSEntry2/wsAdmin.asmx/HasEntry?_memberId=" & _id
        _url = StripUrl(_url)
        xmldoc.Load(_url)
        Dim _retstr As String = ""
        Dim _showAs As String = ""
        If Not xmldoc.GetElementsByTagName("MEMBERSHIP_ID")(0) Is Nothing Then
            Try
                _showAs = xmldoc.GetElementsByTagName("SHOW_AS")(0).ChildNodes(0).Value
            Catch ex As Exception

            End Try
            Dim _displayAs As String = xmldoc.GetElementsByTagName("DISPLAY_AS")(0).ChildNodes(0).Value
            If _showAs = "" Then
                _retstr = _displayAs
            Else
                _retstr = _showAs
            End If
        End If
        Return _retstr
    End Function
    <WebMethod()> _
    <ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Xml, XmlSerializeString:=True)> _
    Public Function GetAddMember(ByVal _memberId As String) As String

        Dim xmldoc As New XmlDocument
        'Dim _url As String = "http://localhost:63154/NationalShow.asmx/GetSingleExhibitor?_memberId=" & _memberId
        Dim _url As String = "http://web.adga.org/NSEntry2/wsAdmin.asmx/GetSingleExhibitor?_memberId=" & _memberId
        _url = StripUrl(_url)
        xmldoc.Load(_url)
        Dim _retstr As String = ""
        If Not xmldoc.GetElementsByTagName("MEMBERSHIP_ID")(0) Is Nothing Then
            _retstr = _retstr & Trim(xmldoc.GetElementsByTagName("MEMBERSHIP_ID")(0).ChildNodes(0).Value) & ";"
            _retstr = _retstr & Trim(xmldoc.GetElementsByTagName("CUSTOMER_ID")(0).ChildNodes(0).Value) & ";"
            _retstr = _retstr & Trim(xmldoc.GetElementsByTagName("DISPLAY_AS")(0).ChildNodes(0).Value) & ";"
            _retstr = _retstr & Trim(xmldoc.GetElementsByTagName("SHOW_AS")(0).ChildNodes(0).Value)
        End If
        Return _retstr
    End Function

    <ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Xml, XmlSerializeString:=True)> _
    Public Function GetMemberInfo(ByVal _id As String) As String

        Dim xmldoc As New XmlDocument
        'Dim _url As String = "http://localhost:63154/NationalShow.asmx/GetMembershipInformation?_MembershipID=" & _id
        Dim _url As String = "http://web.adga.org/NSEntry2/wsAdmin.asmxx/GetMembershipInformation?_MembershipID=" & _id
        _url = StripUrl(_url)
        xmldoc.Load(_url)
        Dim _retstr As String = ""
        If Not xmldoc.GetElementsByTagName("MEMBERSHIP_ID")(0) Is Nothing Then
            _retstr = _retstr & Trim(xmldoc.GetElementsByTagName("MEMBERSHIP_ID")(0).ChildNodes(0).Value) & ";"
            Dim _namestr As String
            Try
                Dim _namearr() As String = Trim(xmldoc.GetElementsByTagName("MEMBERSHIP_NAME")(0).ChildNodes(0).Value).Split(",")
                _namestr = _namearr(1) & " " & _namearr(0)
            Catch ex As Exception
                _namestr = Trim(xmldoc.GetElementsByTagName("MEMBERSHIP_NAME")(0).ChildNodes(0).Value)
            End Try
            _retstr = _retstr & _namestr & ";"
            Try
                _retstr = _retstr & Trim(xmldoc.GetElementsByTagName("TATTOO")(0).ChildNodes(0).Value) & ";"
            Catch ex As Exception
                _retstr = _retstr & ";"
            End Try
            Try
                _retstr = _retstr & Trim(xmldoc.GetElementsByTagName("HERDNAME")(0).ChildNodes(0).Value) & ";"
            Catch ex As Exception
                _retstr = _retstr & ";"
            End Try
            _retstr = _retstr & Trim(xmldoc.GetElementsByTagName("MEMBERSHIP_CODE")(0).ChildNodes(0).Value) & ";"
            _retstr = _retstr & Trim(xmldoc.GetElementsByTagName("DUES_YEAR")(0).ChildNodes(0).Value)
        End If
        Return _retstr
    End Function
    Function StripUrl(ByVal _oldUrl As String) As String
        Dim _newUrl As String
        _newUrl = _oldUrl.Replace("""", "")
        _newUrl = _newUrl.Replace("'", "")
        Return _newUrl
    End Function

End Class
