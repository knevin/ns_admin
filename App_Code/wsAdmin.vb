﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.OleDb
Imports System.Data
Imports System.Data.Odbc
Imports System.IO
Imports ADODB
Imports System
Imports System.Xml
Imports System.Web.Script.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class wsAdmin
     Inherits System.Web.Services.WebService

    Public _dsn As String = "ADGA_STAGE"
    'Public _dsn As String = "ROSS"
    Public dbConn As New ADODB.Connection
    Protected Shared ConnectionString As String
    Dim dsGetGoatHistory As DataSet
    Dim dsGoatHistory As DataSet

    <WebMethod()> Public Function ATest() As String
        Dim dbConnection = New ADODB.Connection
        'dbConnection.Provider = "MSDAORA.1"
        'dbConnection.Provider = "MSORCL"
        ConnectionString = "Data Source=" & _dsn & ";user id=ROSSPROD;password=Qxb$72Q;"
        'dbConnection.CursorLocation = ADODB.CursorLocationEnum.adUseClient
        dbConnection.ConnectionString = ConnectionString
        Try
            dbConnection.Open()
        Catch ex As Exception
            Return "Exception: " & ex.ToString
        End Try
        dbConnection.Close()
        Return "OK"
    End Function

    '<WebMethod()> Public Function ATest() As String
    '    Dim dbConnection As New ADODB.Connection
    '    With dbConnection
    '        .Provider = "MSDASQL"
    '        .CursorLocation = ADODB.CursorLocationEnum.adUseClient
    '        '.ConnectionString = "data source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=10.23.30.24)(PORT=1521))(CONNECT_DATA =(SERVER=DEDICATED)(SERVICE_NAME = ROSS)));user id=ROSSPROD;password=Qxb$72Q"
    '        '.ConnectionString = "data source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=110.23.30.181)(PORT=1521))(CONNECT_DATA =(SERVER=DEDICATED)(SERVICE_NAME = ROSS1)));user id=ROSSPROD;password=Qxb$72Q"
    '        .ConnectionString = "data source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=10.25.156.243)(PORT=1521))(CONNECT_DATA =(SERVER=DEDICATED)(SERVICE_NAME = ORCL)));user id=ROSSPROD;password=Qxb$72Q"
    '        Try
    '            .Open()
    '            dbConn = dbConnection
    '        Catch ex As Exception
    '            Return (ex.Message)
    '        End Try

    '    End With

    '    Return "OK"
    'End Function

    'Sub OLDGetDBConnection()
    '    Dim dbConnection As New ADODB.Connection
    '    With dbConnection
    '        .Provider = "MSDAORA.1"
    '        .CursorLocation = ADODB.CursorLocationEnum.adUseClient
    '        '.ConnectionString = "data source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.10)(PORT=1521))(CONNECT_DATA =(SERVER=DEDICATED)(SERVICE_NAME = ROSS)));user id=ROSSPROD;password=Qxb$72Q"
    '        .ConnectionString = "data source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=10.23.30.181)(PORT=1521))(CONNECT_DATA =(SERVER=DEDICATED)(SERVICE_NAME = ROSS3)));user id=ROSSPROD;password=Qxb$72Q"
    '        Try
    '            .Open()
    '            dbConn = dbConnection
    '        Catch ex As Exception

    '        End Try

    '    End With
    'End Sub
    Sub GetDBConnection()

        'Dim dbConn As New ADODB.Connection
        'dbConn.Provider = "MSDAORA.1"
        If (dbConn.State > 0) Then
            dbConn.Close()
        End If

        'ConnectionString = "Data Source=ROSS3;user id=ROSSPROD;password=Qxb$72Q;"
        ConnectionString = "Data Source=" & _dsn & ";user id=ROSSPROD;password=Qxb$72Q;"
        dbConn.CursorLocation = ADODB.CursorLocationEnum.adUseClient
        dbConn.ConnectionString = ConnectionString
        Try
            dbConn.Open()
        Catch ex As Exception
            'MsgBox("Exception: " & ex.ToString)
        End Try
    End Sub
    <WebMethod()> Public Function CustomerBalance(ByVal _customerID As String, ByVal _folderID As String) As Double

        Dim _ds As New DataSet("CUSTBAL")
        Dim _oa As New OleDb.OleDbDataAdapter

        Dim _sql As New System.Text.StringBuilder
        _sql.Append("SELECT NVL(SUM(AMOUNT),0) FROM GENERAL_LEDGER G WHERE G.FOLDER_PAGE_ID NOT IN (SELECT ID FROM FOLDER_PAGE WHERE FOLDER_ID =" & _folderID & ") AND CUSTOMER_ID =" & _customerID)

        GetDBConnection()
        Dim _cmd As New ADODB.Command
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = _sql.ToString
            '.Parameters.Append(.CreateParameter(, ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInput, 20, _folderID))

            'Dim _customer As New ROSSModules.clsCustomer
            '_customer.LoadByID(_customerID, Now)
            'Dim _lookupCustomer As String = _customer.Sponsor("ID")
            'If _lookupCustomer = "" Then _lookupCustomer = _customerID

            '.Parameters.Append(.CreateParameter(, ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInput, 20, _customerID))

            _oa.Fill(_ds, .Execute, "BALDUE")
            _oa.Dispose()
        End With

        If _ds.Tables(0).Rows.Count > 0 Then
            If Not Double.TryParse(_ds.Tables(0).Rows(0).Item(0).ToString, CustomerBalance) Then
                CustomerBalance = 0
            End If
        End If
        _ds.Dispose()
        dbConn.Close()
        Return CustomerBalance
    End Function
    <WebMethod()> Public Function GetFolderDetail(ByVal _FolderID As String) As DataSet
        Dim _tempDS As New DataSet("FOLDER_DETAIL")
        Dim sql As StringBuilder = New StringBuilder
        Dim _oa As New OleDb.OleDbDataAdapter
        Dim customerID As String = ""
        Dim _cmd As New ADODB.Command
        GetDBConnection()
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText

            sql.Append("SELECT   /*+ ORDERED */ TO_CHAR (f.ID) folder_id, TRUNC (NVL (e.date_received, f.date_created)) received_date,e.postmark, fs.web_description, fs.description, ")
            sql.Append("membership_id,f.date_completed,fs.code,f.STATEMENT_NOTE, c.ID CUSTOMER_ID FROM folder_status fs, folder f, envelope e, customer c  WHERE(e.ID = f.envelope_id) AND fs.ID = f.folder_status_id ")
            sql.Append("AND fs.code <> 'X' AND F.ID = " & _FolderID & " AND c.ID = f.customer_id AND c.effdt = (SELECT MAX (effdt) FROM customer c2 WHERE c2.ID = c.ID AND c2.effdt <= SYSDATE) AND c.effdt_status = 'A' ORDER BY 1 DESC")
            .CommandText = sql.ToString
            _oa.Fill(_tempDS, .Execute, "FOLDER")
            If (_tempDS.Tables(0).Rows.Count > 0) Then
                customerID = _tempDS.Tables("FOLDER").Rows(0)("CUSTOMER_ID")
            Else ' have to get customer id some other way

            End If


            sql.Length = 0
            sql.Append("SELECT  FOLDER_PAGE_COMMENT DESCRIPTION, NVL(GL.QTY,0) QTY,	NVL(GL.UNIT_PRICE,0) UNIT_PRICE,NVL(GL.AMOUNT,0) AMOUNT, FP.ID FOLDER_PAGE_ID FROM FOLDER_PAGE FP, ")
            sql.Append("GENERAL_LEDGER GL WHERE FP.FOLDER_ID = " & _FolderID & " AND   GL.FOLDER_PAGE_ID (+) = FP.ID AND   NVL(FP.HIDE_PAGE, 'N') <> 'Y' AND   DOCUMENT_STATUS_ID NOT IN (1,5) ORDER BY FOLDER_PAGE_COMMENT")
            .CommandText = sql.ToString
            _oa.Fill(_tempDS, .Execute, "FOLDER_PAGE")

            sql.Length = 0
            sql.Append("SELECT fpd.folder_page_id, FPD.ID FOLDER_PAGE_DOCUMENT_ID FROM folder_page_document fpd, ross_printed_documents rpd,(SELECT DISTINCT fp.ID fp_1 FROM folder_page fp, general_ledger gl ")
            sql.Append("WHERE fp.folder_id = " & _FolderID & " AND gl.folder_page_id = fp.ID AND fp.hide_page <> 'Y' AND fp.document_status_id NOT IN (1, 5)) alias_13 WHERE(fpd.ID = rpd.folder_page_document_id) ")
            sql.Append("AND rpd.date_printed = (SELECT MAX (date_printed) FROM ross_printed_documents rpd2 WHERE rpd2.folder_page_document_id = rpd.folder_page_document_id) AND alias_13.fp_1 = fpd.folder_page_id")
            .CommandText = sql.ToString
            _oa.Fill(_tempDS, .Execute, "FOLDER_PAGE_DOCUMENT")

            .CommandText = "SELECT SUM(AMOUNT) AMOUNT FROM GENERAL_LEDGER WHERE FOLDER_PAGE_ID IN (SELECT ID FROM FOLDER_PAGE WHERE FOLDER_ID = " & _FolderID & ")"
            _oa.Fill(_tempDS, .Execute, "GENERAL_LEDGER")

        End With

        Dim _dt As New DataTable("CUSTOMER")
        Dim _rs As ADODB.Recordset
        With _cmd
            .CommandText = "SELECT * FROM MEMBERSHIP_VIEW WHERE CUSTOMER_ID = " & customerID

            _rs = .Execute
            _oa.Fill(_dt, _rs)
        End With
        _rs.Close()
        _oa.Dispose()
        _rs = Nothing
        _oa = Nothing
        _cmd = Nothing

        _tempDS.Tables.Add(_dt)
        dbConn.Close()
        Return _tempDS
    End Function

    <WebMethod()> Public Function WriteAdd(ByVal _stmnt As String) As String
        Dim _cmd As New ADODB.Command
        GetDBConnection()
        Try
            _cmd.ActiveConnection = dbConn
            _cmd.CommandType = ADODB.CommandTypeEnum.adCmdText
            _cmd.CommandText = _stmnt
            _cmd.Execute()
        Catch ex As Exception
            _cmd = Nothing
            Return "FAILURE:" & ex.Message
        End Try
        Return "OK"
    End Function

    <WebMethod()> Public Function WriteNSSponsor(ByVal _vars As String) As String
        Dim _cmd As New ADODB.Command
        Dim _arr() As String = _vars.Split("|")
        Dim _awardNum As String = _arr(0)
        Dim _sponsor As String = _arr(1)
        Dim _memId As String = _arr(2)
        Dim _email As String = _arr(3)
        Dim _web As String = _arr(4)
        GetDBConnection()
        Try
            _cmd.ActiveConnection = dbConn
            _cmd.CommandType = ADODB.CommandTypeEnum.adCmdText
            _cmd.CommandText = "UPDATE ROSSPROD.NS_SPONSORS  SET SPONSOR_NAME='" & _sponsor & "',SPONSOR_ID='" & _memId & "',SPONSOR_EMAIL='" & _email & "',SPONSOR_WEB='" & _web & "' WHERE AWARD_NUM = " & _awardNum
            _cmd.Execute()
        Catch ex As Exception
            _cmd = Nothing
            Return "FAILURE:" & ex.Message
        End Try
        Return "OK"
    End Function

    <WebMethod()> Public Function GetSponsors(ByVal _breed As String) As DataSet
        Dim _oa As New OleDb.OleDbDataAdapter
        Dim _cmd As New ADODB.Command
        Dim _arrBreed() As String = {"ALPINE", "LAMANCHA", "NIGERIAN DWARF", "NUBIAN", "OBERHASLI", "RECORDED GRADE", "SAANEN", "SABLE", "TOGGENBURG"}
        Dim i As Integer = 0
        GetDBConnection()

        Dim _ds As New DataSet("NSSponsors")
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            If _breed = "ALL" Then
                For i = 0 To 8
                    .CommandText = "SELECT * FROM NS_SPONSORS WHERE BREED = '" & _arrBreed(i) & "' AND YEAR = 2012 ORDER BY AWARD_NUM"
                    Try
                        _oa.Fill(_ds, .Execute, _arrBreed(i))
                    Catch ex As Exception

                    End Try
                Next i
            Else
                .CommandText = "SELECT * FROM NS_SPONSORS WHERE BREED = '" & _breed & "' AND YEAR = 2012 ORDER BY AWARD_NUM"
                Try
                    _oa.Fill(_ds, .Execute, _breed)
                Catch ex As Exception

                End Try
            End If
        End With
        _oa.Dispose()
        _oa = Nothing
        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing

        Return _ds
    End Function
    '<WebMethod()> Public Function BuildSubArray() As String
    '    Dim _data(9) As String

    '_data(0)= "99964"
    '_data(1)= "1121834"
    '_data(2)= "985754"
    '_data(3)= "1179281"
    '_data(4)= "PL1387853"
    '_data(5)= "LITTLE-BUNCH PD D ENGEDI"
    '_data(6)= "MILKER"
    '_data(7)= "LaMancha 5 And 6 Year Old Milker"
    '_data(8)= "12"
    '    _data(9) = "L"
    '    Dim _retval As String = WriteSubstitute(_data, 767)
    '    Return _retval
    'End Function

    '<WebMethod()> Public Function BuildPlaceDt() As String
    '    Dim _ds As New DataSet
    '    Dim _retval As String = ""
    '    Dim _colArr() As String = {"ClassNum", "EntryNum", "Placing"}
    '    Dim _ucolArr() As String = {"UClassNum", "UEntryNum", "UPlacing"}
    '    Dim _dtp As New DataTable
    '    Dim _dtu As New DataTable
    '    Dim _col As DataColumn
    '    Dim row As DataRow
    '    Dim i As Integer = 0
    '    For i = 0 To _colArr.GetUpperBound(0) ' Placing data table
    '        _col = New DataColumn
    '        _col.DataType = Type.GetType("System.String")
    '        _col.ColumnName = _colArr(i)
    '        _dtp.Columns.Add(_col)
    '    Next
    '    For i = 0 To _ucolArr.GetUpperBound(0) ' Udder data table
    '        _col = New DataColumn
    '        _col.DataType = Type.GetType("System.String")
    '        _col.ColumnName = _ucolArr(i)
    '        _dtu.Columns.Add(_col)
    '    Next
    '    row = _dtp.NewRow()
    '    row("ClassNum") = "1"
    '    row("EntryNum") = "28"
    '    row("Placing") = "1"
    '    _dtp.Rows.Add(row)
    '    row = _dtp.NewRow()
    '    row("ClassNum") = "1"
    '    row("EntryNum") = "32"
    '    row("Placing") = "2"
    '    _dtp.Rows.Add(row)
    '    row = _dtp.NewRow()
    '    row("ClassNum") = "1"
    '    row("EntryNum") = "34"
    '    row("Placing") = "3"
    '    _dtp.Rows.Add(row)
    '    row = _dtp.NewRow()
    '    row("ClassNum") = "1"
    '    row("EntryNum") = "37"
    '    row("Placing") = "4"
    '    _dtp.Rows.Add(row)
    '    row = _dtp.NewRow()
    '    row("ClassNum") = "1"
    '    row("EntryNum") = "47"
    '    row("Placing") = "5"
    '    _dtp.Rows.Add(row)
    '    row = _dtp.NewRow()
    '    row("ClassNum") = "1"
    '    row("EntryNum") = "48"
    '    row("Placing") = "6"
    '    _dtp.Rows.Add(row)

    '    row = _dtu.NewRow()
    '    row("UClassNum") = "1"
    '    row("UEntryNum") = "28"
    '    row("UPlacing") = "1"
    '    _dtu.Rows.Add(row)
    '    row = _dtu.NewRow()
    '    row("UClassNum") = "1"
    '    row("UEntryNum") = "34"
    '    row("UPlacing") = "2"
    '    _dtu.Rows.Add(row)
    '    row = _dtu.NewRow()
    '    row("UClassNum") = "1"
    '    row("UEntryNum") = "32"
    '    row("UPlacing") = "3"
    '    _dtu.Rows.Add(row)

    '    _ds.Tables.Add(_dtp)
    '    _ds.Tables.Add(_dtu)
    '    _ds.Tables(0).TableName = "Placing"
    '    _ds.Tables(1).TableName = "Udder"

    '    _retval = WriteNSPlacing(_ds)
    '    Return _retval
    'End Function

    '<WebMethod()> Public Function BuildFakeDT() As String
    '    Dim _ds As New DataSet
    '    Dim _retval As String = ""
    '    Dim _colmember() As String = {"Membership_Id", "Customer_Id", "Display_As", "DHU", "Comments", "Owner_Ids"}
    '    Dim _col11() As String = {"1502399", "140819", "REINHOLTZ, KATHY & KACEY", "null", "Would be wonderful if pens were next to each other", "137798"}
    '    Dim _colnames() As String = {"Customer_Id", "Breeder_Id", "Sire_Id", "Dam_Id", "Goat_Id", "Group_Type", "Reg_Num", "Name", "Class_Number", "Class_Name", "Charge"}
    '    Dim _col0() As String = {"140819", "140819", "140819", "140819", "140819", "140819", "140819", "null", "null", "null", "null", "null"}
    '    Dim _col1() As String = {"137798", "137798", "137798", "137798", "123421", "null", "140819", "null", "null", "null", "null", "null"}
    '    Dim _col2() As String = {"1318826", "1237012", "1237012", "1237012", "1060803", "null", "null", "null", "null", "null", "null", "null"}
    '    Dim _col3() As String = {"1236363", "1164369", "1164369", "1164369", "1067015", "null", "null", "null", "null", "null", "null", "null"}
    '    Dim _col4() As String = {"1351178", "1351181", "1311485", "1311488", "1236363", "null", "null", "null", "null", "null", "null", "1311484"}
    '    Dim _col5() As String = {"D", "D", "D", "D", "M", "G", "G", "E", "E", "E", "E", "D"}
    '    Dim _col6() As String = {"PN1559383", "PN1559386", "PN1519766", "PN1519769", "PN1444802", "PN1445449", "", "", "", "", "", "AB1519765"}
    '    Dim _col7() As String = {"DREAM AWAY'S KC'S GOLDILOCKS", "DREAM AWAY'S SILVER MIST", "DREAM AWAY'S WISH UPON A STAR", "DREAM AWAY'S ON CLOUD NINE", "KATMAR'S MS KC'S BE DAZZLED", "JESTA FARM HADLYME HERO", "", "", "", "", "", "DREAM AWAY'S KC'S CALLIOPE"}
    '    Dim _col8() As String = {"71", "71", "75", "75", "80", "77", "76", "null", "null", "null", "null", "111"}
    '    Dim _col9() As String = {"Nubian Junior Kid", "Nubian Junior Kid", "Nubian Senior Yearling", "Nubian Senior Yearling", "Nubian 3 Year Old Milker", "Nubian Junior Get of Sire", "Nubian Best 3 Junior Does", "2 Animal Pens", "1 Tack Pens", "3 Parking Pass", "3 Wine and Cheese", "Oberhasli Senior Yearling"}
    '    Dim _col10() As String = {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "13"}
    '    Dim _dt As New DataTable
    '    Dim _mt As New DataTable
    '    Dim _col As DataColumn
    '    Dim i As Integer = 0
    '    For i = 0 To _colnames.GetUpperBound(0)
    '        _col = New DataColumn
    '        _col.DataType = Type.GetType("System.String")
    '        _col.ColumnName = _colnames(i)
    '        _dt.Columns.Add(_col)
    '    Next
    '    Dim _newRow As DataRow
    '    For i = 0 To _col0.GetUpperBound(0)
    '        _newRow = _dt.NewRow()
    '        _newRow("Customer_Id") = _col0(i)
    '        _newRow("Breeder_Id") = _col1(i)
    '        _newRow("Sire_Id") = _col2(i)
    '        _newRow("Dam_Id") = _col3(i)
    '        _newRow("Goat_Id") = _col4(i)
    '        _newRow("Group_Type") = _col5(i)
    '        _newRow("Reg_Num") = _col6(i)
    '        _newRow("Name") = _col7(i)
    '        _newRow("Class_Number") = _col8(i)
    '        _newRow("Class_Name") = _col9(i)
    '        _newRow("Charge") = _col10(i)
    '        _dt.Rows.Add(_newRow)
    '    Next

    '    For i = 0 To _colmember.GetUpperBound(0)
    '        _col = New DataColumn
    '        _col.DataType = Type.GetType("System.String")
    '        _col.ColumnName = _colmember(i)
    '        _mt.Columns.Add(_col)
    '    Next
    '    Dim _mRow As DataRow
    '    _mRow = _mt.NewRow()
    '    _mRow("Membership_Id") = _col11(0)
    '    _mRow("Customer_Id") = _col11(1)
    '    _mRow("Display_As") = _col11(2)
    '    _mRow("DHU") = _col11(3)
    '    _mRow("Comments") = _col11(4)
    '    _mt.Rows.Add(_mRow)

    '    _ds.Tables.Add(_mt)
    '    _ds.Tables(0).TableName = "Member"
    '    _ds.Tables.Add(_dt)
    '    _ds.Tables(1).TableName = "Order"

    '    _retval = WriteNSOrder(_ds)
    '    Return _retval
    'End Function

    <WebMethod()> Public Function WriteNSOrder(ByVal _ds As DataSet) As String
        Dim _oa As New OleDb.OleDbDataAdapter
        Dim _cmd As New ADODB.Command
        Dim _mt As DataTable = _ds.Tables("Member")
        Dim _dt As DataTable = _ds.Tables("Order")
        Dim _membershipId As String = _mt.Rows(0).Item("Membership_Id").ToString
        Dim _customerId As String = _mt.Rows(0).Item("Customer_Id").ToString
        Dim _nsRows As String = ""
        Dim _fpRows As String = ""
        Dim _folderId As String = ""
        Dim i As Integer = 0
        Dim _orderTotal As Double = 0.0
        For i = 0 To _dt.Rows.Count - 1
            _orderTotal = _orderTotal + CDbl(_dt.Rows(i).Item("Charge")) ' Add all of the charges together
        Next

        GetDBConnection()
        _cmd.ActiveConnection = dbConn
        _cmd.CommandType = ADODB.CommandTypeEnum.adCmdText
        dbConn.BeginTrans() ' Start transaction
        _nsRows = WriteNSTable(_cmd, _ds) ' Write the NS Show and Animal Entries
        If _nsRows.StartsWith("FAILURE:") Then ' Rollback the db and and return the error
            dbConn.RollbackTrans()
            _cmd = Nothing
            Return _nsRows
        End If
        If _orderTotal > 0 Then ' Only write folder/folder pages if there's an actual charge on the order
            _folderId = WriteNSFolder(_cmd, _customerId) ' Open and Write the Folder/Envelope Entries
            If _folderId.StartsWith("FAILURE:") Then ' Rollback the db and and return the error
                dbConn.RollbackTrans()
                _cmd = Nothing
                Return _nsRows
            End If
            _fpRows = WriteNSFolderPage(_cmd, _ds, _folderId) ' Write the Folder Page Entries
            If _fpRows.StartsWith("FAILURE:") Then ' Rollback the db and and return the error
                dbConn.RollbackTrans()
                _cmd = Nothing
                Return _nsRows
            End If
        End If
        dbConn.CommitTrans()
        _cmd = Nothing
        If _orderTotal = 0 Then ' if there ar no actual charge on the order, we have to provide a folder id to let caller know it's ok
            _folderId = "0"
        End If
        Return "SUCCESS:" & _folderId
    End Function

    Private Function WriteNSFolderPage(ByVal _cmd As ADODB.Command, ByVal _ds As DataSet, ByVal _folderId As String) As String
        Dim _mt As DataTable = _ds.Tables("Member")
        Dim _dt As DataTable = _ds.Tables("Order")
        Dim _folderPageId As String = ""
        Dim _generalLedgerId As String = ""
        Dim _glCode As String = ""
        Dim _fpDescription As String = ""
        Dim _insertStr As String = ""
        Dim _sponsorId As String = _mt.Rows(0).Item("Customer_Id").ToString.Trim
        Dim _rs As ADODB.Recordset
        Dim i As Integer = 0

        ' See if the member is sponsored - if so, we'll charge the sponsors account
        _cmd.CommandText = "SELECT SPONSOR FROM CUSTOEMR_VIEW WHERE ID='" & _mt.Rows(0).Item("Customer_Id").ToString.Trim & "'"
        _rs = _cmd.Execute
        If _rs.RecordCount > 0 Then _sponsorId = _rs.Fields(0).Value

        For i = 0 To _dt.Rows.Count - 1
            If CDbl(_dt.Rows(i).Item("Charge")) > 0 Then ' It's a new entry - write a folder page for it
                ' FOLDER PAGE LOGIC
                If _dt.Rows(i).Item("Reg_Num").ToString <> "null" Then
                    _fpDescription = Year(Now) & " NS " & _dt.Rows(i).Item("Reg_Num").ToString & " " & _dt.Rows(i).Item("Name").ToString.ToUpper.Replace("'", "''") & " " & _dt.Rows(i).Item("Class_Name").ToString.ToUpper
                Else
                    _fpDescription = Year(Now) & " NS " & _dt.Rows(i).Item("Class_Name").ToString.ToUpper
                End If
                _folderPageId = GetNextId("FOLDER_PAGE_ID")
                _insertStr = "INSERT INTO FOLDER_PAGE VALUES (" & _folderPageId & ",81,SYSDATE,-110,7," & _folderId & ",'','" & _fpDescription & "','','ADGA\CITRIX01',SYSDATE,-110)"
                _cmd.CommandText = _insertStr
                Try
                    _cmd.Execute()
                Catch ex As Exception
                    Return "FAILURE:" & ex.Message
                End Try
                _insertStr = ""

                ' GENERAL LEDGER LOGIC
                Select Case _dt.Rows(i).Item("Group_Type").ToString
                    Case "M"
                        _glCode = "7731"
                    Case "D"
                        _glCode = "7731"
                    Case "G"
                        _glCode = "7735"
                    Case "E"
                        _glCode = GetGlCode(_dt.Rows(i).Item("Class_Name").ToString.ToUpper)
                End Select
                _generalLedgerId = GetNextId("GENERAL_LEDGER_ID")
                _insertStr = "INSERT INTO GENERAL_LEDGER VALUES (" & _generalLedgerId & ",SYSDATE," & _folderPageId & "," & _glCode & ",-" & _dt.Rows(i).Item("Charge") & "," & _sponsorId & ",-110,SYSDATE,-110,SYSDATE,'','" & _fpDescription & "','',1," & _dt.Rows(i).Item("Charge") & ",'')"
                _cmd.CommandText = _insertStr
                Try
                    _cmd.Execute()
                Catch ex As Exception
                    Return "FAILURE:" & ex.Message
                End Try
                _fpDescription = ""
                _folderPageId = ""
                _generalLedgerId = ""
                _insertStr = ""
                _glCode = ""
            End If
        Next
        Return "SUCCESS:"
    End Function

    Private Function WriteNSFolder(ByVal _cmd As ADODB.Command, ByVal _customerId As String) As String
        Dim _folderStr As String = ""
        Dim _envelopeStr As String = ""
        Dim _folderId As String = ""
        Dim _envelopeId As String = ""

        _folderId = GetNextId("FOLDER_ID")
        _envelopeId = GetNextId("ENVELOPE_ID")
        If _folderId = "0" Then
            Return "FAILURE:COULD NOT OPEN FOLDER"
        Else 'Open a folder
            ' Insert the Folder and Envelope into the db
            _folderStr = "INSERT INTO FOLDER VALUES ( " & _folderId & "," & _envelopeId & ",   17,  'N'," & _customerId & ",  SYSDATE,  -110, SYSDATE, -110, '', '', '', '',  '', '',  'N', '', 'N','',   'N', '',  '','')"
            _envelopeStr = "INSERT INTO ENVELOPE VALUES (" & _envelopeId & ",  SYSDATE,-110,SYSDATE,-110,SYSDATE,'','','','N', SYSDATE)"
            Try
                _cmd.CommandText = _folderStr
                _cmd.Execute()
                _cmd.CommandText = _envelopeStr
                _cmd.Execute()
            Catch ex As Exception
                dbConn.RollbackTrans()
                _cmd = Nothing
                Return "FAILURE:" & ex.Message
            End Try
        End If
        Return _folderId
    End Function

    Function GetGlCode(ByVal desc As String) As String
        If InStr(desc, "INDIVIDUAL ENTRY") > 0 Then
            Return "7731"
        ElseIf InStr(desc, "GROUP ENTRY") > 0 Then
            Return "7735"
        ElseIf InStr(desc, "ANIMAL PENS") > 0 Then
            Return "7732"
        ElseIf InStr(desc, "TACK PENS") > 0 Then
            Return "7744"
        ElseIf InStr(desc, "NON-SHOW") > 0 Then
            Return "7736"
        ElseIf InStr(desc, "EXHIBIT") > 0 Then
            Return "7759"
        ElseIf InStr(desc, "WELCOME") > 0 Then
            Return "7733"
        ElseIf InStr(desc, "BOOK") > 0 Then
            Return "7734"
        ElseIf InStr(desc, "WINE") > 0 Then
            Return "7755"
        ElseIf InStr(desc, "VENDOR") > 0 Then
            Return "7930"
        Else
            Return "000"
        End If
    End Function

    Private Function WriteNSTable(ByVal _cmd As ADODB.Command, ByVal _ds As DataSet) As String
        Dim _mt As DataTable = _ds.Tables("Member")
        Dim _dt As DataTable = _ds.Tables("Order")
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim _totalRows As Integer = 0
        Dim _rowAdded As Integer = 0
        Dim _equipNameArr() As String = {"Animal Pens Lg", "Animal Pens Sm", "Tack Pens", "Non-Show Pens", "Wine and Cheese", "Showbook Advertising", "Vendor Tables"}
        Dim _equipArr(6, 1) As String
        Dim _insertStr As String = ""
        Dim _membershipId As String = _mt.Rows(0).Item("Membership_Id").ToString.Trim
        Dim _customerId As String = _mt.Rows(0).Item("Customer_Id").ToString.Trim
        Dim _displayAs As String = _mt.Rows(0).Item("Display_As").ToString
        Dim _DHU As String = _mt.Rows(0).Item("DHU").ToString
        Dim _comments As String = _mt.Rows(0).Item("Comments").ToString
        Dim _owners As String = _mt.Rows(0).Item("Owner_Ids").ToString.Trim

        Try
            _cmd.CommandText = "DELETE FROM ROSSPROD.NS_ANIMAL_ENTRY WHERE MEMBERSHIP_ID = '" & _membershipId & "'"
            _cmd.Execute()
            _cmd.CommandText = "DELETE FROM ROSSPROD.NS_SHOW_ENTRY WHERE MEMBERSHIP_ID = '" & _membershipId & "'"
            _cmd.Execute()
        Catch ex As Exception
            Return "FAILURE: DELETE PREVIOUS ORDER"
        End Try

        For i = 0 To _dt.Rows.Count - 1
            If _dt.Rows(i).Item("Group_Type") = "M" Or _dt.Rows(i).Item("Group_Type") = "D" Or _dt.Rows(i).Item("Group_Type") = "G" Then
                Dim _str As New StringBuilder
                ' it goes to the animal table
                _str.Append("INSERT INTO ROSSPROD.NS_ANIMAL_ENTRY VALUES (")
                _str.Append(Year(Now) & ",") ' Show Year
                _str.Append("To_Date('" & Now().ToShortDateString & "','MM/DD/YYYY'),") ' Entry Date
                _str.Append("NS_ENTRY_ID.NEXTVAL,") ' Entry Num
                _str.Append("null,") ' Checked In
                _str.Append("'" & _membershipId & "',") ' Membership Id
                _str.Append("'" & _customerId & "',") ' ' Customer Id
                _str.Append("'" & _displayAs.Replace("'", "''") & "',") ' Display As
                _str.Append("'" & _DHU.Replace("'", "''") & "',") ' Herd Unit
                _str.Append("null,") ' Barcode
                _str.Append(_dt.Rows(i).Item("Breeder_Id").ToString.Trim & ",") ' Breeder Id
                _str.Append(_dt.Rows(i).Item("Sire_Id").ToString.Trim & ",") ' Sire Id
                _str.Append(_dt.Rows(i).Item("Dam_Id").ToString.Trim & ",") ' Dam Id
                _str.Append(_dt.Rows(i).Item("Goat_Id").ToString.Trim & ",") ' Goat Id
                _str.Append("'" & _dt.Rows(i).Item("Reg_Num").ToString.Trim & "',") ' Reg Num
                _str.Append("'" & _dt.Rows(i).Item("Name").ToString.Replace("'", "''") & "',") ' Goat Name
                _str.Append("'" & _dt.Rows(i).Item("Group_Type").ToString.Trim & "',") 'Group Type
                _str.Append("'" & _dt.Rows(i).Item("Class_Name").ToString.Trim & "',") ' Class Name
                _str.Append(_dt.Rows(i).Item("Class_Number").ToString.Trim & ",") ' Class number
                If _dt.Rows(i).Item("Class_Name").ToString.IndexOf("Sable") > -1 Then
                    _str.Append("'C',") ' Breed Sable
                ElseIf _dt.Rows(i).Item("Class_Name").ToString.IndexOf("Oberhasli") > -1 Then
                    _str.Append("'B',") ' Breed Oberhasli
                ElseIf _dt.Rows(i).Item("Class_Name").ToString.IndexOf("Nigerian") > -1 Then
                    _str.Append("'D',") ' Breed Nigerian Dwarf
                Else
                    _str.Append("'" & Mid(_dt.Rows(i).Item("Class_Name").ToString, 1, 1) & "',") ' Breed             
                End If
                _str.Append("null,") ' Placing
                _str.Append("null,") ' Udder
                _str.Append("null,") ' Awards
                _str.Append("'WEB'") ' Entry
                _insertStr = _str.ToString & ")"
                _cmd.CommandText = _insertStr
                Try
                    _cmd.Execute(_rowAdded)
                    _totalRows = _totalRows + _rowAdded
                Catch ex As Exception
                    Return "FAILURE:" & _insertStr & ":" & ex.Message
                End Try
            End If
        Next
        ' Put together the Equipment insert
        For i = 0 To _equipNameArr.GetUpperBound(0)
            _equipArr(i, 0) = _equipNameArr(i)
            _equipArr(i, 1) = 0
        Next
        Dim _equipStr As String = ""
        Dim _equipCount As String = ""
        Dim _hasEquipment As Boolean = False

        Dim info As Byte() ' TEMP
        Dim _fwrite As New System.Text.StringBuilder ' TEMP
        Dim fs As FileStream ' TEMP
        Dim fstring As String = "C:\Temp\NSAdmin" ' TEMP
        If File.Exists(fstring) = False Then ' TEMP
            fs = File.Create(fstring) ' TEMP
            fs.Close() ' TEMP
        End If ' TEMP
        fs = File.OpenWrite(fstring) ' TEMP

        For i = 0 To _dt.Rows.Count - 1
            If _dt.Rows(i).Item("Group_Type") = "E" Then

                _fwrite.Length = 0 ' TEMP
                _fwrite.Append(_dt.Rows(i).Item(9).ToString() & vbCrLf) ' TEMP
                _totalRows = _totalRows + 1
                _hasEquipment = True
                _equipStr = _dt.Rows(i).Item(9).ToString

                _equipCount = _equipStr.Substring(0, _equipStr.IndexOf(" "))
                For j = 0 To _equipArr.GetUpperBound(0)

                    _fwrite.Length = 0 ' TEMP
                    _fwrite.Append("COMPARING " & _equipStr & " TO " & _equipArr(j, 0)) ' TEMP

                    If _equipStr.Contains(_equipArr(j, 0)) Then
                        _fwrite.Append(" MATCH" & vbCrLf) ' TEMP

                        _equipArr(j, 1) = CDbl(_equipArr(j, 1)) + CDbl(_equipCount)
                        Exit For
                    Else
                        _fwrite.Append(" NO MATCH" & vbCrLf) ' TEMP
                    End If
                    info = New UTF8Encoding(True).GetBytes(_fwrite.ToString & vbCrLf) ' TEMP
                    fs.Write(info, 0, info.Length) ' TEMP

                Next
            End If
        Next

        fs.Close() ' TEMP

        Dim _str1 As New StringBuilder
        _str1.Append("INSERT INTO ROSSPROD.NS_SHOW_ENTRY VALUES (")
        _str1.Append(Year(Now) & ",") ' Show Year
        _str1.Append("To_Date('" & Now().ToShortDateString & "','MM/DD/YYYY'),") ' Entry Date
        _str1.Append("'" & _membershipId & "',") ' Membership Id
        _str1.Append("'" & _customerId & "',") ' ' Customer Id
        _str1.Append("'" & _displayAs.Replace("'", "''") & "',") ' Display As
        _str1.Append("'" & _DHU.Replace("'", "''") & "',") ' Herd Unit
        For i = 0 To _equipArr.GetUpperBound(0)
            _str1.Append(_equipArr(i, 1) & ",")
        Next
        _str1.Append("'" & _comments.Replace("'", "''") & "',") ' Comments
        _str1.Append("'WEB',")  ' Entry
        _str1.Append("'" & _owners & "'") ' Entry
        _insertStr = _str1.ToString & ")"
        _cmd.CommandText = _insertStr
        Try
            _cmd.Execute(_rowAdded)
            _totalRows = _totalRows + _rowAdded
        Catch ex As Exception
            Return "FAILURE:" & _insertStr & ":" & ex.Message
        End Try
        Return "SUCCESS:" & _totalRows
    End Function

    Public Function GetNextId(ByVal _type As String) As String
        Dim _rs As ADODB.Recordset
        Dim _cmd As New ADODB.Command
        Dim _retval As String
        If dbConn.State = 0 Then GetDBConnection()
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT " & _type & ".NEXTVAL FROM DUAL"
            _rs = .Execute
            If Not _rs.EOF Then
                If _type = "FOLDER_ID" Or _type = "ENVELOPE_ID" Then
                    _retval = Year(Now) & "0" & _rs.Fields(0).Value
                Else
                    _retval = _rs.Fields(0).Value
                End If
            Else
                _retval = "0"
            End If
        End With
        _rs.Close()
        _rs = Nothing
        _cmd = Nothing
        Return _retval
    End Function

    <WebMethod()> Public Function WriteNSPlacing(ByVal _ds As DataSet) As String
        Dim _cmd As New ADODB.Command
        Dim _hasUdder As Boolean = True
        Dim _totalRows As Integer = 0
        Dim _rowInserted As Integer = 0
        Dim _pt As New DataTable
        Dim _ut As New DataTable
        Dim _at As New DataTable
        If _ds.Tables.Count = 3 Then
            _pt = _ds.Tables("Placing")
            _ut = _ds.Tables("Udder")
            _at = _ds.Tables("Awards")
        ElseIf _ds.Tables.Count = 2 Then
            _pt = _ds.Tables("Placing")
            _ut = _ds.Tables("Udder")
        Else
            _pt = _ds.Tables("Placing")
            _hasUdder = False
        End If
        Dim i As Integer = 0
        GetDBConnection()
        _cmd.ActiveConnection = dbConn
        _cmd.CommandType = ADODB.CommandTypeEnum.adCmdText
        dbConn.BeginTrans() ' Start transaction

        For i = 0 To _pt.Rows.Count - 1
            _cmd.CommandText = "UPDATE NS_ANIMAL_ENTRY SET PLACING = " & _pt.Rows(i).Item("Placing") & " WHERE CLASS_NUM=" & _pt.Rows(i).Item("ClassNum") & " AND ENTRY_NUM=" & _pt.Rows(i).Item("EntryNum")
            Try
                _cmd.Execute(_rowInserted)
                _totalRows = _totalRows + _rowInserted
            Catch ex As Exception
                dbConn.RollbackTrans()
                _cmd = Nothing
                Return "FAILURE:" & _cmd.CommandText & ":" & ex.Message
            End Try
        Next

        If _hasUdder = True Then
            For i = 0 To _ut.Rows.Count - 1
                _cmd.CommandText = "UPDATE NS_ANIMAL_ENTRY SET UDDER= 'U" & _ut.Rows(i).Item("UPlacing") & "' WHERE CLASS_NUM=" & _ut.Rows(i).Item("UClassNum") & " AND ENTRY_NUM=" & _ut.Rows(i).Item("UEntryNum")
                Try
                    _cmd.Execute(_rowInserted)
                    _totalRows = _totalRows + _rowInserted
                Catch ex As Exception
                    dbConn.RollbackTrans()
                    _cmd = Nothing
                    Return "FAILURE:" & _cmd.CommandText & ":" & ex.Message
                End Try
            Next
        End If
        dbConn.CommitTrans()
        _cmd = Nothing
        Return "SUCCESS:" & _totalRows
    End Function

    <WebMethod()> Public Function WriteNSCheckin(ByVal _ds As DataSet) As String
        Dim _cmd As New ADODB.Command
        Dim _totalRows As Integer = 0
        Dim _rowInserted As Integer = 0
        Dim _ct As New DataTable
        _ct = _ds.Tables("Checkin")
        Dim i As Integer = 0
        GetDBConnection()
        _cmd.ActiveConnection = dbConn
        _cmd.CommandType = ADODB.CommandTypeEnum.adCmdText
        dbConn.BeginTrans() ' Start transaction

        For i = 0 To _ct.Rows.Count - 1
            '_cmd.CommandText = "UPDATE NS_ANIMAL_ENTRY SET CHECKED_IN='Y' WHERE CLASS_NUM=" & _ct.Rows(i).Item("ClassNum") & " AND ENTRY_NUM=" & _ct.Rows(i).Item("EntryNum")
            _cmd.CommandText = "UPDATE NS_ANIMAL_ENTRY SET CHECKED_IN='Y' WHERE ENTRY_NUM=" & _ct.Rows(i).Item("EntryNum")
            Try
                _cmd.Execute(_rowInserted)
                _totalRows = _totalRows + _rowInserted
            Catch ex As Exception
                dbConn.RollbackTrans()
                _cmd = Nothing
                Return "FAILURE:" & _cmd.CommandText & ":" & ex.Message
            End Try
        Next

        dbConn.CommitTrans()
        _cmd = Nothing
        Return "SUCCESS:" & _totalRows
    End Function

    <WebMethod()> Public Function GetExhibitors() As DataSet
        Dim _oa As New OleDb.OleDbDataAdapter
        Dim _cmd As New ADODB.Command
        Dim _strb As New StringBuilder
        GetDBConnection()

        Dim _ds As New DataSet("Exhibitors")
        With _cmd

            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText

            _strb.Length = 0
            _strb.Append("SELECT TRIM(MEMBERSHIP_ID) MEMBERSHIP_ID, TRIM(CUSTOMER_ID) CUSTOMER_ID, DISPLAY_AS, SHOW_AS FROM ROSSPROD.NS_SHOW_ENTRY ORDER BY SHOW_AS")
            .CommandText = _strb.ToString
            Try
                _oa.Fill(_ds, .Execute, "Exhibitors")
            Catch ex As Exception

            End Try

        End With
        _oa.Dispose()
        _oa = Nothing
        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing

        Return _ds
    End Function
    <WebMethod()> Public Function GetSingleExhibitor(ByVal _memberId As String) As DataSet
        Dim _oa As New OleDb.OleDbDataAdapter
        Dim _cmd As New ADODB.Command
        Dim _strb As New StringBuilder
        GetDBConnection()
        Dim _ds As New DataSet("Exhibitor")
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            _strb.Length = 0
            _strb.Append("SELECT TRIM(MEMBERSHIP_ID) MEMBERSHIP_ID, TRIM(CUSTOMER_ID) CUSTOMER_ID, DISPLAY_AS, SHOW_AS FROM ROSSPROD.NS_SHOW_ENTRY WHERE MEMBERSHIP_ID='" & _memberId & "'")
            .CommandText = _strb.ToString
            Try
                _oa.Fill(_ds, .Execute, "Exhibitor")
            Catch ex As Exception

            End Try

        End With
        _oa.Dispose()
        _oa = Nothing
        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing
        Return _ds
    End Function
    <WebMethod()> Public Function GetAddGoatInfo(ByVal _regId As String) As String
        Dim _rs As ADODB.Recordset = Nothing
        Dim _cmd As New ADODB.Command
        Dim _strb As New StringBuilder
        Dim _tempReturn As String
        GetDBConnection()
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            _strb.Length = 0
            _strb.Append("SELECT GV.ID, GV.DISPLAY_NAME, GB.SIRE_ID, GB.DAM_ID, GB.DATE_OF_BIRTH, GB.BREEDER_ID, GV.TATTOO FROM GOAT_VIEW GV, GOAT_BIRTHS GB WHERE GV.BIRTH_ID=GB.ID AND GV.OLD_REG_NUM='" & _regId & "'")
            .CommandText = _strb.ToString
            Try
                _rs = .Execute
                If Not _rs.EOF Then
                    _tempReturn = _rs.Fields(0).Value & ";" & _rs.Fields(1).Value & ";" & _rs.Fields(2).Value & ";" & _rs.Fields(3).Value & ";" & _rs.Fields(4).Value & ";" & _rs.Fields(5).Value & ";" & _rs.Fields(6).Value
                Else
                    _tempReturn = "NONE"
                End If
            Catch ex As Exception
                _tempReturn = "NONE"
            End Try

        End With
        _rs.Close()
        _rs = Nothing
        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing

        Return _tempReturn
    End Function

    <WebMethod()> Public Function HasEntry(ByVal _memberId As String) As DataSet
        Dim _oa As New OleDb.OleDbDataAdapter
        Dim _cmd As New ADODB.Command
        Dim _strb As New StringBuilder
        GetDBConnection()

        Dim _ds As New DataSet("NSEntries")
        With _cmd

            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            _strb.Append("SELECT SHOW_YEAR, ENTRY_DATE, ENTRY_NUM, CHECKED_IN, TRIM(MEMBERSHIP_ID) MEMBERSHIP_ID,TRIM(CUSTOMER_ID) CUSTOMER_ID,DISPLAY_AS,")
            _strb.Append("SHOWING_AS, BARCODE, TRIM(BREEDER_ID) BREEDER_ID, TRIM(SIRE_ID) SIRE_ID, TRIM(DAM_ID) DAM_ID, TRIM(GOAT_ID) GOAT_ID, TRIM(REG_NUM) REG_NUM,")
            _strb.Append("TRIM(ANM_NAME) ANM_NAME, TRIM(CLASS_TYPE) CLASS_TYPE, TRIM(CLASS_NAME) CLASS_NAME, TRIM(CLASS_NUM) CLASS_NUM, TRIM(BREED_CODE) BREED_CODE, PLACING, UDDER, AWARDS, ENTRY ")
            _strb.Append("FROM ROSSPROD.NS_ANIMAL_ENTRY WHERE MEMBERSHIP_ID='" & _memberId & "'")
            .CommandText = _strb.ToString
            Try
                _oa.Fill(_ds, .Execute, "GOATS")
            Catch ex As Exception

            End Try
            _strb.Length = 0
            '_strb.Append("SELECT SHOW_YEAR, ENTRY_DATE, TRIM(MEMBERSHIP_ID) MEMBERSHIP_ID, TRIM(CUSTOMER_ID) CUSTOMER_ID, DISPLAY_AS, SHOW_AS,")
            '_strb.Append("ANIMAL_PENS_SM ANIMAL_PENS_SMALL, ANIMAL_PENS_MED ANIMAL_PENS_MEDIUM, ANIMAL_PENS_LG ANIMAL_PENS_LARGE, NS_PENS NON_SHOW_PENS, WELCOME_DINNER, BOOKS, WINE_CHEESE, VENDOR_TABLES, NOTES, ENTRY, TRIM(OWNERS) OWNERS ")
            '_strb.Append("FROM ROSSPROD.NS_SHOW_ENTRY WHERE MEMBERSHIP_ID='" & _memberId & "'")
            _strb.Append("SELECT * FROM ROSSPROD.NS_SHOW_ENTRY WHERE MEMBERSHIP_ID='" & _memberId & "'")
            .CommandText = _strb.ToString
            Try
                _oa.Fill(_ds, .Execute, "MEMBER")
            Catch ex As Exception

            End Try

        End With
        _oa.Dispose()
        _oa = Nothing
        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing

        Return _ds
    End Function


    <WebMethod()> _
    Public Function GetGoatInfo(ByVal _GoatID As String) As DataSet
        Dim _oa As New OleDb.OleDbDataAdapter
        Dim _cmd As New ADODB.Command
        GetDBConnection()
        If _GoatID.Length = 8 Then _GoatID = InsertPurebred(_GoatID)

        GetGoatInfo = New DataSet("GoatInfo")
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT DISPLAY_NAME,TO_CHAR(DATE_OF_BIRTH,'DD-MON-YYYY') DOB, SEX, MEMBERSHIP_ID, DATEGOATDIED(OLD_REG_NUM) DEAD, TATTOO FROM ROSSPROD.GOAT_VIEW, ROSSPROD.CUSTOEMR_VIEW, ROSSPROD.GOAT_BIRTHS WHERE OLD_REG_NUM = '" & _GoatID.ToUpper & "' AND CUSTOEMR_VIEW.ID=GOAT_VIEW.CUSTOMER_ID AND GOAT_VIEW.BIRTH_ID=GOAT_BIRTHS.ID"
            Try
                _oa.Fill(GetGoatInfo, .Execute, "GOAT_VIEW")
            Catch ex As Exception
                GetGoatInfo = New DataSet("GoatInfo")
            End Try
        End With
        _oa.Dispose()
        _oa = Nothing

        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing

    End Function
    <WebMethod()> _
    Public Function CheckMemberTattoo(ByVal _customerID As String, ByVal _tattoo As String) As String
        Dim _cmd As New ADODB.Command
        Dim _RS As ADODB.Recordset
        Dim _retval As String = "BAD"
        Dim _ownerID As String = ""
        GetDBConnection()

        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT CUSTOMER_ID FROM CUSTOMER_TATTOO_VIEW ct WHERE ct.TATTOO='" & _tattoo & "'"
            _RS = .Execute()
        End With
        If Not _RS.EOF Then
            _ownerID = CStr(_RS.Fields(0).Value)
            If _ownerID <> _customerID Then
                _RS.Close()
                With _cmd
                    .ActiveConnection = dbConn
                    .CommandType = ADODB.CommandTypeEnum.adCmdText
                    .CommandText = "SELECT GRANTEE_CUSTOMER_ID FROM CUST_TATTOO_AUTH_VIEW WHERE TATTOO_OWNER_CUSTOMER_ID = (SELECT CUSTOMER_ID FROM CUSTOMER_TATTOO_VIEW ct WHERE ct.TATTOO='" & _tattoo & "')"
                    _RS = .Execute()
                End With
                If Not _RS.EOF Then
                    Dim i As Integer
                    For i = 0 To _RS.RecordCount - 1
                        If CStr(_RS.Fields(0).Value) = _customerID Then
                            _retval = "OK"
                            Exit For
                        End If
                        _RS.MoveNext()
                    Next
                End If
            Else
                _retval = "OK"
            End If
        Else ' Nobody owns it - like Z10 or Y20
            _retval = "OK"
        End If
        _RS.Close()
        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing
        Return _retval

    End Function
    <WebMethod()> _
    Public Function NSGetGoatHistory(ByVal _GoatID As String) As DataSet
        Dim _oa As New OleDb.OleDbDataAdapter
        Dim _cmd As New ADODB.Command
        GetDBConnection()
        If _GoatID.Length = 8 Then _GoatID = InsertPurebred(_GoatID)
        dsGetGoatHistory = New DataSet("GOAT")
        With _cmd

            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT GOAT.EFFDT,MEMBERSHIP_ID FROM ROSSPROD.GOAT,ROSSPROD.CUSTOEMR_VIEW WHERE OLD_REG_NUM = '" & _GoatID.ToUpper & "' AND GOAT.CUSTOMER_ID=CUSTOEMR_VIEW.ID AND ((GOAT.DEAD IS NULL) OR (GOAT.DEAD='N')) ORDER BY EFFDT ASC"

            Try
                _oa.Fill(dsGetGoatHistory, .Execute, "GOAT")
            Catch ex As Exception

            End Try

        End With
        _oa.Dispose()
        _oa = Nothing

        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing
        Return dsGoatHistory
    End Function
    <WebMethod()> Public Function GetSponsee(ByVal MemberID As String) As DataSet

        Dim _oa As New OleDb.OleDbDataAdapter
        Dim _cmd As New ADODB.Command

        Dim ds As DataSet = New DataSet("Sponsor")
        If (MemberID = "0140186") Then
            Dim myIds As Data.DataTable = ds.Tables.Add("Ids")
            Dim myDr As Data.DataRow
            With myIds
                .Columns.Add("MEMBERSHIP_ID", Type.GetType("System.String"))
            End With
            myDr = myIds.NewRow()
            myDr("MEMBERSHIP_ID") = "0800839"
            myIds.Rows.Add(myDr)
            myDr = myIds.NewRow()
            myDr("MEMBERSHIP_ID") = "0864256"
            myIds.Rows.Add(myDr)
            myDr = myIds.NewRow()
            myDr("MEMBERSHIP_ID") = "1501797"
            myIds.Rows.Add(myDr)
        ElseIf (MemberID = "0880401") Then
            Dim myIds As Data.DataTable = ds.Tables.Add("Ids")
            Dim myDr As Data.DataRow
            With myIds
                .Columns.Add("MEMBERSHIP_ID", Type.GetType("System.String"))
            End With
            myDr = myIds.NewRow()
            myDr("MEMBERSHIP_ID") = "0238477"
            myIds.Rows.Add(myDr)
            myDr = myIds.NewRow()
            myDr("MEMBERSHIP_ID") = "0083188"
            myIds.Rows.Add(myDr)
            myDr = myIds.NewRow()
            myDr("MEMBERSHIP_ID") = "1501797"
            myIds.Rows.Add(myDr)
        ElseIf (MemberID = "0000406") Then
            Dim myIds As Data.DataTable = ds.Tables.Add("Ids")
            Dim myDr As Data.DataRow
            With myIds
                .Columns.Add("MEMBERSHIP_ID", Type.GetType("System.String"))
            End With
            myDr = myIds.NewRow()
            myDr("MEMBERSHIP_ID") = "0000414"
            myIds.Rows.Add(myDr)
            myDr = myIds.NewRow()
            myDr("MEMBERSHIP_ID") = "1482938"
            myIds.Rows.Add(myDr)
            myDr = myIds.NewRow()
            myDr("MEMBERSHIP_ID") = "1521018"
            myIds.Rows.Add(myDr)
        Else
            GetDBConnection()
            With _cmd

                .ActiveConnection = dbConn
                .CommandType = ADODB.CommandTypeEnum.adCmdText
                .CommandText = "select membership_id, display_as from custoemr_view where id in (select csv.sponsee from custoemr_view cv, customer_sponsor_view csv " & _
                " where cv.membership_id='" & MemberID & "' and cv.id=csv.sponsor)"
                Try
                    _oa.Fill(ds, .Execute, "Sponsor")
                Catch ex As Exception
                    ds = New DataSet("Sponsor")
                End Try

            End With
            _oa.Dispose()
            _oa = Nothing

            _cmd = Nothing
            dbConn.Close()
            dbConn = Nothing
        End If
        Return ds
    End Function

    <WebMethod()> Public Function AreLinked(ByVal SubMemberID As String, ByVal OwnMemberID As String) As Boolean
        Dim _subSponsor As String = ""
        Dim _ownSponsor As String = ""
        Dim _RS As ADODB.Recordset
        Dim _cmd As New ADODB.Command
        GetDBConnection()

        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT MEMBERSHIP_ID FROM CUSTOEMR_VIEW WHERE ID IN " & _
                "(SELECT SPONSOR FROM CUSTOMER_SPONSOR WHERE SPONSEE = (SELECT ID FROM CUSTOEMR_VIEW WHERE MEMBERSHIP_ID='" & SubMemberID & "'))"
            _RS = .Execute()
            If _RS.RecordCount > 0 Then
                _subSponsor = _RS.Fields(0).Value.ToString
            End If
            _RS.Close()
            If _subSponsor = OwnMemberID Then
                _cmd = Nothing
                dbConn.Close()
                dbConn = Nothing
                Return True
            End If
            .CommandText = "SELECT MEMBERSHIP_ID FROM CUSTOEMR_VIEW WHERE ID IN " & _
                "(SELECT SPONSOR FROM CUSTOMER_SPONSOR WHERE SPONSEE = (SELECT ID FROM CUSTOEMR_VIEW WHERE MEMBERSHIP_ID='" & OwnMemberID & "'))"
            _RS = .Execute()
            If _RS.RecordCount > 0 Then
                _ownSponsor = _RS.Fields(0).Value.ToString
            End If
            _RS.Close()
            _RS = Nothing
        End With
        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing
        If _subSponsor = "" And _ownSponsor = "" Then
            Return False
        Else
            If _subSponsor = _ownSponsor Then
                Return True
            ElseIf _ownSponsor = SubMemberID Then
                Return True
            Else
                Return False
            End If
        End If
    End Function


    Private Function LengthOwned(ByVal _ds As Data.DataSet, ByVal _submitter As String, ByVal _edate As Date) As Boolean
        Dim _dsdate As Date = Now
        Dim _arr(0, 1) As String
        Dim _longestDate As Integer = 0
        Dim i As Integer = 0
        For i = 0 To _ds.Tables(0).Rows.Count - 1
            If _ds.Tables(0).Rows(i).Item("MEMBERSHIP_ID") = _submitter Then
                _dsdate = _ds.Tables(0).Rows(i).Item("EFFDT")
                _longestDate = _edate.Subtract(_dsdate).Days
            End If
        Next
        If _longestDate > 3 Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function IsSponsor(ByVal _submitter As String, ByVal _sponsee As String) As Boolean
        Dim _sponsorList As New Data.DataSet
        Dim i As Integer
        Dim _retval As Boolean = False
        _sponsorList = GetSponsee(_submitter)
        If (_sponsorList.Tables(0).Rows.Count > 0) Then
            For i = 0 To _sponsorList.Tables(0).Rows.Count - 1
                If (_sponsorList.Tables(0).Rows(i).Item("MEMBERSHIP_ID").ToString = _sponsee) Then
                    _retval = True
                    Exit For
                End If
            Next
        End If
        Return _retval
    End Function
    <WebMethod()> _
    Public Function GetBreeder(ByVal _sireID As String, ByVal _damID As String, ByVal _dob As Date, ByVal _submitter As String) As String
        Dim _retval As String = ""
        'Dim _eDate As Date = DateAdd(DateInterval.Day, -141, _dob)
        'Dim _sDate As Date = DateAdd(DateInterval.Day, -159, _dob)
        Dim _eDate As Date = DateAdd(DateInterval.Day, -148, _dob)
        Dim _sDate As Date = DateAdd(DateInterval.Day, -154, _dob)
        Dim _damBreeder As String = ""
        Dim _damOwner As String = ""
        Dim _returndamOwner As String = ""
        Dim _returndamBreeder As String = ""
        Dim _returnsireOwner As String = ""
        Dim _sireOwner As String = ""
        Dim _numOwners As Integer = 0
        Dim _damDS As New Data.DataSet
        Dim _sireDS As New Data.DataSet
        Dim i As Integer = 0
        ''''''''''''  PROCESS: DAM OWNER ''''''''''''
        ' Get the owner of the dam at Date Of Birth
        _damDS = GetGoatHistory(_damID)
        If _damDS.Tables(0).Rows.Count > 0 Then
            For i = 0 To _damDS.Tables(0).Rows.Count - 1
                If (_damDS.Tables(0).Rows(i).Item("EFFDT") <= _dob) Then
                    'If (_damDS.Tables(0).Rows(i).Item("EFFDT") <= _eDate) Then
                    _damOwner = _damDS.Tables(0).Rows(i).Item("MEMBERSHIP_ID").ToString
                End If
            Next
        End If
        ' Is submitter that owner?
        If (_damOwner = _submitter) Then ' Yes - Set submitter as owner and allow to continue
            _returndamOwner = "SUB_DAM_OWNER:" & _damOwner
        End If
        If (_returndamOwner = "") Then ' Submitter was not owner. Is submitter sponsor of that owner?
            Dim _sponsorList As New Data.DataSet
            _sponsorList = GetSponsee(_submitter)
            If (_sponsorList.Tables(0).Rows.Count > 0) Then
                For i = 0 To _sponsorList.Tables(0).Rows.Count - 1
                    If (_sponsorList.Tables(0).Rows(i).Item("MEMBERSHIP_ID").ToString = _damOwner) Then ' Yes  - Set owner as owner and allow to continue
                        _returndamOwner = "SPO_DAM_OWNER:" & _sponsorList.Tables(0).Rows(i).Item("MEMBERSHIP_ID").ToString
                        Exit For
                    End If
                Next
            End If
        End If
        If (_returndamOwner = "") Then ' Submitter was not sponsor. Is submitter SPONSEE of owner?
            Dim _sponseeList As New Data.DataSet
            _sponseeList = GetSponsee(_damOwner)
            If (_sponseeList.Tables(0).Rows.Count > 0) Then
                For i = 0 To _sponseeList.Tables(0).Rows.Count - 1
                    If (_sponseeList.Tables(0).Rows(i).Item("MEMBERSHIP_ID").ToString = _submitter) Then ' Yes  - Set owner as owner and allow to continue
                        _returndamOwner = "SPO_DAM_OWNER:" & _damOwner
                        Exit For
                    End If
                Next
            End If
        End If
        If (_returndamOwner = "") Then ' Submitter was not SPONSEE. DO submitter and owner have the same sponsor?
            Dim _areLinked As Boolean = AreLinked(_submitter, _damOwner)
            If _areLinked = True Then
                _returndamOwner = "SPO_DAM_OWNER:" & _damOwner
            End If
        End If

        If (_returndamOwner = "") Then ' No - STOP REGISTRATION
            _returndamOwner = "NOT_DAM_OWNER:" & _damOwner
        End If
        ''''''''''''  PROCESS: DAM BREEDER ''''''''''''
        ' Get the owner(s) of the dam at DOB minus 141 to 159 days
        Dim _breederSet(0) As String
        '_breederSet(0) = _damDS.Tables(0).Rows(0).Item("MEMBERSHIP_ID")
        For i = 0 To _damDS.Tables(0).Rows.Count - 1
            If (_damDS.Tables(0).Rows(i).Item("EFFDT") <= _sDate) Then
                _breederSet(0) = _damDS.Tables(0).Rows(i).Item("MEMBERSHIP_ID")
            End If
            If (_damDS.Tables(0).Rows(i).Item("EFFDT") >= _sDate) And (_damDS.Tables(0).Rows(i).Item("EFFDT") <= _eDate) And (_breederSet(0) <> _damDS.Tables(0).Rows(i).Item("MEMBERSHIP_ID")) Then
                ReDim Preserve _breederSet(_breederSet.Length)
                _breederSet(_breederSet.Length - 1) = _damDS.Tables(0).Rows(i).Item("MEMBERSHIP_ID")
            End If
        Next
        ' Was submitter owner during the entire period?
        If (_breederSet.Length = 1) And (_breederSet(0) = _submitter) Then ' Yes - Set submitter as breeder and allow to continue
            _returndamBreeder = "SUB_DAM_BREEDER:" & _submitter
        ElseIf (_breederSet.Length > 1) Then ' No
            Dim _isOk As Boolean = LengthOwned(_damDS, _submitter, _eDate)
            If _isOk = True Then
                _returndamBreeder = "SUB_DAM_BREEDER:" & _submitter
            Else
                _returndamBreeder = "MUL_DAM_BREEDER:" & _breederSet(0) ' Yes - UNKNOWN BREEDER - STOP REGISTRATION
                For i = 1 To _breederSet.Length - 1
                    _returndamBreeder = _returndamBreeder & "|" & _breederSet(i)
                Next
            End If
        Else ' Someone else was owner of dam during that entire period - Set that someone as breeder and allow to continue  
            _returndamBreeder = "OTH_DAM_BREEDER:" & _breederSet(0)
        End If

        ''''''''''''  PROCESS: SIRE OWNER ''''''''''''
        _sireDS = GetGoatHistory(_sireID)

        ' Get the owner(s) of the sire at DOB minus 141 to 159 days
        Dim _sireSet(0) As String
        _sireSet(0) = _sireDS.Tables(0).Rows(0).Item("MEMBERSHIP_ID")
        For i = 0 To _sireDS.Tables(0).Rows.Count - 1
            If (_sireDS.Tables(0).Rows(i).Item("EFFDT") <= _eDate) Then
                _sireSet(0) = _sireDS.Tables(0).Rows(i).Item("MEMBERSHIP_ID")
            End If
            If (_sireDS.Tables(0).Rows(i).Item("EFFDT") >= _sDate) And (_sireDS.Tables(0).Rows(i).Item("EFFDT") <= _eDate) And (_sireSet(0) <> _sireDS.Tables(0).Rows(i).Item("MEMBERSHIP_ID")) Then
                ReDim Preserve _sireSet(_sireSet.Length)
                _sireSet(_breederSet.Length - 1) = _sireDS.Tables(0).Rows(i).Item("MEMBERSHIP_ID")
            End If
        Next
        ' Was submitter owner during the entire period?
        If (_sireSet.Length = 1) And (_sireSet(0) = _submitter) Then ' Yes - Set submitter as breeder and allow to continue
            _returnsireOwner = "SUB_SIRE_OWNER:" & _submitter
        ElseIf (_sireSet.Length > 1) Then ' No
            Dim _isOk As Boolean = LengthOwned(_sireDS, _submitter, _eDate)
            If _isOk = True Then
                _returnsireOwner = "SUB_SIRE_OWNER:" & _submitter
            Else
                _returnsireOwner = "MUL_SIRE_OWNER:" & _sireSet(0)
                For i = 1 To _sireSet.Length - 1
                    _returnsireOwner = _returnsireOwner & "|" & _sireSet(i)
                Next
            End If
        ElseIf (_sireSet.Length = 1) And (_sireSet(0) <> _submitter) Then ' The owner is another person
            ' See if the submitter is a sponsor of that owner
            Dim _issponsor As Boolean = IsSponsor(_submitter, _sireSet(0))
            If _issponsor = True Then
                _returnsireOwner = "SPO_SIRE_OWNER:" & _sireSet(0)
            Else
                ' See if the submitter is a SPONSEE of that owner
                Dim _issponsee As Boolean = IsSponsor(_sireSet(0), _submitter)
                If _issponsee = True Then
                    _returnsireOwner = "SPO_SIRE_OWNER:" & _sireSet(0)
                Else ' Submitter was not SPONSEE. DO submitter and owner have the same sponsor?
                    Dim _areLinked As Boolean = AreLinked(_submitter, _sireSet(0))
                    If _areLinked = True Then
                        _returnsireOwner = "SPO_SIRE_OWNER:" & _sireSet(0)
                    Else

                        _returnsireOwner = "NOT_SIRE_OWNER:" & _sireSet(0)
                    End If
                End If
            End If
        End If
        _retval = _returndamBreeder & ";" & _returndamOwner & ";" & _returnsireOwner & ";"
        Return _retval
    End Function

    Function InsertPurebred(ByVal _goatID As String) As String
        Dim theResult As Integer = 0
        If Integer.TryParse(_goatID.Substring(1, 1), theResult) Then
            If theResult >= 0 Then
                _goatID = "P" & _goatID
            End If
        End If
        InsertPurebred = _goatID
    End Function
    <WebMethod()> Public Function GetMemberInfo(ByVal CustNum As String) As DataSet
        Dim _oa As New OleDb.OleDbDataAdapter
        Dim _cmd As New ADODB.Command
        GetDBConnection()
        If (dbConn.State <> 1) Then
            Return New DataSet("Error")
        End If
        GetMemberInfo = New DataSet("Member")
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT cv.ID, cv.MEMBERSHIP_ID, cv.DISPLAY_AS, cv.STREET1, cv.STREET2, cv.STREET3, cv.CITY, cv.STATE, " & _
                            "cv.POSTAL_CODE, cp.PHONE_NUMBER, cv.EMAIL, ch.NAME " & _
                            "FROM CUSTOEMR_VIEW cv, CUSTOMER_PHONE_VIEW cp, HERD_VIEW ch " & _
                            "WHERE cv.ID = cp.CUSTOMER_ID and cv.ID = ch.CUSTOMER_ID (+) AND cp.PHONE_TYPE_ID = 1 AND cv.MEMBERSHIP_ID = '" & CustNum & "'"

            Try
                _oa.Fill(GetMemberInfo, .Execute, "Member")
            Catch ex As Exception
                GetMemberInfo = New DataSet("Member")
            End Try

        End With
        _oa.Dispose()
        _oa = Nothing
        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing
    End Function
    Private Function CreateCustomerDataTable() As DataTable

        Dim _dt As New DataTable("CUSTOMER")

        With _dt.Columns
            .Add("MEMBERSHIP_ID")
            .Add("MEMBERSHIP_NAME")

            .Add("MAIL_ADDRESS")
            .Add("PHYSICAL_ADDRESS")

            .Add("PHONE")
            .Add("FAX")
            .Add("EMAIL")
            .Add("WEBPAGE")
            .Add("MEMBERSHIP_TYPE")
            .Add("MEMBERSHIP_CODE")
            .Add("DUES_YEAR")
            .Add("TATTOO")
            .Add("HERDNAME")
            .Add("ACCOUNT_BALANCE")
            .Add("CITY")
            .Add("STATE")
            .Add("ZIPCODE")
            .Add("ADDRESS")

        End With

        Return _dt
    End Function

    <WebMethod()> _
    Public Function GetMembershipInformation(ByVal _MembershipID As String) As DataSet
        Return GetMembershipInfo(_MembershipID, Nothing)
    End Function
    Private Function GetMembershipInfo(ByVal _MembershipID As String, ByVal _CustomerID As String) As DataSet
        Dim _tempDS As New DataSet("MEMBERSHIP")
        'Dim _dt As DataTable = CreateCustomerDataTable()

        'Dim _customer As New NSEntry2.clsCustomer

        'If _MembershipID Is Nothing Then
        '    _customer.LoadByID(_CustomerID, Now)
        'Else
        '    _customer.LoadByMembership(_MembershipID, Now)
        'End If

        'Dim _DR As DataRow = _dt.NewRow
        '_DR("MEMBERSHIP_ID") = _customer.MembershipID

        'If _customer.CustomerID <> "" Then

        '    _DR("MEMBERSHIP_NAME") = _customer.CustomerName
        '    _DR("PHONE") = _customer.PrimaryPhone
        '    _DR("FAX") = _customer.PrimaryFax
        '    _DR("EMAIL") = _customer.EMail.ToLower
        '    _DR("WEBPAGE") = _customer.WebSite.ToLower
        '    _DR("DUES_YEAR") = _customer.LatestDuesYear

        '    '<COMMENT>  GET THE TATTOO INFORMAITON 
        '    _DR("TATTOO") = _customer.Tattoo(False)
        '    If CStr(_DR("TATTOO")) = "" Then
        '        _DR("TATTOO") = _customer.Tattoo(True, Now)
        '        If CStr(_DR("TATTOO")) = "" Then
        '            _DR("TATTOO") = _customer.Tattoo(True, Now, True)
        '            If CStr(_DR("TATTOO")) <> "" Then
        '                _DR("TATTOO") = CStr(_DR("TATTOO")) & " - Non-Member Lock"
        '            End If
        '        Else
        '            _DR("TATTOO") = CStr(_DR("TATTOO")) & " - Authorized"
        '        End If
        '    End If

        '    '<COMMENT>  GET THE HERD INFORMATION 
        '    _DR("HERDNAME") = _customer.Herd(False)
        '    If CStr(_DR("HERDNAME")) = "" Then
        '        _DR("HERDNAME") = _customer.Herd(True)
        '        If CStr(_DR("HERDNAME")) = "" Then
        '            _DR("HERDNAME") = _customer.Herd(True, Now, True)
        '            If CStr(_DR("HERDNAME")) <> "" Then
        '                _DR("HERDNAME") = CStr(_DR("HERDNAME")) & " - Non-Member Lock"
        '            End If
        '        Else
        '            _DR("HERDNAME") = CStr(_DR("HERDNAME")) & " - Authorized"
        '        End If
        '    End If

        '    Dim _MembershipType As New ROSSModules.clsMembershipTypes
        '    _MembershipType.LoadByID(_customer.MembershipTypeID)
        '    _DR("MEMBERSHIP_TYPE") = _MembershipType.Description
        '    _DR("MEMBERSHIP_CODE") = _MembershipType.MembershipCode

        '    '<COMMENT>  GET THE MAILING ADDRESS
        '    _DR("MAIL_ADDRESS") = _customer.AddressText(_customer.PrimayAddressTypeID)
        '    _DR("PHYSICAL_ADDRESS") = _customer.AddressText("4")
        '    _DR("ACCOUNT_BALANCE") = _customer.GetAccountBalance("0")

        '    _DR("CITY") = _customer.PrimaryAddressCity
        '    _DR("STATE") = _customer.PrimaryAddressState
        '    _DR("ZIPCODE") = _customer.PrimaryAddressZip
        '    _DR("ADDRESS") = _customer.PrimaryStreetAddress
        'End If

        '_dt.Rows.Add(_DR)

        '_tempDS.Tables.Add(_dt)
        Return _tempDS

    End Function

    <WebMethod()> _
    Public Function GetGoatHistory(ByVal _GoatID As String) As DataSet
        Dim _oa As New OleDb.OleDbDataAdapter
        Dim _cmd As New ADODB.Command
        GetDBConnection()
        If _GoatID.Length = 8 Then _GoatID = InsertPurebred(_GoatID)
        GetGoatHistory = New DataSet("GoatHistory")
        With _cmd

            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT GOAT.EFFDT,MEMBERSHIP_ID FROM ROSSPROD.GOAT,ROSSPROD.CUSTOEMR_VIEW WHERE OLD_REG_NUM = '" & _GoatID.ToUpper & "' AND GOAT.CUSTOMER_ID=CUSTOEMR_VIEW.ID AND ((GOAT.DEAD IS NULL) OR (GOAT.DEAD='N')) ORDER BY EFFDT ASC"

            Try
                _oa.Fill(GetGoatHistory, .Execute, "GOAT")
            Catch ex As Exception

            End Try

        End With
        _oa.Dispose()
        _oa = Nothing

        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing

    End Function

    <WebMethod()> Public Function GetGoatParents(ByVal _regId As String) As DataSet
        Dim _oa As New OleDb.OleDbDataAdapter
        Dim _cmd As New ADODB.Command
        If _regId.Length = 8 Then _regId = "P" & _regId
        _regId = _regId.ToUpper
        GetDBConnection()
        GetGoatParents = New DataSet("Goat")
        With _cmd

            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT gv.CUSTOMER_ID, GETBREEDERID(gv.ID) BREEDER_ID, gv.ID GOAT_ID, gv.OLD_REG_NUM, gv.DISPLAY_NAME GOAT_NAME, gb.SIRE_ID, GETGOATNAME(gb.SIRE_ID) SIRE_NAME, gb.DAM_ID, GETGOATNAME(gb.DAM_ID) DAM_NAME, gb.DATE_OF_BIRTH BIRTHDATE FROM GOAT_VIEW gv,GOAT_BIRTHS gb WHERE gv.OLD_REG_NUM='" & _regId & "' AND gb.ID = gv.BIRTH_ID"
            Try
                _oa.Fill(GetGoatParents, .Execute, "Goat")
            Catch ex As Exception
                GetGoatParents = New DataSet("Goat")
            End Try
        End With
        _oa.Dispose()
        _oa = Nothing
        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing
    End Function


    <WebMethod()> Public Function VerifyBoard(ByVal _memberID As String) As DataSet
        Dim _oa As New OleDb.OleDbDataAdapter
        Dim _cmd As New ADODB.Command
        GetDBConnection()

        VerifyBoard = New DataSet("Board")
        Dim _sql As String = "SELECT UNIQUE m.MEMBERSHIP_ID,m.ID CUSTOMER_ID, m.DISPLAY_AS FROM CUSTOEMR_VIEW m, CUSTOMER_COMMITTEE_VIEW c " & _
                           "WHERE(m.ID = c.CUSTOMER_ID) AND c.COMITTEE_ID in (10,55) AND m.MEMBERSHIP_ID='" & _memberID & "'"
        With _cmd

            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = _sql

            Try
                _oa.Fill(VerifyBoard, .Execute, "Board")
            Catch ex As Exception
                VerifyBoard = New DataSet("Board")
            End Try

        End With
        _oa.Dispose()
        _oa = Nothing

        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing
    End Function

    <WebMethod()> Public Function BuildGoatList(ByVal CustomerIds As String) As Data.DataSet
        Dim _oa As New OleDb.OleDbDataAdapter
        Dim _customerID As String = ""
        Dim _cmd As New ADODB.Command
        Dim _RS As New ADODB.Recordset
        Dim _tempDS As New DataSet("GOAT_SEARCH")
        GetDBConnection()
        If (dbConn.State <> 1) Then
            Return _tempDS
        End If
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT G.ID, G.OLD_REG_NUM, TRIM(GETGOATNAME(G.ID)) GOAT_NAME, TO_CHAR(GB.DATE_OF_BIRTH, 'MM/DD/YYYY') DATE_OF_BIRTH, GB.SIRE_ID, GB.DAM_ID, G.CUSTOMER_ID, GB.BREEDER_ID " & _
                            "FROM GOAT G, GOAT_BIRTHS GB WHERE  G.CUSTOMER_ID IN (" & CustomerIds & ") " & _
                            "AND G.EFFDT = (SELECT MAX(EFFDT) FROM GOAT G2  WHERE G2.ID = G.ID) AND  G.EFFDT_STATUS = 'A' " & _
                            "AND  GB.ID = G.BIRTH_ID AND G.SEX = 'F' ORDER BY 3"
            _oa.Fill(_tempDS, .Execute, "GOATS")
            _oa.Dispose()
        End With
        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing
        Return _tempDS
    End Function

    <WebMethod()> Public Function GetSubstitute(ByVal _regId As String) As Data.DataSet
        Dim _cmd As New ADODB.Command
        Dim _RS As New ADODB.Recordset
        Dim _tempDS As New DataSet("SUBSTITUTE")
        GetDBConnection()
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT G.ID, G.OLD_REG_NUM, TRIM(GETGOATNAME(G.ID)) GOAT_NAME, TO_CHAR(GB.DATE_OF_BIRTH, 'MM/DD/YYYY') DATE_OF_BIRTH, GB.SIRE_ID, GB.DAM_ID, G.CUSTOMER_ID, GB.BREEDER_ID " & _
                            "FROM GOAT G, GOAT_BIRTHS GB WHERE  G.OLD_REG_NUM ='" & _regId & "' " & _
                            "AND G.EFFDT = (SELECT MAX(EFFDT) FROM GOAT G2  WHERE G2.ID = G.ID) AND  G.EFFDT_STATUS = 'A' " & _
                            "AND  GB.ID = G.BIRTH_ID AND G.SEX = 'F' ORDER BY 3"
            Dim _oa As New OleDb.OleDbDataAdapter
            _oa.Fill(_tempDS, .Execute, "SUBSTITUTE")
            _oa.Dispose()
        End With
        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing
        Return _tempDS
    End Function

    <WebMethod()> Public Function WriteSubstitute(ByVal _data() As String, ByVal _entryNum As Integer) As String
        Dim _cmd As New ADODB.Command
        Dim _RS As New ADODB.Recordset
        Dim _errorStr As String = ""
        Dim _rowUpdated As Integer = 0
        GetDBConnection()
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            dbConn.BeginTrans() ' Start transaction
            _cmd.CommandText = "UPDATE NS_ANIMAL_ENTRY SET BREEDER_ID= '" & _data(0) & "', SIRE_ID = '" & _data(1) & "'," & _
                               "DAM_ID='" & _data(2) & "', GOAT_ID='" & _data(3) & "', REG_NUM='" & _data(4).ToUpper & "', " & _
                               "ANM_NAME='" & _data(5).ToUpper & "', CLASS_TYPE='" & _data(6).ToUpper & "', CLASS_NAME='" & _data(7).ToUpper & "', " & _
                               "CLASS_NUM='" & _data(8) & "', BREED_CODE='" & _data(9).ToUpper & "' WHERE ENTRY_NUM=" & _entryNum
            Try
                _cmd.Execute(_rowUpdated)
            Catch ex As Exception
                _errorStr = ex.Message
            End Try

        End With
        If _rowUpdated = 1 Then
            dbConn.CommitTrans()
            _cmd = Nothing
            dbConn.Close()
            dbConn = Nothing
            Return "SUCCESS:" & _rowUpdated
        Else
            dbConn.RollbackTrans()
            _cmd = Nothing
            dbConn.Close()
            dbConn = Nothing
            Return "FAILURE:" & _cmd.CommandText & ":" & _errorStr
        End If

    End Function

    <WebMethod()> Public Function WriteGroupSubstitute(ByVal _data() As String, ByVal _entryNum As Integer) As String
        Dim _cmd As New ADODB.Command
        Dim _RS As New ADODB.Recordset
        Dim _errorStr As String = ""
        Dim _rowUpdated As Integer = 0
        GetDBConnection()
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            dbConn.BeginTrans() ' Start transaction
            _cmd.CommandText = "UPDATE NS_ANIMAL_ENTRY SET REG_NUM='" & _data(0) & "', " & _
                               "ANM_NAME='" & _data(1) & "', CLASS_NAME='" & _data(2) & "', " & _
                               "CLASS_NUM='" & _data(3) & "', BREED_CODE='" & _data(4) & "' WHERE ENTRY_NUM=" & _entryNum
            Try
                _cmd.Execute(_rowUpdated)
            Catch ex As Exception
                _errorStr = ex.Message
            End Try

        End With
        If _rowUpdated = 1 Then
            dbConn.CommitTrans()
            _cmd = Nothing
            dbConn.Close()
            dbConn = Nothing
            Return "SUCCESS:" & _rowUpdated
        Else
            dbConn.RollbackTrans()
            _cmd = Nothing
            dbConn.Close()
            dbConn = Nothing
            Return "FAILURE:" & _cmd.CommandText & ":" & _errorStr
        End If

    End Function

    <WebMethod()> _
    Public Function GetRegAndName(ByVal _goatId As String) As String
        Dim _rs As ADODB.Recordset
        Dim _cmd As New ADODB.Command
        Dim _tempReturn As String
        GetDBConnection()
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT OLD_REG_NUM, TRIM(DISPLAY_NAME) DISPLAY_NAME FROM GOAT_VIEW WHERE ID=" & _goatId
            _rs = .Execute
            If Not _rs.EOF Then
                _tempReturn = _rs.Fields(0).Value & "," & _rs.Fields(1).Value
            Else
                _tempReturn = "NONE"
            End If
        End With
        _rs.Close()
        _rs = Nothing
        _cmd = Nothing
        Return _tempReturn
    End Function

    <WebMethod()> _
    Public Function GetCurrBalance(ByVal _membershipId As String) As Double
        Dim _rs As ADODB.Recordset
        Dim _cmd As New ADODB.Command
        Dim _sponsorId As String = ""
        Dim _currBalance As Double = 0.0
        GetDBConnection()
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT SPONSOR FROM CUSTOEMR_VIEW WHERE MEMBERSHIP_ID=" & _membershipId
            _rs = .Execute
            If Not _rs.EOF Then
                _sponsorId = _rs.Fields(0).Value
            End If
            _rs.Close()
            .CommandText = "SELECT SUM(AMOUNT) FROM GENERAL_LEDGER WHERE CUSTOMER_ID=" & _sponsorId
            _rs = .Execute
            If Not _rs.EOF Then
                _currBalance = _rs.Fields(0).Value
            End If
            _rs.Close()
        End With

        _rs = Nothing
        _cmd = Nothing
        Return _currBalance
    End Function

    <WebMethod()> _
    Public Function GetNSEntry(ByVal _entryNum As String) As String
        Dim _rs As ADODB.Recordset = Nothing
        Dim _cmd As New ADODB.Command
        Dim _tempReturn As String = ""
        GetDBConnection()
        With _cmd

            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT ENTRY_NUM, REG_NUM, ANM_NAME, NVL(SHOWING_AS,DISPLAY_AS) MEMBER, CLASS_NAME, CLASS_NUM FROM NS_ANIMAL_ENTRY WHERE ENTRY_NUM=" & _entryNum
            Try
                _rs = .Execute
                If Not _rs.EOF Then
                    _tempReturn = _rs.Fields(0).Value & "|" & _rs.Fields(1).Value & "|" & _rs.Fields(2).Value & "|" & _rs.Fields(3).Value & "|" & _rs.Fields(4).Value & "|" & _rs.Fields(5).Value
                Else
                    _tempReturn = "NONE"
                End If
            Catch ex As Exception
                _tempReturn = "NONE"
            End Try

        End With
        _rs.Close()
        _rs = Nothing
        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing

        Return _tempReturn
    End Function

    <WebMethod()> _
    Public Function GetClassNum(ByVal _className As String) As String
        Dim _rs As ADODB.Recordset = Nothing
        Dim _cmd As New ADODB.Command
        Dim _retVal As String = "NA"
        GetDBConnection()
        With _cmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            .CommandText = "SELECT CLASS_NUM FROM NS_CLASSES WHERE CLASS_DESCRIPTION ='" & _className & "'"
            _rs = .Execute
            If Not _rs.EOF Then
                _retVal = _rs.Fields(0).Value
            End If
        End With
        _rs.Close()
        _rs = Nothing
        _cmd = Nothing
        dbConn.Close()
        dbConn = Nothing
        Return _retVal
    End Function
    '''''''''''''''''''''''''''''''''' REPORTING FUNCTIONS '''''''''''''''''''''''''''''
    <WebMethod()> Public Function GetAllEntries() As DataSet
        '''''''''''''''''''''''''''''''' NEW FOR 2010 '''''''''''''''''''''''''''''''''
        Dim _classoa As New OleDb.OleDbDataAdapter
        Dim _classcmd As New ADODB.Command
        Dim SQL As New StringBuilder
        GetDBConnection()

        GetAllEntries = New DataSet("Entries")
        With _classcmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            SQL.Append("SELECT a.CLASS_TYPE TYPE ,")
            SQL.Append("DECODE(a.BREED_CODE,'A','Alpine','B','Oberhasli','C','Sable','D','Nigerian Dwarf','E','Recorded Grade','L','LaMancha','N','Nubian','R','Recorded Grade','S','Saanen','T','Toggenburg') BREED,")
            SQL.Append("a.CLASS_NAME NAME,COUNT(a.CLASS_NUM) COUNT FROM NS_ANIMAL_ENTRY a  ")
            SQL.Append("GROUP BY a.CLASS_TYPE,a.BREED_CODE,a.CLASS_NAME ORDER BY a.BREED_CODE,a.CLASS_NAME")
            .CommandText = SQL.ToString
            Try
                _classoa.Fill(GetAllEntries, .Execute, "Entries")
            Catch ex As Exception
                GetAllEntries = New DataSet("Entries")
            End Try

        End With
        _classoa.Dispose()
        _classoa = Nothing

        _classcmd = Nothing
        dbConn.Close()
        dbConn = Nothing
    End Function

    <WebMethod()> Public Function GetAllEquipment() As DataSet
        '''''''''''''''''''''''''''''''' NEW FOR 2010 '''''''''''''''''''''''''''''''''
        Dim _classoa As New OleDb.OleDbDataAdapter
        Dim _classcmd As New ADODB.Command
        GetDBConnection()

        GetAllEquipment = New DataSet("Equipment")
        With _classcmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText

            'Dim SQL As String = "SELECT SUM(ANIMAL_PENS_SM) ANIMAL_SMALL,'|',SUM(ANIMAL_PENS_MED) ANIMAL_MEDIUM,'|',SUM(ANIMAL_PENS_LG) ANIMAL_LARGE,'|',SUM(NS_PENS) NONSHOW,'|', SUM(WELCOME_DINNER) WELCOME,'|',SUM(BOOKS) BOOKS,'|', SUM(WINE_CHEESE) WINE,'|', SUM(VENDOR_TABLES) VENDOR FROM NS_SHOW_ENTRY"
            Dim SQL As String = "SELECT SUM(ANIMAL_PENS_SM) ANIMAL_SMALL, SUM(ANIMAL_PENS_MED) ANIMAL_MEDIUM, SUM(ANIMAL_PENS_LG) ANIMAL_LARGE ,SUM(NS_PENS) NONSHOW, SUM(WELCOME_DINNER) WELCOME, SUM(BOOKS) BOOKS,  SUM(WINE_CHEESE) WINE,  SUM(VENDOR_TABLES) VENDOR FROM NS_SHOW_ENTRY"
            .CommandText = SQL
            Try
                _classoa.Fill(GetAllEquipment, .Execute, "Equipment")
            Catch ex As Exception
                GetAllEquipment = New DataSet("Equipment")
            End Try

        End With
        _classoa.Dispose()
        _classoa = Nothing

        _classcmd = Nothing
        dbConn.Close()
        dbConn = Nothing
    End Function

    <WebMethod()> Public Function GetExhibitorTotals() As String
        '''''''''''''''''''''''''''''''' NEW FOR 2010 '''''''''''''''''''''''''''''''''
        Dim _RS As New ADODB.Recordset
        Dim _classcmd As New ADODB.Command
        Dim _totalExhibitors As String = ""
        GetDBConnection()

        With _classcmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            Dim SQL As String = "SELECT COUNT(UNIQUE MEMBERSHIP_ID) EXHIBITORS FROM NS_SHOW_ENTRY"
            .CommandText = SQL
            _RS = .Execute()

            If Not _RS.EOF Then
                _totalExhibitors = CStr(_RS.Fields(0).Value)
            End If
            _RS.Close()
            _RS = Nothing
        End With
        _classcmd = Nothing
        dbConn.Close()
        dbConn = Nothing
        Return _totalExhibitors
    End Function

    <WebMethod()> Public Function GetPenDetail() As DataSet
        '''''''''''''''''''''''''''''''' NEW FOR 2010 '''''''''''''''''''''''''''''''''
        Dim _classoa As New OleDb.OleDbDataAdapter
        Dim _classcmd As New ADODB.Command
        GetDBConnection()

        GetPenDetail = New DataSet("Pens")
        With _classcmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText

            Dim SQL As String = "SELECT MEMBERSHIP_ID  || '|' ||  DISPLAY_AS || '|' || SHOW_AS || '|' || ANIMAL_PENS  || '|' || TACK_PENS  ||'|' || NS_PENS  || '|' || EXHIBIT_PENS  || '|' || NOTES || ';' REC FROM NS_SHOW_ENTRY"
            .CommandText = SQL
            Try
                _classoa.Fill(GetPenDetail, .Execute, "Pens")
            Catch ex As Exception
                GetPenDetail = New DataSet("Pens")
            End Try

        End With
        _classoa.Dispose()
        _classoa = Nothing

        _classcmd = Nothing
        dbConn.Close()
        dbConn = Nothing
    End Function

    <WebMethod()> Public Function excelPenDetail() As DataSet
        '''''''''''''''''''''''''''''''' NEW FOR 2010 '''''''''''''''''''''''''''''''''
        Dim _classoa As New OleDb.OleDbDataAdapter
        Dim _classcmd As New ADODB.Command
        GetDBConnection()

        excelPenDetail = New DataSet("Pens")
        With _classcmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText

            Dim SQL As String = "SELECT n.ENTRY_DATE, n.MEMBERSHIP_ID,n.DISPLAY_AS, STATE, SHOW_AS ,COUNT(CLASS_NUM) INDIVIDUAL_ENTRIES,ANIMAL_PENS_SM,ANIMAL_PENS_MED,ANIMAL_PENS_LG,NS_PENS, NOTES FROM NS_SHOW_ENTRY n, MEMBERSHIP_VIEW m, NS_ANIMAL_ENTRY a WHERE n.MEMBERSHIP_ID=m.MEMBERSHIP_ID and a.MEMBERSHIP_ID=n.MEMBERSHIP_ID AND a.CLASS_TYPE <> 'G' GROUP BY  n.ENTRY_DATE, n.MEMBERSHIP_ID,n.DISPLAY_AS, STATE, SHOW_AS ,ANIMAL_PENS_SM,ANIMAL_PENS_MED,ANIMAL_PENS_LG,NS_PENS, NOTES ORDER BY  n.ENTRY_DATE DESC"
            .CommandText = SQL
            Try
                _classoa.Fill(excelPenDetail, .Execute, "Pens")
            Catch ex As Exception
                excelPenDetail = New DataSet("Pens")
            End Try

        End With
        _classoa.Dispose()
        _classoa = Nothing

        _classcmd = Nothing
        dbConn.Close()
        dbConn = Nothing
    End Function

    <WebMethod()> Public Function ExtrasDetails() As DataSet
        '''''''''''''''''''''''''''''''' NEW FOR 2010 '''''''''''''''''''''''''''''''''
        Dim _classoa As New OleDb.OleDbDataAdapter
        Dim _classcmd As New ADODB.Command
        GetDBConnection()

        ExtrasDetails = New DataSet("Extras")
        With _classcmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText

            Dim SQL As String = "SELECT n.ENTRY_DATE, n.MEMBERSHIP_ID,n.DISPLAY_AS, SHOW_AS , WELCOME_DINNER,WINE_CHEESE,BOOKS SHOWBOOK_ADVERTS  FROM NS_SHOW_ENTRY n ORDER BY  n.ENTRY_DATE DESC"
            .CommandText = SQL
            Try
                _classoa.Fill(ExtrasDetails, .Execute, "Extras")
            Catch ex As Exception
                ExtrasDetails = New DataSet("Extras")
            End Try

        End With
        _classoa.Dispose()
        _classoa = Nothing

        _classcmd = Nothing
        dbConn.Close()
        dbConn = Nothing
    End Function

    <WebMethod()> Public Function ExhibitorTotals() As String
        '''''''''''''''''''''''''''''''' NEW FOR 2010 '''''''''''''''''''''''''''''''''
        Dim _RS As New ADODB.Recordset
        Dim _classcmd As New ADODB.Command
        Dim _retStr As String = ""
        GetDBConnection()

        With _classcmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText

            .CommandText = "SELECT 'WEX',NVL(COUNT(*),0) FROM NS_SHOW_ENTRY WHERE ENTRY='WEB' UNION SELECT 'WAN', NVL(COUNT(*),0) FROM NS_ANIMAL_ENTRY WHERE ENTRY='WEB' AND CLASS_TYPE <> 'G' UNION SELECT 'OEX', NVL(COUNT(*),0) FROM NS_SHOW_ENTRY WHERE ENTRY='OFFICE' UNION SELECT  'OAN', NVL(COUNT(*),0) FROM NS_ANIMAL_ENTRY WHERE ENTRY='OFFICE' AND CLASS_TYPE <> 'G'"
            _RS = .Execute()
            If Not _RS.EOF Then
                _retStr = CStr(_RS.Fields(0).Value) & "," & CStr(_RS.Fields(1).Value)
                _RS.MoveNext()
                _retStr = _retStr & "|" & CStr(_RS.Fields(0).Value) & "," & CStr(_RS.Fields(1).Value)
                _RS.MoveNext()
                _retStr = _retStr & "|" & CStr(_RS.Fields(0).Value) & "," & CStr(_RS.Fields(1).Value)
                _RS.MoveNext()
                _retStr = _retStr & "|" & CStr(_RS.Fields(0).Value) & "," & CStr(_RS.Fields(1).Value)
            End If
            _RS.Close()
        End With
        _RS = Nothing
        _classcmd = Nothing
        dbConn.Close()
        dbConn = Nothing

        Return _retStr
    End Function
    <WebMethod()> Public Function NewExhibitorTotals() As DataSet
        '''''''''''''''''''''''''''''''' NEW FOR 2012 '''''''''''''''''''''''''''''''''
        Dim _classcmd As New ADODB.Command
        Dim _str As New StringBuilder
        Dim _classoa As New OleDb.OleDbDataAdapter
        Dim _ds As DataSet = New DataSet("Totals")
        GetDBConnection()
        With _classcmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            _str.Append("SELECT 1,'WEB', (SELECT NVL(COUNT(*),0) FROM NS_SHOW_ENTRY WHERE ENTRY='WEB'),")
            _str.Append("to_char((SELECT NVL(COUNT(*),0) FROM NS_SHOW_ENTRY WHERE ENTRY='WEB')/(SELECT NVL(COUNT(*),0) FROM NS_SHOW_ENTRY), '9999.99')*100,")
            _str.Append("(SELECT NVL(COUNT(*),0) FROM NS_ANIMAL_ENTRY WHERE ENTRY='WEB' AND CLASS_TYPE<>'G'),")
            _str.Append("to_char((SELECT NVL(COUNT(*),0) FROM NS_ANIMAL_ENTRY WHERE ENTRY='WEB' AND CLASS_TYPE<>'G')/(SELECT NVL(COUNT(*),0) FROM NS_ANIMAL_ENTRY WHERE CLASS_TYPE <> 'G'), '9999.99')*100")
            _str.Append(" FROM DUAL UNION ")
            _str.Append("SELECT 2,'OFFICE',(SELECT NVL(COUNT(*),0) FROM NS_SHOW_ENTRY WHERE ENTRY='OFFICE'),")
            _str.Append("to_char((SELECT NVL(COUNT(*),0) FROM NS_SHOW_ENTRY WHERE ENTRY='OFFICE')/(SELECT NVL(COUNT(*),0) FROM NS_SHOW_ENTRY), '9999.99')*100,")
            _str.Append("(SELECT NVL(COUNT(*),0) FROM NS_ANIMAL_ENTRY WHERE ENTRY='OFFICE' AND CLASS_TYPE<>'G'),")
            _str.Append("to_char((SELECT NVL(COUNT(*),0) FROM NS_ANIMAL_ENTRY WHERE ENTRY='OFFICE' AND CLASS_TYPE<>'G')/(SELECT NVL(COUNT(*),0) FROM NS_ANIMAL_ENTRY WHERE CLASS_TYPE <> 'G'), '9999.99')*100")
            _str.Append(" FROM DUAL UNION ")
            _str.Append("SELECT 3,'TOTALS',(SELECT NVL(COUNT(*),0) FROM NS_SHOW_ENTRY),")
            _str.Append("to_char(1, '9999.99')*100,(SELECT NVL(COUNT(*),0) FROM NS_ANIMAL_ENTRY WHERE CLASS_TYPE<>'G'),")
            _str.Append("to_char(1, '9999.99')*100 FROM DUAL")
            .CommandText = _str.ToString
            Try
                _classoa.Fill(_ds, .Execute, "Totals")
            Catch ex As Exception
                _ds = New DataSet("Totals")
            End Try
        End With
        _classoa.Dispose()
        _classoa = Nothing
        _classcmd = Nothing
        dbConn.Close()
        dbConn = Nothing
        _ds.Tables(0).Columns(0).ColumnName = "ORDER"
        _ds.Tables(0).Columns(1).ColumnName = "TYPE"
        _ds.Tables(0).Columns(2).ColumnName = "EXHIBITORS"
        _ds.Tables(0).Columns(3).ColumnName = "PERCENT_EXHIBITORS"
        _ds.Tables(0).Columns(4).ColumnName = "ENTRIES"
        _ds.Tables(0).Columns(5).ColumnName = "PERCENT_ENTRIES"
        Return _ds
    End Function



    <WebMethod()> Public Function GetAllMemberEntries() As DataSet
        '''''''''''''''''''''''''''''''' NEW FOR 2010 '''''''''''''''''''''''''''''''''
        Dim _classoa As New OleDb.OleDbDataAdapter
        Dim _classcmd As New ADODB.Command
        Dim SQL As New StringBuilder
        GetDBConnection()

        GetAllMemberEntries = New DataSet("Entries")
        With _classcmd
            .ActiveConnection = dbConn
            .CommandType = ADODB.CommandTypeEnum.adCmdText
            SQL.Append("SELECT a.MEMBERSHIP_ID ID, NVL(a.SHOWING_AS,DISPLAY_AS) Name, a.CLASS_TYPE TYPE ,")
            SQL.Append("DECODE(a.BREED_CODE,'A','Alpine','B','Oberhasli','C','Sable','D','Nigerian Dwarf','E','Recorded Grade','L','LaMancha','N','Nubian','R','Recorded Grade','S','Saanen','T','Toggenburg') BREED,")
            SQL.Append("a.CLASS_NAME NAME,COUNT(a.CLASS_NUM) COUNT FROM NS_ANIMAL_ENTRY a   ")
            SQL.Append("GROUP BY a.MEMBERSHIP_ID, NVL(a.SHOWING_AS,DISPLAY_AS), a.CLASS_TYPE,a.BREED_CODE,a.CLASS_NAME ORDER BY a.MEMBERSHIP_ID, a.BREED_CODE,a.CLASS_NAME")
            .CommandText = SQL.ToString
            Try
                _classoa.Fill(GetAllMemberEntries, .Execute, "Entries")
            Catch ex As Exception
                GetAllMemberEntries = New DataSet("Entries")
            End Try

        End With
        _classoa.Dispose()
        _classoa = Nothing

        _classcmd = Nothing
        dbConn.Close()
        dbConn = Nothing
    End Function
    '''''''''''''''''''''''''''''''''' END REPORTING FUNCTIONS '''''''''''''''''''''''''''''

End Class