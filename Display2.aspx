﻿<%@ Page Language="C#" %>
<%@ Import Namespace="CrystalDecisions.Shared" %>
<%@ Import Namespace="CrystalDecisions.CrystalReports.Engine" %>
<%@ Import Namespace="CrystalDecisions.Web" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Net.Mail" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta content="IE=EmulateIE7" http-equiv="X-UA-Compatible" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
    <title>Report</title>
<script language="javascript" type="text/javascript">
function window_onload() {

}
</script>
</head>
<body onload="return window_onload()">
    <form id="form1" runat="server">
    <div>
        <h5 id="hdr" runat="server">Generating Report</h5>
    </div>
    </form>
</body>
<script runat="server" language="c#" type="text/C#">

    TableLogOnInfo crLogonInfo = new CrystalDecisions.Shared.TableLogOnInfo();
    
    protected void  Page_Load(object sender, EventArgs e)
    {
        this.Focus();
         string reportNum = Request.QueryString["r"];
        int _reportNum = Convert.ToInt16(reportNum);
        switch (_reportNum)
        {
            case 1: // 
                string _division = Request.QueryString["d"];
                if (_division != "")
                {
                    BuildExhibitorReport(_division);
                }
                break;
            case 2: // 
                BuildCheckinReport();
                break;
            case 3: // 
                BuildRingReport();
                break;
            case 4: //
                break;
            case 5: // 
                string _memberId = Request.QueryString["m"];
                if (_memberId != "")
                {
                    BuildIndividualRingReport(_memberId);
                }
                break;
            case 6: // 
                string _division0 = Request.QueryString["d"];
                if (_division0 != "")
                {
                    BuildSecretaryReport(_division0);
                }
                break;
            case 7: // 
                BuildExhibitorBarcodeReport();
                break;
            case 8: // 
                string _division2 = Request.QueryString["d"];
                if (_division2 != "")
                {
                    BuildMilkoutReport(_division2);
                }
                break;
            case 9: // 
                string _division3 = Request.QueryString["d"];
                if (_division3 != "")
                {
                    BuildShowbookReport(_division3);
                }
                break;
            case 12: // 
                string _division4 = Request.QueryString["d"];
                if (_division4 != "")
                {
                    BuildResultsReport(_division4);
                }
                break;
            case 13: // 
                string _memberId2 = Request.QueryString["m"];
                if (_memberId2 != "")
                {
                    BuildSingleCheckinReport(_memberId2);
                }
                break;
        }
    }

    public void BuildColReport(string _customerId, string _fName)
    {
        ReportDocument oRpt = new ReportDocument();
        string str = Server.MapPath("Reports\\CURRENTLY_OWNED.rpt");
        string str1 = str.Replace("\\", "\\\\");
        oRpt.Load(str1);
        oRpt = LoadParams(oRpt, "CUSTOMERID", _customerId);
        SetLogin(oRpt);
        oRpt.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
        oRpt.PrintOptions.PaperSize = PaperSize.PaperLetter;
        oRpt.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
        ToPdf(oRpt, _fName);
    }
    public void BuildExhibitorReport(string _division)
    {
         ReportDocument oRpt=null ;
        try
        {
           oRpt = new ReportDocument();
        }
        catch (Exception e)
        {
            Console.WriteLine("{0} Exception caught.", e);
        }
        string str = Server.MapPath("Reports\\Exhibitor.rpt");
        string str1 = str.Replace("\\", "\\\\");
        oRpt.Load(str1);
        oRpt = LoadParams(oRpt, "division", _division);
        SetLogin(oRpt);
        oRpt.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
        oRpt.PrintOptions.PaperSize = PaperSize.PaperLetter;
        oRpt.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
        ToPdf(oRpt, "Exhibitors");
    }
    public void BuildCheckinReport()
    {
        ReportDocument oRpt = new ReportDocument();
        string str = Server.MapPath("Reports\\CheckIn.rpt");
        string str1 = str.Replace("\\", "\\\\");
        oRpt.Load(str1);
        //oRpt = LoadParams(oRpt, "CUSTOMERID", _customerId);
        SetLogin(oRpt);
        oRpt.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
        oRpt.PrintOptions.PaperSize = PaperSize.PaperLetter;
        oRpt.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
        ToPdf(oRpt, "CheckIn");
    }
    public void BuildRingReport()
    {
        ReportDocument oRpt = new ReportDocument();
        string str = Server.MapPath("Reports\\RingLabels.rpt");
        string str1 = str.Replace("\\", "\\\\");
        oRpt.Load(str1);
        //oRpt = LoadParams(oRpt, "CUSTOMERID", _customerId);
        SetLogin(oRpt);
        oRpt.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
        oRpt.PrintOptions.PaperSize = PaperSize.PaperLetter;
        oRpt.PrintOptions.PaperOrientation = PaperOrientation.Portrait;
        ToPdf(oRpt, "RingLabels");
    }
    public void BuildIndividualRingReport(string _memberId)
    {
        ReportDocument oRpt = new ReportDocument();
        string str = Server.MapPath("Reports\\RingLabelsIndividual.rpt");
        string str1 = str.Replace("\\", "\\\\");
        oRpt.Load(str1);
        oRpt = LoadParams(oRpt, "memberid", _memberId);
        SetLogin(oRpt);
        oRpt.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
        oRpt.PrintOptions.PaperSize = PaperSize.PaperLetter;
        oRpt.PrintOptions.PaperOrientation = PaperOrientation.Portrait;
        ToPdf(oRpt, "RingLabels");
    }
    public void BuildSecretaryReport(string _division)
    {
        ReportDocument oRpt = new ReportDocument();
        string str = Server.MapPath("Reports\\Secretary.rpt");
        string str1 = str.Replace("\\", "\\\\");
        oRpt.Load(str1);
        oRpt = LoadParams(oRpt, "division", _division);
        SetLogin(oRpt);
        oRpt.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
        oRpt.PrintOptions.PaperSize = PaperSize.PaperLetter;
        oRpt.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
        ToPdf(oRpt, "Secretary");
    }
    public void BuildMilkoutReport(string _division)
    {
        ReportDocument oRpt = new ReportDocument();
        string str = Server.MapPath("Reports\\Milkout.rpt");
        string str1 = str.Replace("\\", "\\\\");
        oRpt.Load(str1);
        oRpt = LoadParams(oRpt, "division", _division);
        SetLogin(oRpt);
        oRpt.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
        oRpt.PrintOptions.PaperSize = PaperSize.PaperLetter;
        oRpt.PrintOptions.PaperOrientation = PaperOrientation.Portrait;
        ToPdf(oRpt, "Milkout");
    }
    public void BuildShowbookReport(string _division)
    {
        ReportDocument oRpt = new ReportDocument();
        string str = Server.MapPath("Reports\\ShowBook.rpt");
        string str1 = str.Replace("\\", "\\\\");
        oRpt.Load(str1);
        oRpt = LoadParams(oRpt, "division", _division);
        SetLogin(oRpt);
        oRpt.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
        oRpt.PrintOptions.PaperSize = PaperSize.PaperLetter;
        oRpt.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
        ToPdf(oRpt, "Showbook");
    }
    public void BuildResultsReport(string _division)
    {
        ReportDocument oRpt = new ReportDocument();
        string str = Server.MapPath("Reports\\Results.rpt");
        string str1 = str.Replace("\\", "\\\\");
        oRpt.Load(str1);
        oRpt = LoadParams(oRpt, "division", _division);
        SetLogin(oRpt);
        oRpt.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
        oRpt.PrintOptions.PaperSize = PaperSize.PaperLetter;
        oRpt.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
        ToPdf(oRpt, "Results");
    }
    public void BuildExhibitorBarcodeReport()
    {
        ReportDocument oRpt = new ReportDocument();
        string str = Server.MapPath("Reports\\ExhibitorBarcodes.rpt");
        string str1 = str.Replace("\\", "\\\\");
        oRpt.Load(str1);
        //oRpt = LoadParams(oRpt, "CUSTOMERID", _customerId);
        SetLogin(oRpt);
        oRpt.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
        oRpt.PrintOptions.PaperSize = PaperSize.PaperLetter;
        oRpt.PrintOptions.PaperOrientation = PaperOrientation.Portrait;
        ToPdf(oRpt, "Exhibitors");
    }
    public void BuildSingleCheckinReport(string memberId)
    {
        ReportDocument oRpt = new ReportDocument();
        string str = Server.MapPath("Reports\\Single_CheckIn.rpt");
        string str1 = str.Replace("\\", "\\\\");
        oRpt.Load(str1);
        oRpt = LoadParams(oRpt, "memberId", memberId);
        SetLogin(oRpt);
        oRpt.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
        oRpt.PrintOptions.PaperSize = PaperSize.PaperLetter;
        oRpt.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
        ToPdf(oRpt, "CheckIn");
    }
    public void SetLogin(ReportDocument tbl)
    {
        crLogonInfo = tbl.Database.Tables[0].LogOnInfo;
        crLogonInfo.ConnectionInfo.UserID = System.Configuration.ConfigurationManager.AppSettings["user"];
        crLogonInfo.ConnectionInfo.Password = System.Configuration.ConfigurationManager.AppSettings["pass"];
        crLogonInfo.ConnectionInfo.DatabaseName = System.Configuration.ConfigurationManager.AppSettings["db"];
    }
    public ReportDocument LoadParams(ReportDocument _oRpt, String _pName, String _pVal)
    {
        // Declare the parameter related objects.
        ParameterDiscreteValue crParameterDiscreteValue;
        ParameterFieldDefinitions crParameterFieldDefinitions;
        ParameterFieldDefinition crParameterFieldLocation;
        ParameterValues crParameterValues;

        //Get the report's parameters collection.
        crParameterFieldDefinitions = _oRpt.DataDefinition.ParameterFields;

        // Set the first parameter
        // - Get the parameter, tell it to use the current values vs default value.
        // - Tell it the parameter contains 1 discrete value vs multiple values.
        // - Set the parameter's value.
        // - Add it and apply it.
        // - Repeat these statements for each parameter.

        crParameterFieldLocation = crParameterFieldDefinitions[_pName];
        crParameterValues = crParameterFieldLocation.CurrentValues;
        crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
        crParameterDiscreteValue.Value = _pVal;
        crParameterValues.Add(crParameterDiscreteValue);
        crParameterFieldLocation.ApplyCurrentValues(crParameterValues);

        return _oRpt;
    }
    public void ToPdf(ReportDocument oRpt, string _fName)
    {
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Open_Window", "ReturnDone();", true);

        if (oRpt.IsLoaded)
        {            
            try
            {
                Title = _fName;
                MemoryStream oStream = (MemoryStream)oRpt.ExportToStream(ExportFormatType.PortableDocFormat);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "inline; filename=" + _fName);
                Response.BinaryWrite(oStream.ToArray());
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                oRpt.Close();
                oRpt.Dispose();
            }
            catch (Exception e)
            {
                oRpt.Close();
                oRpt.Dispose();
                Response.Redirect("DisplayError.html", false);
            }
        } else {
            Response.Redirect("DisplayError.html", false);
        }
    }
</script>

</html>
