﻿Imports System
Imports System.Data
Imports System.Data.DataSet
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Script.Services
Imports System.Web.Services
Imports wsProxy


Partial Class NS_Entry
    Inherits System.Web.UI.Page
    Dim CAlpine() As Integer = {89, 90, 91, 92, 93, 96, 97, 98, 99, 100, 101, 94, 95, 102, 103, 104, 105, 106}
    Dim CLaMancha() As Integer = {1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 13, 6, 7, 14, 15, 16, 17, 18}
    Dim CToggenburg() As Integer = {19, 20, 21, 22, 23, 26, 27, 28, 29, 30, 31, 24, 25, 32, 33, 34, 35, 36}
    Dim CRecorded_Grade() As Integer = {37, 38, 39, 40, 41, 43, 44, 45, 46, 47, 48, 42, 0, 49, 50, 0, 51, 52}
    Dim CSaanen() As Integer = {53, 54, 55, 56, 57, 60, 61, 62, 63, 64, 65, 58, 59, 66, 67, 68, 69, 70}
    Dim COberhasli() As Integer = {71, 72, 73, 74, 75, 78, 79, 80, 81, 82, 83, 76, 77, 84, 85, 86, 87, 88}
    Dim CNigerian_Dwarf() As Integer = {107, 108, 109, 110, 111, 114, 115, 116, 117, 118, 119, 112, 113, 120, 121, 122, 123, 124}
    Dim CNubian() As Integer = {125, 126, 127, 128, 129, 132, 133, 134, 135, 136, 137, 130, 131, 138, 139, 140, 141, 142}
    Dim CSable() As Integer = {143, 144, 145, 146, 147, 150, 151, 152, 153, 154, 155, 148, 149, 156, 157, 158, 159, 160}
    Dim CName() As String = {"Junior Kid", "Intermediate Kid", "Senior Kid", "Junior Yearling", "Senior Yearling", "Yearling Milker", "2 Year Old Milker", "3 Year Old Milker", "4 Year Old Milker", "5 And 6 Year Old Milker", "7 Years And Older Milker", "Best 3 Junior Does", "Junior Get Of Sire", "Dairy Herd", "Best 3 Senior Does", "Senior Get Of Sire", "Produce Of Dam", "Dam And Daughter"}
    Dim BreedNames() As String = {"A", "Alpine", "L", "LaMancha", "D", "Nigerian Dwarf", "N", "Nubian", "B", "Oberhasli", "G", "Recorded Grade", "E", "Recorded Grade", "S", "Saanen", "T", "Toggenburg", "C", "Sable"}
    'Dim _classArr() As Array = {CAlpine, CLaMancha, CNigerian_Dwarf, CNubian, COberhasli, CRecorded_Grade, CRecorded_Grade, CSaanen, CToggenburg, CSable, COberhasli}
    Dim _classArr() As Array = {CAlpine, CLaMancha, CNigerian_Dwarf, CNubian, COberhasli, CRecorded_Grade, CSaanen, CToggenburg, CSable, COberhasli}
    'Dim _breeds() As String = {"Alpine", "LaMancha", "Nigerian Dwarf", "Nubian", "Oberhasli", "Recorded Grade", "Recorded Grade", "Saanen", "Toggenburg", "Sable"}
    Dim _breeds() As String = {"Alpine", "LaMancha", "Nigerian Dwarf", "Nubian", "Oberhasli", "Recorded Grade", "Saanen", "Toggenburg", "Sable"}
    'Dim _breedvals() As String = {"A", "L", "D", "N", "B", "E", "G", "S", "T", "C"}
    'Dim _classArrName() As String = {"A", "L", "D", "N", "B", "G", "E", "S", "T", "C", "O"}
    Dim _breedvals() As String = {"A", "L", "D", "N", "B", "G", "S", "T", "C"}
    Dim _classArrName() As String = {"A", "L", "D", "N", "B", "G", "S", "T", "C", "O"}

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        divSubMsg.InnerHtml = ""
        'pnlMessage.Style.Add("display", "none")
        If Not Me.IsPostBack Then
            Dim i As Integer = 0
            Dim j As Integer = 0
            Me.ddbreed.Items.Add("")
            Me.ddaddbreed.Items.Add("")
            For i = 0 To _breeds.GetUpperBound(0)
                Dim itm As New ListItem
                itm.Text = _breeds(i)
                itm.Value = _breedvals(i)
                Me.ddbreed.Items.Add(itm)
                Me.ddaddbreed.Items.Add(itm)

                'Me.ddbreed.Items.Add(_breeds(i))
                'Me.ddaddbreed.Items.Add(_breeds(i))
            Next
            Me.ddclass.Items.Add("")
            Me.ddaddclass.Items.Add("")
            For i = 0 To CName.GetUpperBound(0)
                Me.ddclass.Items.Add(CName(i))
                Me.ddaddclass.Items.Add(CName(i))
            Next
            Me.ddbreed.SelectedIndex = 0
            Me.ddclass.SelectedIndex = 0
            Me.ddaddbreed.SelectedIndex = 0
            Me.ddaddclass.SelectedIndex = 0
            Dim _boxBreeds() As String = {"Alpine", "LaMancha", "Nigerian Dwarf", "Nubian", "Oberhasli", "Recorded Grade", "Saanen", "Sable", "Togeenburg"}
            Dim _boxGroups() As String = {"Best 3 Junior Does", "Junior Get of Sire", "Dairy Herd", "Best 3 Senior Does", "Senior Get of Sire", "Produce of Dam", "Dam and Daughter"}
            For i = 0 To _boxBreeds.GetUpperBound(0)
                For j = 0 To _boxGroups.GetUpperBound(0)
                    Me.ddGroupAll.Items.Add(_boxBreeds(i) & " " & _boxGroups(j))
                Next
            Next
            Me.txtSMemId.Focus()
        Else
            Select Case Me.mviewMain.ActiveViewIndex
                Case 0
                    Me.txtSMemId.Focus()
                Case 1
                    Me.txtMemId.Focus()
                Case 2
                    Me.ddbreed.Focus()
            End Select
        End If
    End Sub
    Public Sub ShowAlert(ByVal message As String)
        divSubMsg.InnerHtml = message
    End Sub
    Protected Sub btnSubmit_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.ServerClick, btnSubmit2.ServerClick
        Dim _msg As String = ""
        divSubMsg.InnerHtml = ""
        Dim _ws As New wsAdmin()
        Dim _ds As DataSet = RecordPlacings()
        Dim _rowsToWrite As Integer = _ds.Tables("Placing").Rows.Count + _ds.Tables("Udder").Rows.Count
        ' Send the dataset to the web service - get a write status back
        Dim _retVal As String = ""
        _retVal = _ws.WriteNSPlacing(_ds)
        If _retVal.StartsWith("SUCCESS") Then
            Dim _split() As String = _retVal.Split(":")
            If CInt(_split(1)) = _rowsToWrite Then ' everything is ok
                _msg = CInt(_split(1)) & " out of " & _rowsToWrite & " entries successfully updated."
                ShowAlert(_msg)
            Else ' not all rows got updated
                _msg = "NOT ALL ENTRIES UPDATED: " & CInt(_split(1)) & " out of " & _rowsToWrite & " updated."
                ShowAlert(_msg)
            End If
        Else ' there was an error in our update
            _msg = "ERROR: " & _retVal
            ShowAlert(_msg)
        End If
        CleanPlacings()
    End Sub
    Protected Sub btnCSubmit_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCSubmit.ServerClick
        Dim _msg As String = ""
        divSubMsg.InnerHtml = ""
        Dim _ws As New wsAdmin()
        Dim _ds As DataSet = RecordCheckin()
        Dim _rowsToWrite As Integer = _ds.Tables("Checkin").Rows.Count
        ' Send the dataset to the web service - get a write status back
        Dim _retVal As String = ""
        _retVal = _ws.WriteNSCheckin(_ds)
        If _retVal.StartsWith("SUCCESS") Then
            Dim _split() As String = _retVal.Split(":")
            If CInt(_split(1)) = _rowsToWrite Then ' everything is ok
                _msg = CInt(_split(1)) & " out of " & _rowsToWrite & " entries successfully updated."
                ShowAlert(_msg)
            Else ' not all rows got updated
                _msg = "NOT ALL ENTRIES UPDATED: " & CInt(_split(1)) & " out of " & _rowsToWrite & " updated."
                ShowAlert(_msg)
            End If
        Else ' there was an error in our update
            _msg = "ERROR: " & _retVal
            ShowAlert(_msg)
        End If
        CleanCheckin()
        Me.txtMemId.Focus()
    End Sub
    Public Function RecordCheckin() As DataSet
        Dim _entryNum As String = ""
        Dim _ds As New DataSet
        Dim _dtc As New DataTable
        Dim _col As DataColumn
        Dim row As DataRow
        Dim _colArr() As String = {"ClassNum", "EntryNum"}
        Dim i As Integer = 0
        Dim _classNum As String = ""
        For i = 0 To _colArr.GetUpperBound(0) ' Checkin data table
            _col = New DataColumn
            _col.DataType = Type.GetType("System.String")
            _col.ColumnName = _colArr(i)
            _dtc.Columns.Add(_col)
        Next

        ' Get the entry number
        For Each myControl As Control In Me.vwCheckIn.Controls
            If TypeOf myControl Is TextBox Then
                If myControl.ID.Length > 9 Then
                    If myControl.ID.Substring(0, 10) = "textCEntry" Then
                        _entryNum = CType(myControl, TextBox).Text
                        _classNum = CType(myControl, TextBox).ToolTip

                        If _entryNum <> "" Then ' there's something in the box
                            row = _dtc.NewRow()
                            row("ClassNum") = _classNum
                            row("EntryNum") = _entryNum
                            _dtc.Rows.Add(row)
                        End If
                    End If
                End If
            End If
        Next myControl

        _ds.Tables.Add(_dtc)
        _ds.Tables(0).TableName = "Checkin"
        Return _ds
    End Function

    Public Function RecordPlacings() As DataSet
        Dim _entryNum As String = ""
        Dim _placing As String = ""
        Dim _ds As New DataSet
        Dim _dtp As New DataTable
        Dim _dtu As New DataTable

        Dim _col As DataColumn
        Dim row As DataRow
        Dim _colArr() As String = {"ClassNum", "EntryNum", "Placing"}
        Dim _ucolArr() As String = {"UClassNum", "UEntryNum", "UPlacing"}

        Dim i As Integer = 0
        For i = 0 To _colArr.GetUpperBound(0) ' Placing data table
            _col = New DataColumn
            _col.DataType = Type.GetType("System.String")
            _col.ColumnName = _colArr(i)
            _dtp.Columns.Add(_col)
        Next
        For i = 0 To _ucolArr.GetUpperBound(0) ' Udder data table
            _col = New DataColumn
            _col.DataType = Type.GetType("System.String")
            _col.ColumnName = _ucolArr(i)
            _dtu.Columns.Add(_col)
        Next

        Dim _classNum As String = GetClassNum()

        ' Get the entry number and it's placing
        For Each myControl As Control In Me.vwResults.Controls
            If TypeOf myControl Is TextBox Then
                If myControl.ID.Length > 9 Then
                    If myControl.ID.Substring(0, 9) = "textEntry" Then
                        _entryNum = CType(myControl, TextBox).Text
                        If _entryNum <> "" Then ' there's something in the box
                            _placing = myControl.ID.Replace("textEntry", "")
                            If (_placing = "40" Or _placing = "41" Or _placing = "42") Then ' it's an udder award
                                row = _dtu.NewRow()
                                row("UClassNum") = _classNum
                                row("UEntryNum") = _entryNum
                                Select Case _placing
                                    Case "40"
                                        row("UPlacing") = "1"
                                    Case "41"
                                        row("UPlacing") = "2"
                                    Case "42"
                                        row("UPlacing") = "3"
                                End Select
                                _dtu.Rows.Add(row)

                            Else ' it's a place award
                                row = _dtp.NewRow()
                                row("ClassNum") = _classNum
                                row("EntryNum") = _entryNum
                                row("Placing") = _placing
                                _dtp.Rows.Add(row)
                            End If
                        End If
                    End If
                End If
            End If
        Next myControl

        _ds.Tables.Add(_dtp)
        _ds.Tables.Add(_dtu)

        _ds.Tables(0).TableName = "Placing"
        _ds.Tables(1).TableName = "Udder"

        Return _ds
    End Function
    Public Function GetClassNum() As String
        Dim _breed As String = Me.ddbreed.Value
        _breed = ConvertBreed(_breed)
        Dim _classname As String = Me.ddclass.Value
        Dim i As Integer = 0
        Dim _cIndex As Integer = 0
        Dim _classarray As Integer = 0
        For i = 0 To _breeds.GetUpperBound(0)
            If _breeds(i) = _breed Then
                _classarray = i
                Exit For
            End If
        Next
        Dim _cArray As Array = _classArr(i)
        For i = 0 To CName.GetUpperBound(0)
            If CName(i) = _classname Then
                _cIndex = i
                Exit For
            End If
        Next
        Dim _classNum As Integer = _cArray(_cIndex)
        Return _classNum
    End Function
    Public Function ConvertBreed(ByVal _breedLetter As String) As String
        Dim _breed As String
        _breed = ""
        Select Case _breedLetter
            Case "A"
                _breed = "Alpine"
            Case "L"
                _breed = "LaMancha"
            Case "D"
                _breed = "Nigerian Dwarf"
            Case "N"
                _breed = "Nubian"
            Case "B"
                _breed = "Oberhasli"
            Case "G"
                _breed = "Recorded Grade"
            Case "E"
                _breed = "Recorded Grade"
            Case "S"
                _breed = "Saanen"
            Case "T"
                _breed = "Toggenburg"
            Case "C"
                _breed = "Sable"
        End Select

        Return _breed
    End Function
    Public Sub CleanCheckin()
        For Each myControl As Control In Me.vwCheckIn.Controls
            If TypeOf myControl Is TextBox Then
                If myControl.ID.Length > 9 Then
                    If myControl.ID.Substring(0, 10) = "textCEntry" Then
                        CType(myControl, TextBox).Text = String.Empty '(or "")
                    End If
                End If
            End If
        Next myControl

        Me.txtMemId.Text = ""
        Me.txtShowAs.Text = ""
    End Sub
    Public Sub CleanPlacings()
        For Each myControl As Control In Me.vwResults.Controls
            If TypeOf myControl Is TextBox Then
                If myControl.ID.Length > 9 Then
                    If myControl.ID.Substring(0, 9) = "textEntry" Then
                        CType(myControl, TextBox).Text = String.Empty '(or "")
                    End If
                End If
            End If
        Next myControl

        Me.txtClassNum.Text = ""
        Me.ddbreed.SelectedIndex = 0
        Me.ddclass.SelectedIndex = 0
    End Sub
    Protected Sub mnuMain_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles mnuMain.MenuItemClick
        Select Case e.Item.Value
            Case "Substitute"
                Me.mviewMain.ActiveViewIndex = 0
                Me.txtSMemId.Focus()
            Case "Check In"
                Me.mviewMain.ActiveViewIndex = 1
                Me.txtMemId.Focus()
            Case "Placings"
                Me.mviewMain.ActiveViewIndex = 2
                Me.ddbreed.Focus()
            Case "Reports"
                Me.mviewMain.ActiveViewIndex = 3
            Case "Add"
                FillExhibitors()
                Me.mviewMain.ActiveViewIndex = 4
        End Select
    End Sub
    Private Sub FillExhibitors()
        If ddExhibitors.Items.Count = 0 Then
            divSubMsg.InnerHtml = ""
            Dim _ws As New wsAdmin()
            Dim _ds As New DataSet
            Try
                _ds = _ws.GetExhibitors()
            Catch ex As Exception

            End Try
            If _ds.Tables.Count > 0 Then
                ddExhibitors.DataSource = _ds.Tables(0)
                ddExhibitors.DataTextField = "SHOW_AS"
                ddExhibitors.DataValueField = "MEMBERSHIP_ID"
                ddExhibitors.DataBind()
                Me.ddaddbreed.SelectedIndex = 0
                Me.ddaddclass.SelectedIndex = 0
            End If
        End If
    End Sub

    Public Sub ShowGroupBox()
        Dim e As New System.EventArgs
        Dim sender As New Object
        btnShowGroupPopup_Click(sender, e)
    End Sub

    Public Sub btnShowGroupPopup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowGroupPopup.Click
        Me.updGroup.Update()
        Me.mdlGroup.Show()
    End Sub
    Protected Sub txtSMemId_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSMemId.TextChanged
        FillGoatDataset(Me.txtSMemId.Text)
    End Sub
    Protected Sub FillGoatDataset(ByVal _memberId As String)
        divSubMsg.InnerHtml = ""
        Dim _ws As New wsAdmin()
        Dim _ds As New DataSet
        Dim _dt As New DataTable
        Me.gvSub.DataSource = Nothing
        Me.gvSub.DataBind()
        Me.ddGroup.Items.Clear()
        _ds = _ws.HasEntry(_memberId)
        If _ds.Tables.Count > 0 Then
            If _ds.Tables(0).Rows.Count > 0 Then
                Me.txtSShowAs.Text = IIf(IsDBNull(_ds.Tables("MEMBER").Rows(0).Item("SHOW_AS")), _ds.Tables("MEMBER").Rows(0).Item("DISPLAY_AS"), _ds.Tables("MEMBER").Rows(0).Item("SHOW_AS"))
            Else
                Me.txtSShowAs.Text = "UNKNOWN"
            End If
            _dt = _ds.Tables("GOATS")
            ViewState("vsGoats") = _dt
            Me.gvSub.DataSource = _dt
            Me.gvSub.DataBind()
            ResetClassType(_dt)
            'Me.gvSub.Columns(2).Visible = False
            'Me.gvSub.Columns(3).Visible = False
            'Me.gvSub.Columns(4).Visible = False
            'Me.gvSub.Columns(5).Visible = False
        End If

    End Sub
    Public Sub ResetClassType(ByVal _dt As DataTable)
        Dim i As Integer = 0
        For i = 0 To _dt.Rows.Count - 1
            Dim _classtype As String = _dt.Rows(i).Item("CLASS_TYPE").ToString
            Dim _regnum As String = _dt.Rows(i).Item("REG_NUM").ToString
            Me.gvSub.Rows(i).Cells(0).ToolTip = _classtype
            If Not IsDBNull(_regnum) Then
                Me.gvSub.Rows(i).Cells(6).ToolTip = _regnum
            End If
        Next

    End Sub
    Protected Sub gvSub_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvSub.RowCancelingEdit
        divSubMsg.InnerHtml = ""
        gvSub.EditIndex = -1
        Dim _dt As New DataTable
        _dt = ViewState("vsGoats")
        Me.gvSub.DataSource = _dt
        gvSub.DataBind()
        ResetClassType(_dt)
    End Sub

    Protected Sub gvSub_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvSub.RowEditing
        divSubMsg.InnerHtml = ""
        If Me.gvSub.Rows(e.NewEditIndex).Cells(0).ToolTip <> "G" Then ' Individual Class
            gvSub.EditIndex = e.NewEditIndex
            Dim _dt As New DataTable
            _dt = ViewState("vsGoats")
            Me.gvSub.DataSource = _dt
            gvSub.DataBind()
            Dim _bx As TextBox = gvSub.Rows(e.NewEditIndex).FindControl("txtREG_NUM")
            '            _bx.Text = ""
            _bx.Focus()
            '_bx.Attributes.Add("onfocusin", " select();")

        Else 'Group Class
            gvSub.EditIndex = e.NewEditIndex
            Me.txtGroupEntryNum.Text = Me.gvSub.Rows(e.NewEditIndex).Cells(1).Text
            FillGroupList()
            ShowGroupBox()
        End If
    End Sub
    Private Sub FillGroupList()
        Dim _classList(0) As String
        _classList(0) = "NONE"
        _classList = CountDams(_classList)
        _classList = CountSires(_classList)
        _classList = CountEntries(_classList)
        _classList = CountHerd(_classList)

        Dim i As Integer = 0
        ddGroup.Items.Clear()
        For i = 0 To _classList.GetUpperBound(0)
            Me.ddGroup.Items.Add(_classList(i))
        Next
        DisableTaken()

    End Sub
    Private Sub DisableTaken()
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim _totGroups As Integer = 0
        Dim _numGreyed As Integer = 0
        Dim _class As String = ""
        Dim _parentStr As String = ""
        Dim _parent As String = ""
        Dim _start As Integer = 0
        Dim _end As Integer = 0
        For j = 0 To gvSub.Rows.Count - 1
            If Me.gvSub.Rows(j).Cells(0).ToolTip = "G" Then
                _totGroups = _totGroups + 1
            End If
        Next

        For i = 0 To ddGroup.Items.Count - 1
            _class = ddGroup.Items(i).Text
            _start = 0
            _end = 0
            _parentStr = ""
            If _class.IndexOf("(") <> -1 Then ' We have a parent in the string - break it out
                _start = _class.IndexOf("(") + 1
                _end = _class.IndexOf(")")
                _parentStr = _class.Substring(_start, _end - _start)
                Dim _split = _parentStr.Split(",")
                If _split.GetUpperBound(0) = 1 Then
                    _parent = _split(1)
                Else
                    _parent = _split(0)
                End If
                _class = _class.Substring(0, _start - 2)
            End If
            For j = 0 To gvSub.Rows.Count - 1
                If Me.gvSub.Rows(j).Cells(0).ToolTip = "G" Then
                    If _parent <> "" Then
                        If gvSub.Rows(j).Cells(9).Text = _class And gvSub.Rows(j).Cells(6).Text.Replace("&nbsp;", "") = _parent Then
                            ddGroup.Items(i).Attributes.Add("style", "color:#CCCCCC")
                            _numGreyed = _numGreyed + 1
                        End If
                    Else
                        If gvSub.Rows(j).Cells(9).Text = _class Then
                            ddGroup.Items(i).Attributes.Add("style", "color:#CCCCCC")
                            _numGreyed = _numGreyed + 1
                        End If
                    End If
                End If
            Next
            _parent = ""
        Next
        If ddGroup.Items.Count = _numGreyed Then
            btnGrpOk.Disabled = True
        End If
    End Sub
    Protected Sub gvSub_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles gvSub.RowUpdated

    End Sub

    Protected Sub gvSub_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvSub.RowUpdating
        Dim _msg As String = ""
        divSubMsg.InnerHtml = ""
        Dim _bx As TextBox = gvSub.Rows(e.RowIndex).FindControl("txtREG_NUM")
        Dim _dd As DropDownList = gvSub.Rows(e.RowIndex).FindControl("ddCLASS_TYPE")
        Dim _regid As String = _bx.Text
        Dim _classtype As String = _dd.SelectedItem.Text
        If _classtype = "D" Then
            _classtype = "DRY"
        Else
            _classtype = "MILKER"
        End If
        Dim _entryNum As Integer = CInt(gvSub.Rows(e.RowIndex).Cells(1).Text)
        divSubMsg.InnerHtml = ""
        Dim _ws As New wsAdmin()
        If _regid <> "" Then
            If _regid.Length = 8 Then _regid = "P" & _regid
            _regid = _regid.ToUpper
            Dim _ds As DataSet = _ws.GetSubstitute(_regid)
            If _ds.Tables(0).Rows.Count > 0 Then
                Dim _retval As Boolean = WriteSub(_ds.Tables(0), _classtype, _entryNum)
                If _retval = True Then
                    gvSub.EditIndex = -1
                    Dim _dt As New DataTable
                    _dt = ViewState("vsGoats")
                    Me.gvSub.DataSource = _dt
                    gvSub.DataBind()
                    ResetClassType(_dt)
                    _msg = "Entry Successfully Updated"
                    ShowAlert(_msg)
                Else
                    _msg = "Error In Update - Not Performed - possible goat does not qualify for class"
                    ShowAlert(_msg)
                    gvSub.EditIndex = -1
                    Dim _dt As New DataTable
                    _dt = ViewState("vsGoats")
                    Me.gvSub.DataSource = _dt
                    gvSub.DataBind()
                End If
            Else
                _msg = "Goat Not Found"""
                ShowAlert(_msg)
                gvSub.EditIndex = -1
                Dim _dt As New DataTable
                _dt = ViewState("vsGoats")
                gvSub.DataBind()
            End If
        End If
    End Sub

    Public Function WriteSub(ByVal _dt As DataTable, ByVal _classType As String, ByVal _entryNum As Integer) As Boolean
        divSubMsg.InnerHtml = ""
        Dim _ws As New wsAdmin()
        Dim _data(9) As String
        Dim _breed As String = ""
        Dim _classNum As String = ""
        Dim _birthDate As String = _dt.Rows(0).Item(3) ' birthdate
        _data(0) = _dt.Rows(0).Item(7) ' breeder
        _data(1) = _dt.Rows(0).Item(4) ' sire
        _data(2) = _dt.Rows(0).Item(5) ' dam
        _data(3) = _dt.Rows(0).Item(0) ' goat id
        _data(4) = _dt.Rows(0).Item(1) ' reg num
        _data(5) = _dt.Rows(0).Item(2).ToString.Replace("'", "''") ' animal name

        If _dt.Rows(0).Item(1).Substring(0, 1) = "G" Then
            _breed = "E"
        Else
            _breed = _dt.Rows(0).Item(1).Substring(1, 1)
        End If
        _classNum = GetClass(_breed, _birthDate, _classType)
        Dim _split = _classNum.Split(",")
        If _split(1) = -1 Then
            Return False
        Else
            _data(6) = _classType.Substring(0, 1) ' class type
            _data(7) = _split(0) ' class name
            _data(8) = _split(1) ' class number
            _data(9) = _breed ' breed code
            Dim _retval As String = _ws.WriteSubstitute(_data, _entryNum)
            If _retval.IndexOf("SUCCESS") > -1 Then
                FillGoatDataset(Me.txtSMemId.Text)
                Return True
            Else
                Return False
            End If
        End If
    End Function
    Public Function GetClass(ByVal _breed As String, ByVal _birthDate As Date, ByVal _classType As String) As String
        Dim _retVal As String = ""
        Dim i As Integer = 0
        Dim _cName As String = "None"
        Dim _arr() As Integer = Nothing
        Dim _arrPointer As Integer = -1
        If _breed = "E" Then _breed = "G"
        If _breed = "O" Then _breed = "B"
        For i = 0 To _classArrName.GetUpperBound(0)
            If _breed.ToString = _classArrName(i).ToString Then
                _arr = _classArr(i)
                _cName = GetBreedName(_breed)
            End If
        Next
        Select Case _classType
            Case "DRY"
                If (_birthDate >= CDate("04/01/2017") And _birthDate <= CDate("06/01/2017")) Then
                    _arrPointer = 0
                ElseIf (_birthDate >= CDate("03/01/2017") And _birthDate <= CDate("03/31/2017")) Then
                    _arrPointer = 1
                ElseIf (_birthDate >= CDate("01/01/2017") And _birthDate <= CDate("02/28/2017")) Then
                    _arrPointer = 2
                ElseIf (_birthDate >= CDate("05/01/2017") And _birthDate <= CDate("12/31/2016")) Then
                    _arrPointer = 3
                ElseIf (_birthDate >= CDate("07/10/2015") And _birthDate <= CDate("04/30/2016")) Then
                    _arrPointer = 4
                End If
            Case "MILKER"
                If (_birthDate >= CDate("07/10/2015") And _birthDate <= CDate("07/30/2017")) Then
                    _arrPointer = 5
                ElseIf (_birthDate >= CDate("07/10/2014") And _birthDate <= CDate("07/09/2015")) Then
                    _arrPointer = 6
                ElseIf (_birthDate >= CDate("07/10/2013") And _birthDate <= CDate("07/09/2014")) Then
                    _arrPointer = 7
                ElseIf (_birthDate >= CDate("07/10/2012") And _birthDate <= CDate("07/09/2013")) Then
                    _arrPointer = 8
                ElseIf (_birthDate >= CDate("07/10/2010") And _birthDate <= CDate("07/09/2012")) Then
                    _arrPointer = 9
                Else
                    _arrPointer = 10
                End If
            Case Else
                If _classType.Contains("Junior Get of Sire") Then
                    _arrPointer = 12
                ElseIf _classType.Contains("Senior Get of Sire") Then
                    _arrPointer = 15
                ElseIf _classType.Contains("Best 3 Junior Does") Then
                    _arrPointer = 11
                ElseIf _classType.Contains("Best 3 Senior Does") Then
                    _arrPointer = 14
                ElseIf _classType.Contains("Dairy Herd") Then
                    _arrPointer = 13
                ElseIf _classType.Contains("Produce of Dam") Then
                    _arrPointer = 16
                ElseIf _classType.Contains("Dam and Daughter") Then
                    _arrPointer = 17
                Else
                    _arrPointer = -1
                End If

        End Select
        If _arrPointer > -1 Then
            If _classType = "DRY" Or _classType = "MILKER" Then
                _retVal = _cName & " " & CName(_arrPointer) & "," & _arr(_arrPointer)
            Else
                _retVal = _classType & "," & _arr(_arrPointer)
            End If
        Else
            _retVal = "None,-1"
        End If
        Return _retVal
    End Function

    Public Function GetBreedName(ByVal _initial As String) As String
        Dim i As Integer = 0
        Dim _retval As String = ""
        For i = 0 To BreedNames.GetUpperBound(0) - 1 Step 2
            If BreedNames(i).ToString = _initial.ToString Then
                _retval = BreedNames(i + 1).ToString
                Exit For
            End If
        Next
        Return _retval
    End Function
    Public Function CountEntries(ByVal _classList() As String) As Array
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim _numDry As Integer = 0
        Dim _numMilkers As Integer = 0
        Dim _breed As String = ""
        Dim _classArr(_classArrName.GetUpperBound(0), 1) As String
        ' DRY
        For i = 0 To _classArrName.GetUpperBound(0)
            _classArr(i, 0) = _classArrName(i)
            _classArr(i, 1) = 0
        Next

        For i = 0 To Me.gvSub.Rows.Count - 1
            If Me.gvSub.Rows(i).Cells(0).ToolTip = "D" Then
                _breed = Me.gvSub.Rows(i).Cells(11).Text
                For j = 0 To _classArr.GetUpperBound(0)
                    If _classArr(j, 0) = _breed Then
                        _classArr(j, 1) = CInt(_classArr(j, 1)) + 1
                        Exit For
                    End If
                Next
            End If
        Next

        ' Loop through the breed array and see if counts > 2
        For i = 0 To _classArr.GetUpperBound(0)
            If _classArr(i, 1) > 2 Then
                _breed = GetBreedName(_classArr(i, 0))
                If _classList(0) <> "NONE" Then
                    ReDim Preserve _classList(_classList.GetUpperBound(0) + 1)
                End If
                _classList(_classList.GetUpperBound(0)) = _breed & " Best 3 Junior Does"
            End If
        Next

        ' MILKER
        For i = 0 To _classArr.GetUpperBound(0)
            _classArr(i, 1) = 0
        Next

        For i = 0 To Me.gvSub.Rows.Count - 1
            If Me.gvSub.Rows(i).Cells(0).ToolTip = "M" Then
                _breed = Me.gvSub.Rows(i).Cells(11).Text
                For j = 0 To _classArr.GetUpperBound(0)
                    If _classArr(j, 0) = _breed Then
                        _classArr(j, 1) = CInt(_classArr(j, 1)) + 1
                        Exit For
                    End If
                Next
            End If
        Next

        ' Loop through the breed array and see if counts > 2
        For i = 0 To _classArr.GetUpperBound(0)
            If _classArr(i, 1) > 2 Then
                _breed = GetBreedName(_classArr(i, 0))
                If _classList(0) <> "NONE" Then
                    ReDim Preserve _classList(_classList.GetUpperBound(0) + 1)
                End If
                _classList(_classList.GetUpperBound(0)) = _breed & " Best 3 Senior Does"
            End If
        Next


        Return _classList
    End Function

    Public Function CountDams(ByVal _classList() As String) As Array
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim _damList(0) As String
        Dim _breed As String = ""
        ' First, build an array of dam id's for all individual entries
        For i = 0 To Me.gvSub.Rows.Count - 1

            If Me.gvSub.Rows(i).Cells(0).ToolTip <> "G" Then
                Dim _inList As Boolean = False
                For j = 0 To _damList.GetUpperBound(0)
                    If _damList(j) = Me.gvSub.Rows(i).Cells(4).Text Then
                        _inList = True
                        Exit For
                    End If
                Next
                If ((_inList = False) And (Me.gvSub.Rows(i).Cells(4).Text <> "&nbsp;")) Then
                    If Not IsNothing(_damList(0)) Then
                        ReDim Preserve _damList(_damList.GetUpperBound(0) + 1)
                    End If
                    _damList(_damList.GetUpperBound(0)) = Me.gvSub.Rows(i).Cells(4).Text
                End If
            End If
        Next

        ' Transfer the dam id to the master array, column 0
        Dim _masterList(_damList.GetUpperBound(0), 3) As String
        For i = 0 To _damList.GetUpperBound(0)
            _masterList(i, 0) = _damList(i)
            _masterList(i, 1) = "N"
            _masterList(i, 2) = 0
            _masterList(i, 3) = ""
        Next

        ' Loop through the dams and check the grid to see if any are showing - set master array column 1 to "Y"
        For i = 0 To _masterList.GetUpperBound(0)
            For j = 0 To gvSub.Rows.Count - 1
                If Me.gvSub.Rows(j).Cells(0).ToolTip = "M" Then
                    If _masterList(i, 0) = gvSub.Rows(j).Cells(5).Text Then
                        _masterList(i, 1) = "Y"
                        Exit For
                    End If
                End If
            Next
        Next

        For i = 0 To _masterList.GetUpperBound(0)
            For j = 0 To gvSub.Rows.Count - 1
                If Me.gvSub.Rows(j).Cells(0).ToolTip = "M" Then
                    If _masterList(i, 0) = gvSub.Rows(j).Cells(4).Text Then
                        _masterList(i, 2) = CInt(_masterList(i, 2)) + 1
                    End If
                End If
            Next
        Next
        'Dim _ws As New localhost.clsNationalShow
        Dim _ws As New wsAdmin()
        Dim _split() As String

        For i = 0 To _masterList.GetUpperBound(0)
            If _masterList(i, 1) = "Y" Then 'Dam and Daughter
                _split = _ws.GetRegAndName(_masterList(i, 0)).Split(",")
                If _classList(0) <> "NONE" Then
                    ReDim Preserve _classList(_classList.GetUpperBound(0) + 1)
                End If
                _breed = GetBreedName(_split(0).Substring(1, 1))
                If _classList(0) <> "NONE" Then
                    _classList(_classList.GetUpperBound(0)) = _breed & " Dam and Daughter (" & _split(0) & "," & _split(1) & ")"
                End If
            End If
            If _masterList(i, 2) > 1 Then 'Produce of Dam
                _split = _ws.GetRegAndName(_masterList(i, 0)).Split(",")
                If _classList(0) <> "NONE" Then
                    ReDim Preserve _classList(_classList.GetUpperBound(0) + 1)
                End If
                _breed = GetBreedName(_split(0).Substring(1, 1))
                _classList(_classList.GetUpperBound(0)) = _breed & " Produce of Dam (" & _split(0) & "," & _split(1) & ")"
            End If
        Next
        Return _classList
    End Function
    Public Function CountSires(ByVal _classList() As String) As Array
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim _sireList(0) As String
        Dim _breed As String = ""
        ' First, build an array of sire id's for all individual entries
        For i = 0 To Me.gvSub.Rows.Count - 1
            If Me.gvSub.Rows(i).Cells(0).ToolTip <> "G" Then
                Dim _inList As Boolean = False
                For j = 0 To _sireList.GetUpperBound(0)
                    If _sireList(j) = Me.gvSub.Rows(i).Cells(3).Text Then
                        _inList = True
                        Exit For
                    End If
                Next
                If ((_inList = False) And (Me.gvSub.Rows(i).Cells(3).Text <> "&nbsp;")) Then
                    If Not IsNothing(_sireList(0)) Then
                        ReDim Preserve _sireList(_sireList.GetUpperBound(0) + 1)
                    End If
                    _sireList(_sireList.GetUpperBound(0)) = Me.gvSub.Rows(i).Cells(3).Text
                End If
            End If
        Next
        ' Transfer the sire id to the master array, column 0
        Dim _masterList(_sireList.GetUpperBound(0), 2) As String
        For i = 0 To _sireList.GetUpperBound(0)
            _masterList(i, 0) = _sireList(i)
            _masterList(i, 1) = 0
            _masterList(i, 2) = 0
        Next
        For i = 0 To Me.gvSub.Rows.Count - 1
            If Me.gvSub.Rows(i).Cells(0).ToolTip = "M" Then
                For j = 0 To _masterList.GetUpperBound(0)
                    If Me.gvSub.Rows(i).Cells(3).Text = _masterList(j, 0) Then
                        _masterList(j, 1) = CInt(_masterList(j, 1)) + 1
                        Exit For
                    End If
                Next
            End If
        Next

        For i = 0 To Me.gvSub.Rows.Count - 1
            If Me.gvSub.Rows(i).Cells(0).ToolTip = "D" Then
                For j = 0 To _masterList.GetUpperBound(0)
                    If Me.gvSub.Rows(i).Cells(3).Text = _masterList(j, 0) Then
                        _masterList(j, 2) = CInt(_masterList(j, 2)) + 1
                        Exit For
                    End If
                Next
            End If
        Next

        'Dim _ws As New localhost.clsNationalShow
        Dim _ws As New wsAdmin()
        Dim _split() As String
        For i = 0 To _masterList.GetUpperBound(0)
            If CInt(_masterList(i, 2)) > 2 Then 'Dry
                _split = _ws.GetRegAndName(_masterList(i, 0)).Split(",")
                If _classList(0) <> "NONE" Then
                    ReDim Preserve _classList(_classList.GetUpperBound(0) + 1)
                End If
                If (_split(0) <> "NONE") Then
                    _breed = GetBreedName(_split(0).Substring(1, 1))
                    _classList(_classList.GetUpperBound(0)) = _breed & " Junior Get of Sire (" & _split(0) & "," & _split(1) & ")"
                    If CInt(_masterList(i, 1)) > 2 Then 'Milker
                        _split = _ws.GetRegAndName(_masterList(i, 0)).Split(",")
                        If _classList(0) <> "NONE" Then
                            ReDim Preserve _classList(_classList.GetUpperBound(0) + 1)
                        End If
                        _breed = GetBreedName(_split(0).Substring(1, 1))
                        _classList(_classList.GetUpperBound(0)) = _breed & " Senior Get of Sire (" & _split(0) & "," & _split(1) & ")"
                    End If
                End If
            End If
        Next
        Return _classList
    End Function

    Public Function CountHerd(ByVal _classList() As String) As Array
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim _breed As String = ""
        Dim _classArr(_classArrName.GetUpperBound(0), 1) As String

        For i = 0 To _classArrName.GetUpperBound(0)
            _classArr(i, 0) = _classArrName(i)
            _classArr(i, 1) = 0
        Next
        For i = 0 To Me.gvSub.Rows.Count - 1
            If Me.gvSub.Rows(i).Cells(0).ToolTip = "M" Then
                _breed = Me.gvSub.Rows(i).Cells(6).ToolTip.Substring(1, 1)
                For j = 0 To _classArr.GetUpperBound(0)
                    If _classArr(j, 0) = _breed Then
                        _classArr(j, 1) = CInt(_classArr(j, 1)) + 1
                        Exit For
                    End If
                Next
            End If
        Next

        For i = 0 To _classArr.GetUpperBound(0)
            If _classArr(i, 1) > 3 Then
                _breed = GetBreedName(_classArr(i, 0))
                If _classList(0) <> "NONE" Then
                    ReDim Preserve _classList(_classList.GetUpperBound(0) + 1)
                End If
                _classList(_classList.GetUpperBound(0)) = _breed & " Dairy Herd"

            End If
        Next
        Return _classList
    End Function

    Protected Sub btnGrpOk_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrpOk.ServerClick
        Dim _msg As String = ""
        divSubMsg.InnerHtml = ""
        Dim _entryNum As String = ""
        Dim _class As String = ""
        Dim _className As String = ""
        Dim _classNum As String = ""
        Dim _split() As String
        Dim _parentName As String = ""
        Dim _parentNum As String = ""
        Dim _start As Integer = 0
        Dim _end As Integer = 0
        Dim _classArrIndex() As String
        Dim _data(4) As String
        _entryNum = Me.txtGroupEntryNum.Text
        If Me.ddGroup.Visible = True Then
            _class = ddGroup.SelectedItem.Text
        Else
            If Me.txtGroupParent.Text.Length > 0 Then
                'Dim _ws As New localhost.clsNationalShow
                Dim _ws As New wsAdmin()
                Dim _ds As DataSet = _ws.GetSubstitute(Me.txtGroupParent.Text.ToUpper)
                Dim _regId As String = _ds.Tables(0).Rows(0).Item("OLD_REG_NUM")
                Dim _goatName As String = _ds.Tables(0).Rows(0).Item("GOAT_NAME").ToString.Replace("'", "''")
                _class = ddGroupAll.SelectedItem.Text & " (" & _regId & "," & _goatName & ")"
            Else
                _class = ddGroupAll.SelectedItem.Text
            End If
        End If
        If _class.IndexOf("(") <> -1 Then ' We have a parent in the string - break it out
            _start = _class.IndexOf("(") + 1
            _end = _class.IndexOf(")")
            _split = _class.Substring(_start, _end - _start).Split(",")
            _parentNum = _split(0)
            _parentName = _split(1)
            _class = _class.Substring(0, _start - 2)
        End If
        _classArrIndex = GetClass(_class.Substring(0, 1), Now(), _class).Split(",")
        _classNum = _classArrIndex(1)
        If _classNum > -1 Then
            If _parentNum <> "" Then
                _data(0) = _parentNum
            Else
                _data(0) = ""
            End If
            If _parentName <> "" Then
                _data(1) = _parentName.Replace("'", "''")
            Else
                _data(1) = ""
            End If
            _data(2) = _class
            _data(3) = _classNum
            '_data(4) = _class.Substring(0, 1)
            _data(4) = GetDivision(_class)
            Dim _ws As New wsAdmin()
            Dim _retval As String = _ws.WriteGroupSubstitute(_data, _entryNum)
            If _retval.IndexOf("SUCCESS") > -1 Then
                FillGoatDataset(Me.txtSMemId.Text)
                ChangeDataTable(_data, _entryNum)
                gvSub.EditIndex = -1
                _msg = "Entry Successfully Updated"
                ShowAlert(_msg)
            Else
                _msg = "Error In Update - Not Performed"
                ShowAlert(_msg)
            End If
        Else
            _msg = "Invalid Class Number Returned"
            ShowAlert(_msg)
        End If
    End Sub
    Public Function GetDivision(Str As String) As String
        If (InStr(Str.ToUpper(), "NIGERIAN")) Then
            Return "D"
        ElseIf (InStr(Str.ToUpper(), "ALPINE")) Then
            Return "A"
        ElseIf (InStr(Str.ToUpper(), "OBERHASLI")) Then
            Return "B"
        ElseIf (InStr(Str.ToUpper(), "NUBIAN")) Then
            Return "N"
        ElseIf (InStr(Str.ToUpper(), "SAANEN")) Then
            Return "S"
        ElseIf (InStr(Str.ToUpper(), "ALPINE")) Then
            Return "A"
        ElseIf (InStr(Str.ToUpper(), "LAMANCHA")) Then
            Return "L"
        ElseIf (InStr(Str.ToUpper(), "RECORDED GRADE")) Then
            Return "G"
        ElseIf (InStr(Str.ToUpper(), "TOGGENBURG")) Then
            Return "T"
        ElseIf (InStr(Str.ToUpper(), "SABLE")) Then
            Return "C"
        Else
            Return "X"
        End If
    End Function

    Public Sub ChangeDataTable(ByVal _data() As String, ByVal _entryNum As String)
        Dim i As Integer = 0
        Dim _dt As New DataTable
        _dt = ViewState("vsGoats")
        For i = 0 To _dt.Rows.Count - 1
            If _dt.Rows(i).Item(2).ToString = _entryNum Then
                _dt.Rows(i).Item(13) = ""
                _dt.Rows(i).Item(14) = ""
                _dt.Rows(i).Item(16) = ""
                _dt.Rows(i).Item(17) = ""
                _dt.Rows(i).Item(13) = _data(0)
                _dt.Rows(i).Item(14) = _data(1)
                _dt.Rows(i).Item(15) = "G"
                _dt.Rows(i).Item(16) = _data(2)
                _dt.Rows(i).Item(17) = _data(3)
                Exit For
            End If
        Next
        Me.gvSub.DataSource = _dt
        gvSub.DataBind()
        ResetClassType(_dt)
        For i = 0 To gvSub.Rows.Count - 1
            If gvSub.Rows(i).Cells(1).Text = _entryNum Then
                gvSub.Rows(i).Cells(0).ToolTip = "G"
                Exit For
            End If
        Next
    End Sub

    Protected Sub chkOverride_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOverride.CheckedChanged
        If Me.chkOverride.Checked Then
            FillAllGroups()
            Me.ddGroup.Visible = False
            Me.ddGroupAll.Visible = True
            Me.txtGroupParent.Visible = True
        Else
            Me.ddGroup.Visible = True
            Me.ddGroupAll.Visible = False
            Me.txtGroupParent.Visible = False
            FillGroupList()
            DisableTaken()
        End If
        'ShowGroupBox()
    End Sub
    Private Sub FillAllGroups()
        Me.ddGroup.Items.Clear()
        'gvSub.EditIndex = e.NewEditIndex
        'Me.txtGroupEntryNum.Text = Me.gvSub.Rows(e.NewEditIndex).Cells(1).Text
        FillGroupList()
        ShowGroupBox()

    End Sub

    Protected Sub btnGrpCancel_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrpCancel.ServerClick
        Me.chkOverride.Checked = False
        gvSub.EditIndex = -1
    End Sub


End Class
