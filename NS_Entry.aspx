﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="NS_Entry.aspx.vb" Inherits="NS_Entry" EnableEventValidation="false" %>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="obout_Interface" Namespace="Obout.Interface" TagPrefix="cc2" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CrystalDecisions.Shared" %>
<%@ Import Namespace="CrystalDecisions.CrystalReports.Engine" %>
<%@ Import Namespace="CrystalDecisions.Web" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ADGA NATIONAL SHOW ONLINE ADMINISTRATION</title>
        <style type="text/css">
        .hdrlbl{text-align: center; border-style: none; border-width: 0px; background-color:White; font-family:Tahoma; font-size: 10pt; color: #849eb5; font-weight: bold;}
        .hdr{text-align: left; border-style: none; border-width: 0px; font-family:Tahoma; font-size: 9pt; color: #849eb5; font-weight: bold;}
        .mr{margin-left:30px; height:50px;}
		.modalPopup
		{
		background-color: #696969;
		filter: alpha(opacity=40);
		opacity: 0.7;
		xindex:-1;
		}
 		.modalbuttons
		{
			font-weight: bold; 
            font-family: Verdana;
            font-size: 9pt;
            text-align: center;
		}
		.modalBackground 
        {
	        filter:alpha(opacity=70);
	        opacity:0.7;
	        background-color:Gray;
        }
         .detail
	    {
		    font-family: verdana;
		    font-size:8px;
		    background-color:#fff;
		    border:solid 2px #000;
		    padding:4px;
		    margin-top: 0px;
	    }
         .lbl
        {
        	font-family: Verdana;
        	font-size: 8pt;
        }
        .cbox
        {
         	font-family: Verdana;
        	font-size: 8pt;
       }
        .bx
        {
        	font-family: Verdana;
        	font-size: 8pt;
        	width: 50px;
        }
        .name
        {
        	font-family: Verdana;
        	font-size: 8pt;
        	width: 300px;
        	border-style: none;
        	border-width: 0px;
        	text-align: left;
/*         	background-color: #F7F6F3; */
         	background-color:Transparent;

       }
        .num
        {
        	font-family: Verdana;
        	font-size: 8pt;
        	width: 100px;
        	border-style: none;
        	border-width: 0px;
        	text-align: left;
/*         	background-color: #F7F6F3; */
         	background-color:Transparent;
       }
        .lblcol
        {
        	width: 150px;
        }
        .lcol
        {
        	width: 50px;
        }
        .mcol
        {
        	width: 325px;
        	text-align: left;
        }
        .drop
        {
 	       font-family: Verdana;
        	font-size: 8pt;
        	width: 200px;
        }
        .div
        {
           /* background-color: #F7F6F3; */       	
            width: 900px;
        }

    </style>
<%--   <script language="javascript" type="text/javascript" src="./NS_Entry_Javs.js"></script>--%>
       <script language="javascript" type="text/javascript" src="http://web.adga.org/NSEntry2/NS_Entry_Javs.js"></script>
</head>
<body>
    <form id="frmMain" style="width: 1000px; height:1800px;background-image: url(form_border_blank.png); background-position:top left; background-repeat:no-repeat;" runat="server" >

    <asp:ScriptManager ID="scrMain" runat="server"  AsyncPostBackTimeout="300">
                <Services><asp:ServiceReference Path="wsProxy.asmx" InlineScript="true" /></Services>
<%--        <Services>
            <asp:ServiceReference Path="NationalShow.asmx" InlineScript="true" /></Services>--%>
    </asp:ScriptManager>
         <table id="tblMenu" runat="server" style="width:900px;" cellpadding="0" cellspacing="0">
            <tr><td class="hdrlbl">ADGA National Show 2017 Online Administration</td></tr>
            <tr><td>&nbsp;</td></tr>
          </table>
          
        <asp:Menu ID="mnuMain" runat="server" Backcolor="#F7F6F3" DynamicHorizontalOffset="2" 
            Font-Bold="True" Font-Names="Times New Roman" Font-Size="Small" ForeColor="MidnightBlue"
            Orientation="Horizontal" StaticSubMenuIndent="10px" style="width: 200px; position: static; height: 15px;">
            <StaticSelectedStyle BackColor="White" />
            <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px"  />
            <DynamicHoverStyle BackColor="#7C6F57" ForeColor="White" />
            <DynamicMenuStyle BackColor="#F7F6F3" />
            <DynamicSelectedStyle BackColor="#5D7B9D" />
            <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
            <StaticHoverStyle BackColor="#7C6F57" ForeColor="White" />
            <Items>
              <asp:MenuItem SeparatorImageUrl="./mnu_sep.JPG" Text="Substitute" Value="Substitute"></asp:MenuItem>
              <asp:MenuItem SeparatorImageUrl="./mnu_sep.JPG" Text="Check In" Value="Check In"></asp:MenuItem>
              <asp:MenuItem SeparatorImageUrl="./mnu_sep.JPG"  Text="Placings" Value="Placings"></asp:MenuItem>
               <asp:MenuItem SeparatorImageUrl="./mnu_sep.JPG"  Text="Reports" Value="Reports"></asp:MenuItem>
              <asp:MenuItem SeparatorImageUrl="./mnu_sep.JPG" Text="Add" Value="Add"></asp:MenuItem>
          </Items>
        </asp:Menu>
        <br />
        <br />
        <div id="divSubMsg" runat="server" style="color:red; font-weight:bold;"></div>
        <br />
        <br />
        <asp:MultiView ID="mviewMain" runat="server" ActiveViewIndex="0" >
             <asp:View ID="vwSub" runat="server" >
              <div id="divSub" class="div">   
                <center><h4 id="headSub" runat="server" style="color:#849eb5;">SUBSTITUTION FORM</h4></center>
                <br />
                    <center><asp:Button ID="btnClearSub" runat="server" CssClass="lbl" Text="Clear All" OnClientClick="ClearSub();" /></center> 
                <br />
             <table id="Table2" width="700px">
             <tr>
                <td>&nbsp;</td>
                <td class="lbl">Member ID&nbsp;&nbsp;<asp:TextBox ID="txtSMemId" runat="server" 
                        CssClass="bx" style="width: 75px;" AutoPostBack="true"></asp:TextBox></td>
                 <td class="lbl">&nbsp;&nbsp;Exhibitor&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtSShowAs" runat="server" CssClass="name"></asp:TextBox></td>
          </tr>
            </table>
            <br />
            <br />
            <center>
                <div id="divGridview" runat="server">
                     
               <asp:GridView ID="gvSub" runat="server" AutoGenerateColumns="False"
                     CellPadding="4" Font-Names="Verdana" Font-Size="8pt" ForeColor="#333333" 
                     GridLines="None" DataKeyNames="ENTRY_NUM" >
                     <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                     <Columns>
                          <asp:CommandField ButtonType="Link" ShowEditButton="true" EditText="Substitute" ShowCancelButton="true" />
                          <asp:BoundField DataField="ENTRY_NUM" HeaderText="Entry" ReadOnly="true" />
                         <asp:BoundField DataField="BREEDER_ID" HeaderText="Breeder"  ReadOnly="true" />
                         <asp:BoundField DataField="SIRE_ID" HeaderText="Sire" ReadOnly="true" />
                         <asp:BoundField DataField="DAM_ID" HeaderText="Dam" ReadOnly="true" />
                         <asp:BoundField DataField="GOAT_ID" HeaderText="Goat Id" ReadOnly="true" />
                         <asp:BoundField DataField="ANM_NAME" HeaderText="Name" ReadOnly="true" ItemStyle-HorizontalAlign="Left" />
                             <asp:TemplateField HeaderText="Reg Id">
                              <ItemTemplate>
                                <%#Eval("REG_NUM")%>
                              </ItemTemplate>
                              <EditItemTemplate>
                                <asp:TextBox runat="server" ID="txtREG_NUM" Text='<%# Eval("REG_NUM")%>' CssClass="cbox" />
                              </EditItemTemplate>
                            </asp:TemplateField>     
                             <asp:TemplateField HeaderText="Class Type">
                              <ItemTemplate>
                                <%#Eval("CLASS_TYPE")%>
                              </ItemTemplate>
                              <EditItemTemplate>
                                <asp:DropDownList runat="server" ID="ddCLASS_TYPE" Text='<%# Eval("CLASS_TYPE")%>' CssClass="cbox">
                   		            <asp:ListItem>M</asp:ListItem>
                   		            <asp:ListItem>D</asp:ListItem>
                   		            <asp:ListItem>G</asp:ListItem>
                 	            </asp:DropDownList>
                              </EditItemTemplate>
                            </asp:TemplateField>                                                  
<%--                         <asp:BoundField DataField="CLASS_TYPE" HeaderText="Class Type" ReadOnly="true" />
--%>                         
                         <asp:BoundField DataField="CLASS_NAME" HeaderText="Class Name" ReadOnly="true" ItemStyle-HorizontalAlign="Left" />
                         <asp:BoundField DataField="CLASS_NUM" HeaderText="Class Num" ReadOnly="true" />
                         <asp:BoundField DataField="BREED_CODE" HeaderText="Breed" ReadOnly="true" />
                     </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                     <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                     <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"  Font-Names="Verdana" Font-Size="8pt" />
                     <EditRowStyle BackColor="#E1E1E1" />
                     <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                 
                 </asp:GridView>
                 </div>
                 </center>
        </div>       
          </asp:View>
          
            <asp:View ID="vwCheckIn" runat="server">
               <div id="divCheck" class="div">   
                <center><h4 id="h1" runat="server" style="color:#849eb5;">CHECK IN FORM</h4></center>
                <br />
           <table id="Table1" width="900px">
             <tr>
                <td>&nbsp;</td>
                <td class="lbl">Member ID&nbsp;&nbsp;<asp:TextBox ID="txtMemId" runat="server" CssClass="bx" style="width: 75px;" OnKeyPress="OnMemberPress(event);"></asp:TextBox></td>
                 <td class="lbl">&nbsp;&nbsp;<asp:TextBox ID="txtShowAs" runat="server" CssClass="name"></asp:TextBox></td>
          </tr>
            </table>
            
        <div id="divCheckin">
        
        <table id="tblCheckin" width="900px">
              <tr>
                <td>&nbsp;</td>
                <td class="lbl">Entry #</td>
                 <td class="lbl">Reg Id</td>
                  <td class="lbl">Animal Name</td>
                 <td class="lbl">Class Number</td>
          </tr>
          <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                 <td>&nbsp;</td>
                  <td>&nbsp;</td>
                 <td>&nbsp;</td>
          </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace1" runat="server" CssClass="lbl" Text="1." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox  ID="textCEntry1" runat="server" CssClass="bx" tabindex="0" OnKeyPress="OnCKeyPress('textCEntry2', event); "></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum1" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName1" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCMName1" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace2" runat="server" CssClass="lbl" Text="2." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry2" runat="server" CssClass="bx" tabindex="1"  OnKeyPress="OnCKeyPress('textCEntry3', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum2" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName2" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCMName2" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace3" runat="server" CssClass="lbl" Text="3." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry3" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry4', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum3" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName3" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCMName3" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>

            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace4" runat="server" CssClass="lbl" Text="4." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry4" runat="server" CssClass="bx" tabindex="3"  OnKeyPress="OnCKeyPress('textCEntry5', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum4" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName4" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCMName4" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
           </tr>

              <tr>
                <td class="lcol"><asp:Label ID="lblCPlace5" runat="server" CssClass="lbl" Text="5." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry5" runat="server" CssClass="bx" tabindex="4"  OnKeyPress="OnCKeyPress('textCEntry6', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum5" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName5" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCMName5" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace6" runat="server" CssClass="lbl" Text="6." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry6" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry7', event)"></asp:TextBox></td>
             <td class="lcol"><asp:TextBox ID="txtCANum6" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName6" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCMName6" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
           </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace7" runat="server" CssClass="lbl" Text="7." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry7" runat="server" CssClass="bx" tabindex="5"  OnKeyPress="OnCKeyPress('textCEntry8', event)"></asp:TextBox></td>
               <td class="lcol"><asp:TextBox ID="txtCANum7" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCAName7" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCMName7" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
           </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace8" runat="server" CssClass="lbl" Text="8." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry8" runat="server" CssClass="bx" tabindex="6"  OnKeyPress="OnCKeyPress('textCEntry9', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum8" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName8" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName8" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace9" runat="server" CssClass="lbl" Text="9." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry9" runat="server" CssClass="bx" tabindex="7"  OnKeyPress="OnCKeyPress('textCEntry10', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum9" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName9" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCMName9" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
           </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace10" runat="server" CssClass="lbl" Text="10." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry10" runat="server" CssClass="bx" tabindex="8"  OnKeyPress="OnCKeyPress('textCEntry11', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum10" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName10" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCMName10" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
              <tr>
                <td class="lcol"><asp:Label ID="lblCPlace11" runat="server" CssClass="lbl" Text="11." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry11" runat="server" CssClass="bx" tabindex="8"  OnKeyPress="OnCKeyPress('textCEntry12', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum11" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName11" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName11" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace12" runat="server" CssClass="lbl" Text="12." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry12" runat="server" CssClass="bx" tabindex="10"  OnKeyPress="OnCKeyPress('textCEntry13', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum12" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName12" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCMName12" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
           </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace13" runat="server" CssClass="lbl" Text="13." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry13" runat="server" CssClass="bx" tabindex="11"  OnKeyPress="OnCKeyPress('textCEntry14', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum13" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName13" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCMName13" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace14" runat="server" CssClass="lbl" Text="14." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry14" runat="server" CssClass="bx" tabindex="12"  OnKeyPress="OnCKeyPress('textCEntry15', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum14" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName14" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCMName14" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
           </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace15" runat="server" CssClass="lbl" Text="15." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry15" runat="server" CssClass="bx" tabindex="13"  OnKeyPress="OnCKeyPress('textCEntry16', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum15" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName15" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCMName15" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace16" runat="server" CssClass="lbl" Text="16." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry16" runat="server" CssClass="bx" tabindex="13"  OnKeyPress="OnCKeyPress('textCEntry17', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum16" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName16" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName16" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace17" runat="server" CssClass="lbl" Text="17." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry17" runat="server" CssClass="bx" tabindex="13"  OnKeyPress="OnCKeyPress('textCEntry18', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum17" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName17" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName17" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace18" runat="server" CssClass="lbl" Text="18." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry18" runat="server" CssClass="bx" tabindex="13"  OnKeyPress="OnCKeyPress('textCEntry19', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum18" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName18" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName18" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace19" runat="server" CssClass="lbl" Text="19." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry19" runat="server" CssClass="bx" tabindex="13"  OnKeyPress="OnCKeyPress('textCEntry20', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum19" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName19" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCMName19" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace20" runat="server" CssClass="lbl" Text="20." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry20" runat="server" CssClass="bx" tabindex="13"  OnKeyPress="OnCKeyPress('textCEntry21', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtCANum20" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName20" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCMName20" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
              <tr>
                <td class="lcol"><asp:Label ID="lblCPlace21" runat="server" CssClass="lbl" Text="21." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry21" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry22', event)"></asp:TextBox></td>
             <td class="lcol"><asp:TextBox ID="txtCANum21" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtCAName21" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCMName21" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace22" runat="server" CssClass="lbl" Text="22." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry22" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry23', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum22" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName22" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCMName22" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace23" runat="server" CssClass="lbl" Text="23." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry23" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry24', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum23" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName23" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName23" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace24" runat="server" CssClass="lbl" Text="24." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry24" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry25', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum24" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName24" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName24" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace25" runat="server" CssClass="lbl" Text="25." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry25" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry26', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum25" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName25" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName25" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace26" runat="server" CssClass="lbl" Text="26." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry26" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry27', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum26" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName26" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName26" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace27" runat="server" CssClass="lbl" Text="27." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry27" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry28', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum27" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName27" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName27" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace28" runat="server" CssClass="lbl" Text="28." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry28" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry29', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum28" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName28" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName28" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace29" runat="server" CssClass="lbl" Text="29." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry29" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry30', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum29" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName29" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName29" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
           <tr>
                <td class="lcol"><asp:Label ID="lblCPlace30" runat="server" CssClass="lbl" Text="30." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry30" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry31', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum30" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName30" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName30" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace31" runat="server" CssClass="lbl" Text="31." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry31" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry32', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum31" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName31" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName31" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace32" runat="server" CssClass="lbl" Text="32." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry32" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry33', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum32" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName32" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName32" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace33" runat="server" CssClass="lbl" Text="33." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry33" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry34', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum33" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName33" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName33" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace34" runat="server" CssClass="lbl" Text="34." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry34" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry35', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum34" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName34" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName34" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace35" runat="server" CssClass="lbl" Text="35." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry35" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry36', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum35" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName35" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName35" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace36" runat="server" CssClass="lbl" Text="36." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry36" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry37', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum36" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName36" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName36" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace37" runat="server" CssClass="lbl" Text="37." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry37" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry38', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum37" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName37" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName37" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace38" runat="server" CssClass="lbl" Text="38." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry38" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry39', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum38" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName38" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName38" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace39" runat="server" CssClass="lbl" Text="39." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry39" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry40', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum39" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName39" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName39" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
            <tr>
                <td class="lcol"><asp:Label ID="lblCPlace40" runat="server" CssClass="lbl" Text="40." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry40" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry41', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum40" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName40" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName40" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace41" runat="server" CssClass="lbl" Text="41." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry41" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry42', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum41" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName41" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName41" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace42" runat="server" CssClass="lbl" Text="42." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry42" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry43', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum42" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName42" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName42" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace43" runat="server" CssClass="lbl" Text="43." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry43" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry44', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum43" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName43" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName43" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace44" runat="server" CssClass="lbl" Text="44." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry44" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry45', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum44" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName44" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName44" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lcol"><asp:Label ID="lblCPlace45" runat="server" CssClass="lbl" Text="45." TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textCEntry45" runat="server" CssClass="bx" tabindex="2"  OnKeyPress="OnCKeyPress('textCEntry45', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtCANum45" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtCAName45" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtCMName45" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
            <tr>
                <td colspan="5" style="text-align: center;"><input id="btnCSubmit" type="button" value="Submit" runat="server" /></td>
            </tr>
 </table>  

        </div>
</div>
            </asp:View>
            
             <asp:View ID="vwResults" runat="server">
               <div id="divPlace" class="div">   
                <center><h4 id="h2" runat="server" style="color:#849eb5;">RESULTS FORM</h4>
                <input id="btnSubmit" type="button" value="Submit" runat="server" /></center>
                <br />
            <table id="tblHeader" width="700px">
            <tr>
                <td>&nbsp;</td>
                <td class="lbl">Breed&nbsp;&nbsp;<select id="ddbreed" runat="server" class="drop"  onchange="FilledDrops()" /></td>
                 <td class="lbl">Class&nbsp;&nbsp;<select id="ddclass" runat="server" class="drop" onchange="FilledDrops()" /></td>
                  <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtClassNum" runat="server" CssClass="cbox" Width="48px"></asp:TextBox></td>
          </tr>
            </table>
            
        <div id="divResults">
        
        <table id="tblMain" width="800px">
             <tr>
                <td>&nbsp;</td>
                <td class="lbl">Entry #</td>
                 <td class="lbl">Reg Id</td>
                  <td class="lbl">Animal Name</td>
                 <td class="lbl">Exhibitor</td>
          </tr>
          <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                 <td>&nbsp;</td>
                  <td>&nbsp;</td>
                 <td>&nbsp;</td>
          </tr>
            <tr>
                <td class="lblcol"><asp:Label ID="lblPlace1" runat="server" CssClass="lbl" Text="1st" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox  ID="textEntry1" runat="server" CssClass="bx" tabindex="0" onkeypress="OnKeyPress('textEntry2', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum1" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName1" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtMName1" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace2" runat="server" CssClass="lbl" Text="2nd" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry2" runat="server" CssClass="bx" tabindex="1"  onkeypress="OnKeyPress('textEntry3', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum2" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName2" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtMName2" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="lblcol"><asp:Label ID="lblPlace3" runat="server" CssClass="lbl" Text="3rd" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry3" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry4', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum3" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName3" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtMName3" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>

            <tr>
                <td class="lblcol"><asp:Label ID="lblPlace4" runat="server" CssClass="lbl" Text="4th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry4" runat="server" CssClass="bx" tabindex="3"  onkeypress="OnKeyPress('textEntry5', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum4" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName4" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtMName4" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
           </tr>

              <tr>
                <td class="lblcol"><asp:Label ID="lblPlace5" runat="server" CssClass="lbl" Text="5th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry5" runat="server" CssClass="bx" tabindex="4"  onkeypress="OnKeyPress('textEntry6', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum5" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName5" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtMName5" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="lblcol"><asp:Label ID="lblPlace6" runat="server" CssClass="lbl" Text="6th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry6" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry7', event)"></asp:TextBox></td>
             <td class="lcol"><asp:TextBox ID="txtANum6" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName6" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtMName6" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
           </tr>
            <tr>
                <td class="lblcol"><asp:Label ID="lblPlace7" runat="server" CssClass="lbl" Text="7th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry7" runat="server" CssClass="bx" tabindex="5"  onkeypress="OnKeyPress('textEntry8', event)"></asp:TextBox></td>
               <td class="lcol"><asp:TextBox ID="txtANum7" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtAName7" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtMName7" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
           </tr>
            <tr>
                <td class="lblcol"><asp:Label ID="lblPlace8" runat="server" CssClass="lbl" Text="8th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry8" runat="server" CssClass="bx" tabindex="6"  onkeypress="OnKeyPress('textEntry9', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum8" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName8" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtMName8" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="lblcol"><asp:Label ID="lblPlace9" runat="server" CssClass="lbl" Text="9th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry9" runat="server" CssClass="bx" tabindex="7"  onkeypress="OnKeyPress('textEntry10', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum9" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName9" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtMName9" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
           </tr>
            <tr>
                <td class="lblcol"><asp:Label ID="lblPlace10" runat="server" CssClass="lbl" Text="10th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry10" runat="server" CssClass="bx" tabindex="8"  onkeypress="OnKeyPress('textEntry11', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum10" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName10" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtMName10" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
              <tr>
                <td class="lblcol"><asp:Label ID="lblPlace11" runat="server" CssClass="lbl" Text="11th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry11" runat="server" CssClass="bx" tabindex="8"  onkeypress="OnKeyPress('textEntry12', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum11" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName11" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtMName11" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="lblcol"><asp:Label ID="lblPlace12" runat="server" CssClass="lbl" Text="12th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry12" runat="server" CssClass="bx" tabindex="10"  onkeypress="OnKeyPress('textEntry13', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum12" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName12" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtMName12" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
           </tr>
            <tr>
                <td class="lblcol"><asp:Label ID="lblPlace13" runat="server" CssClass="lbl" Text="13th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry13" runat="server" CssClass="bx" tabindex="11"  onkeypress="OnKeyPress('textEntry14', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum13" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName13" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtMName13" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace14" runat="server" CssClass="lbl" Text="14th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry14" runat="server" CssClass="bx" tabindex="12"  onkeypress="OnKeyPress('textEntry15', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum14" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName14" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtMName14" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
           </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace15" runat="server" CssClass="lbl" Text="15th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry15" runat="server" CssClass="bx" tabindex="13"  onkeypress="OnKeyPress('textEntry16', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum15" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName15" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtMName15" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace16" runat="server" CssClass="lbl" Text="16th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry16" runat="server" CssClass="bx" tabindex="13"  onkeypress="OnKeyPress('textEntry17', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum16" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName16" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtMName16" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace17" runat="server" CssClass="lbl" Text="17th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry17" runat="server" CssClass="bx" tabindex="13"  onkeypress="OnKeyPress('textEntry18', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtANum17" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtAName17" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtMName17" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace18" runat="server" CssClass="lbl" Text="18th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry18" runat="server" CssClass="bx" tabindex="13"  onkeypress="OnKeyPress('textEntry19', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtANum18" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtAName18" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtMName18" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace19" runat="server" CssClass="lbl" Text="19th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry19" runat="server" CssClass="bx" tabindex="13"  onkeypress="OnKeyPress('textEntry20', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum19" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName19" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtMName19" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace20" runat="server" CssClass="lbl" Text="20th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry20" runat="server" CssClass="bx" tabindex="13"  onkeypress="OnKeyPress('textEntry21', event)"></asp:TextBox></td>
                <td class="lcol"><asp:TextBox ID="txtANum20" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName20" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtMName20" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
              <tr>
                <td class="lblcol"><asp:Label ID="lblPlace21" runat="server" CssClass="lbl" Text="21th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry21" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry22', event)"></asp:TextBox></td>
             <td class="lcol"><asp:TextBox ID="txtANum21" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName21" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtMName21" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace22" runat="server" CssClass="lbl" Text="22th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry22" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry23', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtANum22" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtAName22" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtMName22" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>

             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace23" runat="server" CssClass="lbl" Text="23th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry23" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry24', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtANum23" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtAName23" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtMName23" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace24" runat="server" CssClass="lbl" Text="24th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry24" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry25', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtANum24" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtAName24" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtMName24" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace25" runat="server" CssClass="lbl" Text="25th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry25" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry26', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtANum25" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtAName25" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtMName25" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace26" runat="server" CssClass="lbl" Text="26th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry26" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry27', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtANum26" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtAName26" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtMName26" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
            <tr>
                <td class="lblcol"><asp:Label ID="lblPlace27" runat="server" CssClass="lbl" Text="27th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry27" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry28', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtANum27" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtAName27" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtMName27" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace28" runat="server" CssClass="lbl" Text="28th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry28" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry29', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtANum28" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtAName28" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtMName28" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
            <tr>
                <td class="lblcol"><asp:Label ID="lblPlace29" runat="server" CssClass="lbl" Text="29th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry29" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry30', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtANum29" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtAName29" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtMName29" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace30" runat="server" CssClass="lbl" Text="30th" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry30" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry30', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtANum30" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtAName30" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtMName30" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
             </tr>

            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
           </tr>
           <tr>
                <td colspan="5"><hr /></td>
           </tr>


              <tr>
                <td class="lblcol"><asp:Label ID="lblPlace40" runat="server" CssClass="lbl" Text="1st Udder" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry40" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry41', event)"></asp:TextBox></td>
             <td class="lcol"><asp:TextBox ID="txtANum40" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
                <td class="mcol"><asp:TextBox ID="txtAName40" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtMName40" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
            
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace41" runat="server" CssClass="lbl" Text="2nd Udder" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry41" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry42', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtANum41" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtAName41" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtMName41" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
            </tr>
            
             <tr>
                <td class="lblcol"><asp:Label ID="lblPlace42" runat="server" CssClass="lbl" Text="3rd Udder" TabIndex="100"></asp:Label></td>
                <td class="lcol"><asp:TextBox ID="textEntry42" runat="server" CssClass="bx" tabindex="2"  onkeypress="OnKeyPress('textEntry43', event)"></asp:TextBox></td>
              <td class="lcol"><asp:TextBox ID="txtANum42" runat="server" CssClass="num" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
               <td class="mcol"><asp:TextBox ID="txtAName42" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
              <td class="mcol"><asp:TextBox ID="txtMName42" runat="server" CssClass="name" ReadOnly="true" TabIndex="200"></asp:TextBox></td>
           </tr>
            <tr>
                <td colspan="5"><hr /></td>
           </tr>
          <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
           </tr>
  
 </table>  
                 <center><input id="btnSubmit2" type="button" value="Submit" runat="server" /></center>


        </div>
        </div>
      </asp:View>

             <asp:View ID="vwReports" runat="server">
             <center>
                <div id="divReports" class="div">   
                    <h4 id="h3" runat="server" style="color:#849eb5;">REPORTS</h4>
            <table id="Table3" runat="server" style="width:500px;" cellpadding="0" cellspacing="0">
            <tr>
                <td class="hdr">&nbsp;&nbsp;&nbsp;<span style="text-decoration: underline;">Full Show Reports</span></td>
                <td class="hdr">&nbsp;<span style="text-decoration: underline;">Individual Reports</span></td>
            </tr>
           <tr>
                <td class="mr" style="text-align:left">&nbsp;&nbsp;&nbsp;<cc2:OboutRadioButton ID="rdoExhibitor" runat="server" GroupName="Menu" Text="Exhibitor Report" onclick="LaunchExhibitorReport();return false;"></cc2:OboutRadioButton></td>
                <td class="mr" style="text-align: left;">&nbsp;</td>
            </tr>
            <tr>
                <td class="mr" style="text-align:left">&nbsp;&nbsp;&nbsp;<cc2:OboutRadioButton ID="rdoCheckinReport" runat="server" GroupName="Menu" Text="Check In Report" onclick="LaunchCheckinReport();return false;"></cc2:OboutRadioButton></td>
                <td class="mr" style="text-align: left;">&nbsp;&nbsp;&nbsp;<cc2:OboutRadioButton ID="rdoSingleCheckinReport" runat="server" GroupName="Menu" Text="Single Check In Report" onclick="LaunchSingleCheckinReport();return false;"></cc2:OboutRadioButton></td>
          </tr>
            <tr>
                <td class="mr" style="text-align:left">&nbsp;&nbsp;&nbsp;<cc2:OboutRadioButton ID="rdoRingReport" runat="server" GroupName="Menu" Text="Ring Card Report" onclick="LaunchRingReport();return false;"></cc2:OboutRadioButton></td>
                <td class="mr" style="text-align: left;">&nbsp;&nbsp;&nbsp;<cc2:OboutRadioButton ID="rdoSingleRingReport" runat="server" GroupName="Menu" Text="Single Ring Card Report" onclick="LaunchSingleRingReport();return false;"></cc2:OboutRadioButton></td>
            </tr>
            <tr>
                <td class="mr" style="text-align:left">&nbsp;&nbsp;&nbsp;<cc2:OboutRadioButton ID="rdoMilkout" runat="server" GroupName="Menu" Text="Milkout Report" onclick="LaunchMilkoutReport();return false;"></cc2:OboutRadioButton></td>
                <td class="mr" style="text-align: left;">&nbsp;</td>
            </tr>
            <tr>
                <td class="mr" style="text-align:left">&nbsp;&nbsp;&nbsp;<cc2:OboutRadioButton ID="rdoSecretary" runat="server" GroupName="Menu" Text="Secretary Report" onclick="LaunchSecretaryReport();return false;"></cc2:OboutRadioButton></td>
                <td class="mr" style="text-align: left;">&nbsp;</td>
            </tr>
            <tr>
                <td class="mr" style="text-align:left">&nbsp;&nbsp;&nbsp;<cc2:OboutRadioButton ID="rdoShowbook" runat="server" GroupName="Menu" Text="Showbook Report" onclick="LaunchShowbookReport();return false;"></cc2:OboutRadioButton></td>
                <td class="mr" style="text-align: left;">&nbsp;</td>
            </tr>
             <tr>
                <td class="mr" style="text-align:left">&nbsp;&nbsp;&nbsp;<cc2:OboutRadioButton ID="rdoResults" runat="server" GroupName="Menu" Text="Results Report" onclick="LaunchResultsReport();return false;"></cc2:OboutRadioButton></td>
                <td class="mr" style="text-align: left;">&nbsp;</td>
            </tr>
            <tr>
                <td class="mr" style="text-align:left">&nbsp;&nbsp;&nbsp;<cc2:OboutRadioButton ID="rdoExhibitorBarcode" runat="server" GroupName="Menu" Text="Exhibitor Barcode Report" onclick="LaunchExhibitorBarcodeReport();return false;"></cc2:OboutRadioButton></td>
                <td class="mr" style="text-align: left;">&nbsp;</td>
            </tr>
          </table>
                </div>
            </center>
            </asp:View>
 
              <asp:View ID="vwAdd" runat="server">
             <center>
                <div id="div1" class="div">   
                    <h4 id="h4" runat="server" style="color:#849eb5;">ADD ENTRY</h4>
            <table id="Table4" runat="server" style="width:500px;" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3"><asp:DropDownList ID="ddExhibitors" runat="server" CssClass="lbl"  onchange="GetAddMember()"></asp:DropDownList></td>
            </tr>
            <tr><td colspan="3">&nbsp;</td></tr>
             <tr><td colspan="3">&nbsp;</td></tr>
           <tr>
                <td class="lbl">Breed&nbsp;&nbsp;<select id="ddaddbreed" runat="server" class="drop"  onchange="FillAddDrops()" /></td>
                <td class="lbl">&nbsp;&nbsp;Class&nbsp;&nbsp;<select id="ddaddclass" runat="server" class="drop" onchange="FillAddDrops()" /></td>
                <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtAddClassNum" runat="server" CssClass="cbox" Width="48px"></asp:TextBox></td>
             </tr>
            <tr><td colspan="3">&nbsp;</td></tr>
             <tr><td colspan="3">&nbsp;</td></tr>
            <tr>
                <td class="lbl" style="text-align: left;">Goat Id&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtAddGoat" runat="server" CssClass="lbl" Width="100px" onkeypress="return AddGoatKeyPress(event)"></asp:TextBox></td>
                <td class="lbl" style="text-align: left;" colspan="2"><asp:TextBox ID="txtAddGoatName" runat="server" CssClass="name" Width="250px"></asp:TextBox></td>        
                <td class="lbl" style="text-align: left;"><asp:TextBox ID="txtAddBirthdate" runat="server" CssClass="name" Width="100px"></asp:TextBox><br />
                <asp:TextBox ID="txtAddTattoo" runat="server" CssClass="name" Width="100px"></asp:TextBox></td>
            </tr>
             <tr><td colspan="3">&nbsp;</td></tr>
             <tr><td colspan="3">&nbsp;</td></tr>
            <tr><td colspan="3">&nbsp;</td></tr>
             <tr><td colspan="3">&nbsp;</td></tr>
        </table>
                <input id="btnSubmitAdd" type="button" value="Submit" runat="server" onclick="return WriteAdd();" />
                </div>
            </center>
                  <asp:HiddenField ID="hdnAddMemberId" runat="server" />
                  <asp:HiddenField ID="hdnAddCustomerId" runat="server" />
                  <asp:HiddenField ID="hdnAddDisplayAs" runat="server" />
                  <asp:HiddenField ID="hdnAddShowAs" runat="server" />
                  <asp:HiddenField ID="hdnAddGoatId" runat="server" />
                  <asp:HiddenField ID="hdnAddSireId" runat="server" />
                  <asp:HiddenField ID="hdnAddDamId" runat="server" />
                  <asp:HiddenField ID="hdnBreederId" runat="server" />
           </asp:View>
          
     </asp:MultiView>
        
   <%--  START PANEL MESSAGE POPUP --%>
               
<%--       <asp:Panel ID="pnlMessage" runat="server" BorderColor="Maroon" BorderStyle="Inset"  CssClass="detail" style="display:none; width: 400px; height: auto;  background-color: #F8F7F7;"  BorderWidth="2px">
           <asp:UpdatePanel ID="updMessage" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
           		<asp:Button ID="btnShowMessagePopup" runat="server" Text="btnShowMessage" style="display:none" />
          	<cc1:ModalPopupExtender ID="mdlMessage" runat="server" targetControlID="btnShowMessagePopup" PopupControlID="pnlMessage"
	  	BehaviorID="mdlMSGBehavior" BackgroundCssClass="modalBackground" CancelControlID="btnMsgOk" OkControlID="btnMsgOk"/>
                      <center><asp:Label ID="Label1" runat="server" Text="MESSAGE" style="font-family: Tahoma; font-size: 9pt;"></asp:Label></center>
                    <asp:TextBox ID="txtMessage" runat="server" Text="" style="font-family:Tahoma; font-size: 8pt; font-weight:bold; color:#333333; width:375px; height: 100px; text-align:center; vertical-align: middle; border-style: none; border-width: 0px;  background-color: #F8F7F7;"></asp:TextBox>	             
                   <br />                
                       <center><button id="btnMsgOk" runat="server" style="font-family: Tahoma; font-size: 8pt; text-align:center">Ok</button></center>
            </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>--%>
        
   <%--  END PANEL MESSAGE POPUP --%>
            
   <%--  TEMPORARY --%>    
      
<%--        <div id="cHide" style="display: none;">
        <asp:TextBox ID="txtMessage" runat="server" Text=""></asp:TextBox>
        <button id="btnGrpOk" runat="server"></button>
        <button id="btnGrpCancel" runat="server"></button>
        <asp:CheckBox ID="chkOverride" Text="Override" runat="server" />
        <asp:DropDownList ID="ddGroup" runat="server" Visible="false"></asp:DropDownList>
	    <asp:DropDownList ID="ddGroupAll" runat="server"></asp:DropDownList>
        <asp:TextBox ID="txtGroupEntryNum" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtGroupParent" runat="server" Text=""></asp:TextBox>
        </div>--%>
   <%--  TEMPORARY --%>  

      <%--  START PANEL GROUP POPUP --%>

              <asp:Panel ID="pnlGroup" runat="server" BorderColor="Maroon" BorderStyle="Inset"  CssClass="detail" style="display:none; width: 400px; height: auto;  background-color: #F8F7F7;"  BorderWidth="2px">
           <asp:UpdatePanel ID="updGroup" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
           		<asp:Button ID="btnShowGroupPopup" runat="server" Text="btnShowGroup" style="display:none" />
          	<cc1:ModalPopupExtender ID="mdlGroup" runat="server" targetControlID="btnShowGroupPopup" PopupControlID="pnlGroup"
	  	BehaviorID="mdlGrpBehavior" BackgroundCssClass="modalBackground" CancelControlID="btnGrpCancel" OkControlID="btnGrpOk" />
       <center><asp:Label ID="lblGroup" runat="server" Text="GROUP CLASS SUBSTITUTION" style="font-family: Tahoma; font-size: 9pt;"></asp:Label></center>
                    <br />
                    <asp:Label ID="lblGroupEntryNum" runat="server"  style="font-family:Tahoma; font-size: 8pt;" Text="Entry Number To Substitue:"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtGroupEntryNum" runat="server" style="font-family:Tahoma; font-size: 8pt; width: 75px; text-align: center;"></asp:TextBox>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <asp:CheckBox ID="chkOverride" Text="Override" runat="server" AutoPostBack="false" style="font-family:Tahoma; font-size: 8pt;" />
                   <br />    
                   <br />   
	   <center>
	        <asp:DropDownList ID="ddGroup" runat="server" style="font-family:Tahoma; font-size: 8pt;width:375px;"></asp:DropDownList>
	        <asp:DropDownList ID="ddGroupAll" runat="server" style="font-family:Tahoma; font-size: 8pt;width:300px;" Visible="false"></asp:DropDownList>
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtGroupParent" runat="server" style="font-family:Tahoma; font-size: 8pt;width:60px;" Visible="false"></asp:TextBox>
	   </center>
                   <br />    
                   <br />   
       <table style="width: 400px"><tr>            
                       <td style="width: 200px; text-align: center;"><button id="btnGrpOk" runat="server" style="font-family: Tahoma; font-size: 8pt; text-align:center; width: 75px;">Ok</button></td>
                       <td style="width: 200px; text-align: center;"><button id="btnGrpCancel" runat="server" style="font-family: Tahoma; font-size: 8pt; text-align:center; width: 75px;">Cancel</button></td>
	   </tr>
	   </table>
            </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>

<%--  TEMPORARY
        <asp:DropDownList ID="ddGroup" runat="server" style="font-family:Tahoma; font-size: 8pt;width:375px;" Visible="false"></asp:DropDownList>
	        <asp:DropDownList ID="ddGroupAll" runat="server" style="font-family:Tahoma; font-size: 8pt;width:300px; display: none;" Visible="false"></asp:DropDownList>
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtGroupParent" runat="server" style="font-family:Tahoma; font-size: 8pt;width:60px; display:none;" Visible="false"></asp:TextBox>
        <asp:CheckBox ID="chkOverride" Text="Override" runat="server" AutoPostBack="true" style="font-family:Tahoma; font-size: 8pt; display:none;" />
        <button id="btnGrpOk" runat="server" style="font-family: Tahoma; font-size: 8pt; text-align:center; width: 75px; display:none;">Ok</button>
        <asp:TextBox ID="txtGroupEntryNum" runat="server" style="font-family:Tahoma; font-size: 8pt; width: 75px; text-align: center; display: none;"></asp:TextBox>
        	        <asp:DropDownList ID="DropDownList1" runat="server" style="font-family:Tahoma; font-size: 8pt;width:375px" Visible="false"></asp:DropDownList>
	        <asp:DropDownList ID="DropDownList2" runat="server" style="font-family:Tahoma; font-size: 8pt;width:300px;" Visible="false"></asp:DropDownList>
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="TextBox1" runat="server" style="font-family:Tahoma; font-size: 8pt;width:60px;" Visible="false"></asp:TextBox>
--%>  

   <%--  END PANEL GROUP POPUP --%>  
   
    </form>
    


</body>
</html>
